
/**
 * @author Dulan
 *
 */
package com.rezrobot.pojo;

import org.openqa.selenium.Dimension;
import org.openqa.selenium.JavascriptExecutor;

import com.rezrobot.core.UIElement;
import com.rezrobot.utill.DriverFactory;

public class Image extends UIElement {

	public boolean isImageLoaded() throws Exception {

		Object result = ((JavascriptExecutor) DriverFactory.getInstance()
				.getDriver()).executeScript("return arguments[0].complete && "
				+ "typeof arguments[0].naturalWidth != \"undefined\" && "
				+ "arguments[0].naturalWidth > 0", this.getWebElement());

		boolean loaded = false;
		if (result instanceof Boolean) {
			loaded = (Boolean) result;
			return loaded;
		} else {
			return false;
		}

	}

	public String getImagePath() throws Exception {

		return this.getAttribute("src");
	}
	

}