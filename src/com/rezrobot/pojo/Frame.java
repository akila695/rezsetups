package com.rezrobot.pojo;

import com.rezrobot.core.UIElement;
import com.rezrobot.utill.DriverFactory;

public class Frame extends UIElement {
    
    
    public void  switchToFrame() throws Exception{
	    try {
		DriverFactory.getInstance().getDriver().switchTo().frame(this.getWebElement());
	    } catch (Exception var2) {
	        DriverFactory.getInstance().getScreenShotCreator().getScreenShotOnFailure();
	       // Utilities.pause(15000);
	        throw var2;
	    }
	}




}
