package com.rezrobot.utill;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.InetSocketAddress;
import java.net.Proxy;
import java.net.URL;
import java.util.HashMap;

import com.rezrobot.processor.LabelReadProperties;

public class SetProxyPort {

	public String setProxy(String xmlHomeUrl) throws IOException
	{
		LabelReadProperties 							labelProperty 					= new LabelReadProperties();
		HashMap<String, String> 						configDetails 					= labelProperty.getConfigProperties();
		URL url2 = new URL(xmlHomeUrl);
		Proxy proxy = new Proxy(Proxy.Type.HTTP, new InetSocketAddress(configDetails.get("proxy"), Integer.parseInt(configDetails.get("port")))); // or whatever your proxy is
		HttpURLConnection uc = (HttpURLConnection)url2.openConnection(proxy);
	
		uc.connect();
	
		String line = null;
		StringBuffer tmp = new StringBuffer();
		BufferedReader in = new BufferedReader(new InputStreamReader(uc.getInputStream()));
		while ((line = in.readLine()) != null) {
		   tmp.append(line);
		}
		return String.valueOf(tmp);
	}
}
