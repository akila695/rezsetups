package com.rezrobot.utill;
/**
 * @author Dulan
 *
 */
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;

public class XmlReader {
	
	
public NodeList getNodeList(String tagname){
	
	try {
		System.out.println(tagname);
		InputStream st = new FileInputStream(new File("Resources/Pages/"+tagname+".xml"));
		DocumentBuilderFactory dbFacory  = DocumentBuilderFactory.newInstance();
		DocumentBuilder        db        = dbFacory.newDocumentBuilder();
		Document doc = db.parse(st);
		doc.getDocumentElement().normalize();
		NodeList    nList = doc.getElementsByTagName("element");
		System.out.println(doc.getDocumentElement().getNodeName());
		return nList;
		
		
	} catch (Exception e) {
		e.printStackTrace();
		return null;
	}
}

public static void main(String[] args) {
	XmlReader rd = new XmlReader();
	rd.getNodeList("HomePage");
}

}
