package com.rezrobot.utill;
/**
 * @author Dulan
 *
 */
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;


public class ReadProperties {
	static FileInputStream fs=null;
	static Properties prop=new Properties();
	public static String  readpropety(String property,String File) throws IOException {
	
		try {
			fs= new FileInputStream(new File(File));
			prop.load(fs);
			
			return  prop.getProperty(property);
		
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		
		return prop.getProperty(property);
		
		
		
	}
	
	
	public static String  readpropety(String property) throws IOException {
		
		try {
			fs= new FileInputStream(new File("Config.properties"));
			prop.load(fs);
			
			return  prop.getProperty(property);
		
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		
		return prop.getProperty(property);
		
		
		
	}
	
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	
	public static HashMap<String, String> readpropeties() throws IOException {
		
		HashMap<String, String>    mymap = null ;
		
		try {
			fs= new FileInputStream(new File("Config.properties"));
			prop.load(fs);
			mymap= new HashMap<String, String>((Map)prop);
			return mymap;
		
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		
		return mymap;
		
		
		
	}
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public static HashMap<String, String> readpropeties(String FilePath) throws IOException {
		
		HashMap<String, String>    mymap = null ;
		
		try {
			fs= new FileInputStream(new File(FilePath));
			prop.load(fs);
			mymap= new HashMap<String, String>((Map)prop);
			return mymap;
		
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		
		return mymap;
		
		
		
	}
	public static void main(String[] args ) throws IOException
	{
	
		System.out.println(readpropeties("Resources\\LabelProperties\\fixedPackageLabels.properties").get("fpBEC.packageplanlabe"));

		
              
	}
	
	//Get the key, value of a property file - return as a HashMap
		public static HashMap<String, String> getPropertyInMap(String path) {
			
			Properties				prop		= new Properties();
			FileReader				reader;
			HashMap<String, String> propertyMap	= new HashMap<String, String>();
			
			try {
				reader = new FileReader(new File(path));
				prop.load(reader);
				for (String key : prop.stringPropertyNames()) 
				{
					String value 		= prop.getProperty(key);
					propertyMap.put(key, value);
				}
			} catch (Exception e) {
				//System.out.println(e.toString());
			}
			return propertyMap;
		}



}



