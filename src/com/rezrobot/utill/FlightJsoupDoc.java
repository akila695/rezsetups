/*Sanoj*/
package com.rezrobot.utill;

import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Iterator;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import com.rezrobot.enumtypes.XMLFileType;
import com.rezrobot.enumtypes.XMLLocateType;
import com.rezrobot.flight_circuitry.xml_objects.XML;


public class FlightJsoupDoc {
	
	//Logger 							logger				= null;
	File							input				= null;

	public static String urlGenerator(String turl, String supplier)
	{
		//logger.debug("Generating URL (Append current date)....");
		//String URL1 = "";
		String URL = "";
		//String url = turl;
		//logger.info("URL loaded from property map");
		
		DateFormat	dateFormat	= new SimpleDateFormat("dd-MM-yyyy");
		Calendar	cal			= Calendar.getInstance();
		String		DATE		= dateFormat.format(cal.getTime());
		//logger.debug("Replace the Word DATE by current system date");
		//URL1					= url.replace("DATE", DATE);
		URL						= turl+"%2F"+DATE+"%2F"+supplier+"+-+AIR2";

		
		//logger.info("Returning URL ");
		return URL;
	}
	
	public static Document getDoc(XMLLocateType Locator, XMLFileType tracerPrefix, String tracerNo, String baseUrl, String supplier, XML xmlObject ) throws IOException
	{
		Document RetDoc = null;
		String urlall = "";
		
		/*if(Locator == XMLLocateType.FILEPATH)
		{
			input = new File(tracerNo);
			try 
			{
				RetDoc   = Jsoup.parse(input,"UTF-8");
			} 
			catch (IOException e) 
			{
				
			}
		}*/
		if(Locator == XMLLocateType.TRACER)
		{
			try {
				System.out.println("INFO -> TRACER / XML DOCUMENT READ");
				String TracerPrefix		= tracerPrefix.toString();
				String TracerValue		= tracerNo;
				String TracerID			= TracerPrefix.concat(TracerValue);
				
				String URL = urlGenerator(baseUrl, supplier);
				System.out.println("INFO -> URL : "+URL);
				String xmlurl = urlCreator(URL, TracerID);
				System.out.println(xmlurl);
				String ul = URL.split("/Browser")[0];
				
				urlall = ul+xmlurl;
					
				//urlall = "";
				System.out.println("INFO -> XML URL : "+urlall);
			} catch (Exception e) {
				// TODO: handle exception
				System.out.println(e);
			}
			
			
			try {
				SetProxyPort 			proxyAndPort	= new SetProxyPort();

				RetDoc     = Jsoup.parse(proxyAndPort.setProxy(urlall));
				xmlObject.setUrl(urlall);
			} catch (Exception e) {
				System.out.println("INFO -> JSOUP PARSING FAILED...!!!");
			}				
			
		}
		
		return RetDoc;
	}
	
	public static String urlCreator (String url, String tracer) throws IOException
	{
		SetProxyPort 			proxyAndPort	= new SetProxyPort();
		Document doc = Jsoup.parse(proxyAndPort.setProxy(url));
		
		Iterator<Element> aaa = doc.getElementsByTag("a").iterator();
		String xmlUrl = "";
		//String today = this.getToday();
		while(aaa.hasNext())
		{
			Element currentElement = aaa.next();
			if(currentElement.getElementsByTag("a").first().text().contains(tracer))
			{
				xmlUrl = currentElement.getElementsByTag("a").attr("href");
				if(xmlUrl.contains("/jsp"))
				{
					xmlUrl = xmlUrl.replace("/jsp", "");
				}
				System.out.println("XMLURL : "+xmlUrl);
				break;
			}
		}
		return xmlUrl;
	}

	
	public static void main(String[] args) throws IOException {
					 // "http://v3proddev2.rezg.net/Browser.jsp?sort=-3&file=%2Fvar%2Flog%2Frezg%2Fapp%2Fairxml%2FDATE%2FSUPPLIER+-+AIR1%2FAIR1_"
		String u	= "http://v3proddev2.rezg.net/Browser.jsp?sort=-3&dir=%2Fvar%2Flog%2Frezg%2Fapp%2Fairxml%2FDATE%2FSUPPLIER+-+AIR1";
		//String xml	= "http://v3proddev2.rezg.net/Browser.jsp?sort=-3&file=%2Fvar%2Flog%2Frezg%2Fapp%2Fairxml%2F22-06-2016%2FSabre+-+AIR1%2FAIR1_LowFareSearchResponse_9586_2016-06-22_14%3A34%3A47.028.xml";
		FlightJsoupDoc doc = new FlightJsoupDoc();
		String t = doc.urlGenerator(u, "Sabre");
		System.out.println(t);
		String prefix = "AIR1_LowFareSearchRequest_25801";
		String xmlUrl = doc.urlCreator(t, prefix);
		String ul = u.split("/Browser")[0];
		System.out.println(ul+xmlUrl);
		Document doce = Jsoup.connect(ul+xmlUrl).get();
		System.out.println(doce);
	}
	
	
}
