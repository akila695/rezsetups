
package com.rezrobot.utill;

import java.util.Map;

import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import com.rezrobot.core.UIElement;
import com.rezrobot.pojo.Button;
import com.rezrobot.pojo.Calendar;
import com.rezrobot.pojo.CheckBox;
import com.rezrobot.pojo.ComboBox;
import com.rezrobot.pojo.Div;
import com.rezrobot.pojo.Frame;
import com.rezrobot.pojo.Image;
import com.rezrobot.pojo.InputFiled;
import com.rezrobot.pojo.Label;
import com.rezrobot.pojo.Link;
import com.rezrobot.pojo.RadioButton;
import com.rezrobot.pojo.Table;

/**
 * @author Dulan
 *
 */
public class ElementReader {

private NodeList page ;
private Element currentElement;

public ElementReader(NodeList page){
    this.page  = page;
}


public void loadElements(Map<String,UIElement> Elements){

	if(null != page){
		for (int i = 0; i < page.getLength(); i++) {
			
			if(this.page.item(i).getNodeName().equalsIgnoreCase("Element")) {
			currentElement = (Element)page.item(i);
			
			if(this.currentElement.getAttribute("type").equalsIgnoreCase("button")){
				Button var1 = new Button();
				var1.setElementProperties(this.currentElement.getAttribute("key"), this.currentElement.getAttribute("ref_type"), this.currentElement.getAttribute("ref"));
				Elements.put(this.currentElement.getAttribute("key"), var1);
			} else if(this.currentElement.getAttribute("type").equalsIgnoreCase("calendar")){
				Calendar var2 = new Calendar();
				var2.setElementProperties(this.currentElement.getAttribute("key"), this.currentElement.getAttribute("ref_type"), this.currentElement.getAttribute("ref"));
				Elements.put(this.currentElement.getAttribute("key"), var2);
			} else if(this.currentElement.getAttribute("type").equalsIgnoreCase("input")||this.currentElement.getAttribute("type").equalsIgnoreCase("inputfield")){
				InputFiled var3 = new InputFiled();
				var3.setElementProperties(this.currentElement.getAttribute("key"), this.currentElement.getAttribute("ref_type"), this.currentElement.getAttribute("ref"));
				Elements.put(this.currentElement.getAttribute("key"), var3);
			} else if(this.currentElement.getAttribute("type").equalsIgnoreCase("checkbox")){
				CheckBox var4 = new CheckBox();
				var4.setElementProperties(this.currentElement.getAttribute("key"), this.currentElement.getAttribute("ref_type"), this.currentElement.getAttribute("ref"));
				Elements.put(this.currentElement.getAttribute("key"), var4);
			} else if(this.currentElement.getAttribute("type").equalsIgnoreCase("link")){
				Link var5 = new Link();
				var5.setElementProperties(this.currentElement.getAttribute("key"), this.currentElement.getAttribute("ref_type"), this.currentElement.getAttribute("ref"));
				Elements.put(this.currentElement.getAttribute("key"), var5);
			} else if(this.currentElement.getAttribute("type").equalsIgnoreCase("image")){
				Image var6 = new Image();
				var6.setElementProperties(this.currentElement.getAttribute("key"), this.currentElement.getAttribute("ref_type"), this.currentElement.getAttribute("ref"));
				Elements.put(this.currentElement.getAttribute("key"), var6);
			} else if(this.currentElement.getAttribute("type").equalsIgnoreCase("frame")){
				Frame var7 = new Frame();
				var7.setElementProperties(this.currentElement.getAttribute("key"), this.currentElement.getAttribute("ref_type"), this.currentElement.getAttribute("ref"));
				Elements.put(this.currentElement.getAttribute("key"), var7);
			} else if(this.currentElement.getAttribute("type").equalsIgnoreCase("combobox")){
				ComboBox var8 = new ComboBox();
				var8.setElementProperties(this.currentElement.getAttribute("key"), this.currentElement.getAttribute("ref_type"), this.currentElement.getAttribute("ref"));
				Elements.put(this.currentElement.getAttribute("key"), var8);
			}else if(this.currentElement.getAttribute("type").equalsIgnoreCase("table")){
				Table var9 = new Table();
				var9.setElementProperties(this.currentElement.getAttribute("key"), this.currentElement.getAttribute("ref_type"), this.currentElement.getAttribute("ref"));
				Elements.put(this.currentElement.getAttribute("key"), var9);
			}else if(this.currentElement.getAttribute("type").equalsIgnoreCase("div")){
				Div var10 = new Div();
				var10.setElementProperties(this.currentElement.getAttribute("key"), this.currentElement.getAttribute("ref_type"), this.currentElement.getAttribute("ref"));
				Elements.put(this.currentElement.getAttribute("key"), var10);
			}else if(this.currentElement.getAttribute("type").equalsIgnoreCase("label")||this.currentElement.getAttribute("type").equalsIgnoreCase("text")){
				Label var10 = new Label();
				var10.setElementProperties(this.currentElement.getAttribute("key"), this.currentElement.getAttribute("ref_type"), this.currentElement.getAttribute("ref"));
				Elements.put(this.currentElement.getAttribute("key"), var10);
			}else if(this.currentElement.getAttribute("type").equalsIgnoreCase("radiobutton")){
				RadioButton var11 = new RadioButton();
				var11.setElementProperties(this.currentElement.getAttribute("key"), this.currentElement.getAttribute("ref_type"), this.currentElement.getAttribute("ref"));
				Elements.put(this.currentElement.getAttribute("key"), var11);
			}else {
				UIElement var12=new UIElement();
				var12.setElementProperties(this.currentElement.getAttribute("key"), this.currentElement.getAttribute("ref_type"), this.currentElement.getAttribute("ref"));
				Elements.put(this.currentElement.getAttribute("key"), var12);
			}
			
			}
		}
	}
}

}
