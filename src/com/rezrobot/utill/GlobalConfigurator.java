package com.rezrobot.utill;

import java.util.UUID;

public class GlobalConfigurator {

public static  String HOTEL_SEARCH_CACHE    = "7098";//This will be overridden 
public static  String FLIGHT_SEARCH_CACHE   = "2651822562386";//This will be overridden 
public static  String AVAIL_CACHE_ID        = "ha-hs-19386d8b-e129-4688-88a7-c30505bababf-1437983730-1";//This will be overridden 
public static  String FLIGHT_AVAIL_CACHE_ID = "ha-hs-19386d8b-e129-4688-88a7-c30505bababf-1437983730-1";
public static  String PROPERTY_FILE_PATH    = "../Rezrobot_Details/Common/Config.properties";
public static  String AXIS_CONFIG_PATH      = "C:/rezsystem/Axis2/axis2-1.4.1/repository";
public static  String AXIS_XML_PATH         = "C:/rezsystem/Axis2/axis2-1.4.1/conf/axis2.xml";


public static String getCacheId(){

    String token  = UUID.randomUUID().toString().trim().split("-")[0];
    HOTEL_SEARCH_CACHE = token;
    return token;
	
}

public static String getFlightCacheId(){

	String token  = UUID.randomUUID().toString().trim().split("-")[0];
    FLIGHT_SEARCH_CACHE = token;
    return token;
	
}

public static void main(String[] args) {
	System.out.println(HOTEL_SEARCH_CACHE);
	System.out.println(getCacheId()+"---->"+HOTEL_SEARCH_CACHE.getBytes().length);
	System.out.println(HOTEL_SEARCH_CACHE);
	
}


}
