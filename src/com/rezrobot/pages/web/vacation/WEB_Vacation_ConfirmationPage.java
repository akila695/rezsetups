package com.rezrobot.pages.web.vacation;

import java.util.HashMap;

import com.rezrobot.core.PageObject;
import com.rezrobot.utill.DriverFactory;

public class WEB_Vacation_ConfirmationPage extends PageObject{

	public boolean getConfirmationPageAvailability()
	{
		if(getElementVisibility("label_booking_reference_id", 30))
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	
	public HashMap<String, String> getValues()
	{
		HashMap<String, String> values = new HashMap<>();
		try {
			switchToDefaultFrame();
		} catch (Exception e) {
			// TODO: handle exception
		}
		try {
			System.out.println("Started reading confirmation page");
			
			try {
				getElementVisibility("label_booking_reference_id", 120);
			} catch (Exception e) {
				// TODO: handle exception
				switchToDefaultFrame();
			}
			
			System.out.println(getElement("label_booking_reference_id").getText());
			values.put("bookingReference", 						getElement("label_booking_reference_id").getText());
			System.out.println(getElement("label_reservation_number_id").getText());
			try {
				values.put("reservationNumber", 					getElement("label_reservation_number_id").getText().replace(":", "").trim());
			} catch (Exception e) {
				// TODO: handle exception
				values.put("reservationNumber", getElement("label_booking_not_saved_xpath").getText());
			}	
			values.put("portalTelNumber", 						getElement("label_portal_tel_xpath").getText());
			values.put("portalFax", 							getElement("label_portal_fax_xpath").getText());
			values.put("flightSupConNumber", 					getElement("label_flight_supplier_con_number_xpath").getText());
			values.put("flightDepartureCity", 					getElement("label_flight_departure_city_id").getText());
			values.put("flightArrivalCity", 					getElement("label_flight_arrival_city_id").getText());
			values.put("flightNumOfAdults", 					getElement("label_flight_number_of_adults_xpath").getText());
			values.put("flightNumOfChildren", 					getElement("label_flight_number_of_children_xpath").getText());
			values.put("outBoundDepdateTime", 					getElement("label_outBoundDepdateTime_1_id").getText());
			values.put("outbounddeplocation", 					getElement("label_outbounddeplocation_id").getText());
			values.put("outboundairline", 						getElement("label_outboundairline_id").getText());
			values.put("outBoundArrdatetime", 					getElement("label_outBoundArrdatetime_1_id").getText());
			values.put("outboundarrlocation", 					getElement("label_outboundarrlocation_id").getText());
			values.put("inDepartureDateTime", 					getElement("label_inDepartureDateTime_1_id").getText());
			values.put("inbounddeplocation", 					getElement("label_inbounddeplocation_id").getText());
			values.put("inboundairline", 						getElement("label_inboundairline_id").getText());
			values.put("inArrivalDateTime", 					getElement("label_inArrivalDateTime_1_id").getText());
			values.put("inboundarrlocation", 					getElement("label_inboundarrlocation_id").getText());
			try {
				values.put("outBoundDepdateTime2", 					getElement("label_outBoundDepdateTime_2_id").getText());
				values.put("outbounddeplocation2", 					getElements("label_outbounddeplocation_id").get(1).getText());
				values.put("outboundairline2", 						getElements("label_outboundairline_id").get(1).getText());
				values.put("outBoundArrdatetime2", 					getElement("label_outBoundArrdatetime_2_id").getText());
				values.put("outboundarrlocation2", 					getElements("label_outboundarrlocation_id").get(1).getText());
				values.put("inDepartureDateTime2", 					getElement("label_inDepartureDateTime_2_id").getText());
				values.put("inbounddeplocation2", 					getElements("label_inbounddeplocation_id").get(1).getText());
				values.put("inboundairline2", 						getElements("label_inboundairline_id").get(1).getText());
				values.put("inArrivalDateTime2", 					getElement("label_inArrivalDateTime_2_id").getText());
				values.put("inboundarrlocation2", 					getElements("label_inboundarrlocation_id").get(1).getText());
			} catch (Exception e) {
				// TODO: handle exception
			}
			values.put("hotelName", 							getElement("label_hotel_name_id").getText());
			values.put("hotelBookingStatus", 					getElement("label_hotel_booking_status_id").getText());
			values.put("hotelSupConNumber", 					getElement("label_hotel_supplier_con_number_xpath").getText());
			values.put("checkin", 								getElement("label_hotel_checkin_id").getText());
			values.put("checkout", 								getElements("label_hotel_checkout_id").get(0).getText());
			values.put("numberOfRooms", 						getElements("label_number_of_rooms_id").get(1).getText());
			values.put("numberOfNights", 						getElements("label_number_of_nights_id").get(2).getText());
			values.put("estimatedArrivalTime", 					getElements("label_estimated_Arrival_time_id").get(3).getText());
			String[] 						transID	= DriverFactory.getInstance().getDriver().getCurrentUrl().split("transectionId");
			values.put("transactionID",  transID[1].split("-")[1]);
			String room = "";
			String roomType = "";
			String bedType = "";
			String ratePlan = "";
			for(int i = 1 ; i <= 4 ; i++)
			{
				if(i == 1)
				{
					room 		= getElement("label_hotel_room_id").changeRefAndGetElement(getElement("label_hotel_room_id").getRef().replace("replace", String.valueOf(i))).getText();
					roomType 	= getElement("label_hotel_room_type_id").changeRefAndGetElement(getElement("label_hotel_room_type_id").getRef().replace("replace", String.valueOf(i))).getText();
					bedType 	= getElement("label_hotel_bed_type_id").changeRefAndGetElement(getElement("label_hotel_bed_type_id").getRef().replace("replace", String.valueOf(i))).getText();
					ratePlan 	= getElement("label_hotel_rateplan_id").changeRefAndGetElement(getElement("label_hotel_rateplan_id").getRef().replace("replace", String.valueOf(i))).getText();
				}
				else
				{
					try {
						room 		= room 		+ "," + 	getElement("label_hotel_room_id").changeRefAndGetElement(getElement("label_hotel_room_id").getRef().replace("replace", String.valueOf(i))).getText();
						roomType 	= roomType 	+ "," + 	getElement("label_hotel_room_type_id").changeRefAndGetElement(getElement("label_hotel_room_type_id").getRef().replace("replace", String.valueOf(i))).getText();
						bedType 	= bedType 	+ "," + 	getElement("label_hotel_bed_type_id").changeRefAndGetElement(getElement("label_hotel_bed_type_id").getRef().replace("replace", String.valueOf(i))).getText();
						ratePlan 	= ratePlan 	+ "," + 	getElement("label_hotel_rateplan_id").changeRefAndGetElement(getElement("label_hotel_rateplan_id").getRef().replace("replace", String.valueOf(i))).getText();
					} catch (Exception e) {
						// TODO: handle exception
						break;
					}
					
				}
			}	
			values.put("hotelRooms", 							room);
			values.put("hotelRoomType", 						roomType);
			values.put("hotelBedType", 							bedType);
			values.put("hotelRatePlan", 						ratePlan);
		//	System.out.println(getElement("label_selling_currency_id").getText());
			values.put("sellingCurrency", 						getElement("label_selling_currency_id").getText());
			values.put("subtotal", 								getElement("label_subtotal_id").getText());
			values.put("totalTaxes", 							getElement("label_total_taxes_id").getText());
			values.put("totalPackageCost", 						getElement("label_total_package_cost_id").getText());
			values.put("amountBnProcessednow", 					getElement("label_amount_bn_processed_now_id").getText());
			values.put("amountProcessedByAirline", 				getElement("label_amount_processed_by_airines_id").getText());
			values.put("amountDurAtCheckin", 					getElement("label_amount_due_at_checkin_id").getText());
			values.put("customerFirstname", 					getElement("label_customer_first_name_id").getText().replace(":", ""));
			values.put("customerLastName", 						getElement("label_customer_last_name_id").getText().replace(":", ""));
			values.put("customerAddressId", 					getElement("label_customer_address_id").getText().replace(":", ""));
			values.put("customerCountryId", 					getElement("label_customer_country_id").getText().replace(":", ""));
			values.put("customerPostalCode", 					getElement("label_customer_postal_code_id").getText().replace(":", ""));
			values.put("customerEmergencyNumber", 				getElement("label_customer_emergency_number_id").getText().replace(":", ""));
			values.put("cusotmerCity", 							getElement("label_customer_city_id").getText().replace(":", ""));
			values.put("customerState", 						getElement("label_customer_state_id").getText().replace(":", ""));
			values.put("customerPhoneNumber", 					getElement("label_customer_phone_number_id").getText().replace(":", ""));
			values.put("customerEmail", 						getElement("label_customer_email_id").getText().replace(":", ""));
			
			/*values.put("flightAdultTitle", 						getElement("label_flight_adult_title_id").getText());
			values.put("flightAdultFirstName", 					getElement("label_flight_adult_firstname_id").getText());
			values.put("flightAdultLastname", 					getElement("label_flight_adult_lastname_id").getText());
			values.put("flightAdultFrequentFlyer", 				getElement("label_flight_adult_frequent_flyer_id").getText());
			values.put("flightAdultFrequentFlyerNumber", 		getElement("label_flight_adult_frequent_flyer_number_id").getText());
			values.put("flightAdultEticket", 					getElement("label_flight_adult_eticket_id").getText());
			values.put("flightChildId", 						getElement("label_flight_child_title_id").getText());
			values.put("flightChildFirstName",	 				getElement("label_flight_child_firstname_id").getText());
			values.put("flightChildLastname", 					getElement("label_flight_child_lastname_id").getText());
			values.put("flightChildEticket", 					getElement("label_flight_child_eticket_id").getText());
			values.put("hotelAdultFirstName", 					getElement("label_hotel_adult_firstname_id").getText());
			values.put("hotelAdultTitle", 						getElement("label_hotel_adult_title_id").getText());
			values.put("hotelAdultLastname", 					getElement("label_hotel_adult_lastname_id").getText());
			values.put("hotelChildFirstname", 					getElement("label_hotel_child_firstname_id").getText());
			values.put("hotelChildTittle", 						getElement("label_hotel_child_title_id").getText());
			values.put("hotelChildLastName", 					getElement("label_hotel_child_lastname_id").getText());*/
			
			values.put("merchantTrackId", 						getElements("label_merchant_track_id_id").get(0).getText());
			values.put("authenticationReference", 				getElements("label_merchant_track_id_id").get(1).getText());
			values.put("paymentId", 							getElements("label_merchant_track_id_id").get(2).getText());
			values.put("paymentAmount", 						getElement("label_payment_details_amount_id").getText());
			values.put("paymentCurrency", 						getElement("label_payment_details_currency_xpath").getText());
			try {
				values.put("hotelCandeadline", 						getElement("label_hotel_canDeadline_id").getText());
			} catch (Exception e) {
				// TODO: handle exception
			}
			
			try {
				values.put("flightCanDeadline", 					getElement("label_flight_canDeadline1_id").getText());
			} catch (Exception e) {
				// TODO: handle exception
				try {
					values.put("flightCanDeadline", 					getElement("label_flight_canDeadline2_id").getText());
				} catch (Exception e2) {
					// TODO: handle exception
				}
			}
			
			try {
				values.put("packageCanDealine", 					getElement("label_package_canDealine_id").getText());
			} catch (Exception e) {
				// TODO: handle exception
			}
			
			System.out.println("Completed reading confirmation page");
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println(e);
		}
		return values;
	}
}
