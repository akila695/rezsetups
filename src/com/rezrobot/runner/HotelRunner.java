/**
 * 
 */
package com.rezrobot.runner;

import static org.junit.Assert.*;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.Alert;
import org.openqa.selenium.Capabilities;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.NetworkMode;
import com.rezrobot.common.objects.HotelObject;
import com.rezrobot.common.objects.Reservation;
import com.rezrobot.common.pojo.hotelDetails;
import com.rezrobot.core.PageObject;
import com.rezrobot.dataobjects.CommonBillingInfoRates;
import com.rezrobot.dataobjects.Common_verify_selected_products;
import com.rezrobot.dataobjects.HotelConfirmationPageDetails;
import com.rezrobot.dataobjects.HotelDetails;
import com.rezrobot.dataobjects.HotelOccupancyObject;
import com.rezrobot.dataobjects.HotelPaymentPageMoreDetails;
import com.rezrobot.dataobjects.HotelSearchObject;
import com.rezrobot.pages.CC.common.CC_Com_Admin_ConfigurationPage;
import com.rezrobot.pages.CC.common.CC_Com_Admin_Home;
import com.rezrobot.pages.CC.common.CC_Com_Admin_WebSiteDetails;
import com.rezrobot.pages.CC.common.CC_Com_BO_Menu;
import com.rezrobot.pages.CC.common.CC_Com_BookingConfirmation_criteria;
import com.rezrobot.pages.CC.common.CC_Com_BookingListReport_BookingCard;
import com.rezrobot.pages.CC.common.CC_Com_BookingListReport_criteria;
import com.rezrobot.pages.CC.common.CC_Com_BookingList_LiveBookingMoreInfo;
import com.rezrobot.pages.CC.common.CC_Com_BookingList_Report;
import com.rezrobot.pages.CC.common.CC_Com_Cancellation_Criteria;
import com.rezrobot.pages.CC.common.CC_Com_Cancellation_Report_criteria;
import com.rezrobot.pages.CC.common.CC_Com_ConfirmationMail_Report_hotel;
import com.rezrobot.pages.CC.common.CC_Com_Currency_Page;
import com.rezrobot.pages.CC.common.CC_Com_HomePage;
import com.rezrobot.pages.CC.common.CC_Com_LoginPage;
import com.rezrobot.pages.CC.common.CC_Com_ProfitAndLossReport_Criteria;
import com.rezrobot.pages.CC.common.CC_Com_ProfitAndLoss_Report;
import com.rezrobot.pages.CC.common.CC_Com_ReservationReport_criteria;
import com.rezrobot.pages.CC.common.CC_Com_Reservation_Report;
import com.rezrobot.pages.CC.common.CC_Com_ThirdPartySupplierPayable_Report;
import com.rezrobot.pages.CC.common.CC_Com_ThirdPartySupplierPayable_criteria;
import com.rezrobot.pages.CC.common.CC_Com_VoucherMail_Hotel;
import com.rezrobot.pages.CC.hotel.CC_Hotel_CancellationPage;
import com.rezrobot.pages.CC.hotel.CC_Hotel_Cancellation_Report;
import com.rezrobot.pages.CC.hotel.CC_Hotel_ConfirmationPage;
import com.rezrobot.pages.CC.hotel.CC_Hotel_PaymentPage;
import com.rezrobot.pages.CC.hotel.CC_Hotel_ResultsPage;
import com.rezrobot.pages.web.common.*;
import com.rezrobot.pages.web.hotel.WEB_Hotel_Paymentpage_occupancy_details;
import com.rezrobot.pages.web.hotel.Web_Hotel_ConfirmationPage;
import com.rezrobot.pages.web.hotel.Web_Hotel_paymentOccupancyPage;
import com.rezrobot.pages.web.hotel.Web_Hotel_resultsPage;
import com.rezrobot.pojo.Link;
import com.rezrobot.processor.HotelCancellationPolicyProcesser;
import com.rezrobot.processor.LabelReadProperties;
import com.rezrobot.types.UserType;
import com.rezrobot.utill.Calender;
import com.rezrobot.utill.DriverFactory;
import com.rezrobot.utill.ExcelReader;
import com.rezrobot.utill.ExtentReportTemplate;
import com.rezrobot.utill.HtmlReportTemplate;
import com.rezrobot.utill.PoiExcelReader;
import com.rezrobot.utill.ReportTemplate;
import com.rezrobot.utill.Serializer;
import com.rezrobot.utill.xmlPathGenerator;
import com.rezrobot.utill.xmlPathGeneratorConcurent;
import com.rezrobot.verifications.Hotel_Verify;
import com.rezrobot.xml_readers.GtaReader;
import com.rezrobot.xml_readers.HbReader;
import com.rezrobot.xml_readers.RezLiveReader;
import com.rezrobot.xml_readers.TouricoReader;

public class HotelRunner {

	private HashMap<String, String> PropertySet;
	private StringBuffer PrintWriter;
//	private ReportTemplate PrintTemplate;
	private String Browser = "N/A";
	//pages---------------------------------------------
	private Web_Com_HomePage 								home 							= new Web_Com_HomePage();
	private Web_Hotel_resultsPage 							resultsPage 					= new Web_Hotel_resultsPage();
	private Web_Com_CustomerDetailsPage 					cusDetails 						= new Web_Com_CustomerDetailsPage();
	private Web_Hotel_paymentOccupancyPage					occupancyPage 					= new Web_Hotel_paymentOccupancyPage();
	private Web_Com_BillingInformationPage					billinginfo						= new Web_Com_BillingInformationPage();
	private Web_Com_NotesPage								notes							= new Web_Com_NotesPage();
	private Web_Com_TermsPage								terms 							= new Web_Com_TermsPage();
	private WEB_Hotel_Paymentpage_occupancy_details 		paymentPageMoreDetails 			= new WEB_Hotel_Paymentpage_occupancy_details();
	private Web_Com_upselling								upselling						= new Web_Com_upselling();
	private Web_Com_Verify_selected_products				verifyProducts					= new Web_Com_Verify_selected_products();
	private Web_Com_PaymentGateway							pg								= new Web_Com_PaymentGateway();
	private Web_Hotel_ConfirmationPage						confirmationPage				= new Web_Hotel_ConfirmationPage();
	private CC_Com_LoginPage								login							= new CC_Com_LoginPage();
	private CC_Com_BO_Menu									bomenu							= new CC_Com_BO_Menu();
	private CC_Com_Currency_Page							currency						= new CC_Com_Currency_Page();
	private CC_Com_ReservationReport_criteria				resReportCriteria				= new CC_Com_ReservationReport_criteria();
	private CC_Com_Reservation_Report						resReport						= new CC_Com_Reservation_Report(); 
	private CC_Com_BookingListReport_criteria				bookingListCriteria				= new CC_Com_BookingListReport_criteria();
	private CC_Com_BookingList_Report						bookingList						= new CC_Com_BookingList_Report();
	private CC_Com_BookingList_LiveBookingMoreInfo			bookingListLiveBooking			= new CC_Com_BookingList_LiveBookingMoreInfo();
	private CC_Com_BookingListReport_BookingCard			bookingListBookingCard			= new CC_Com_BookingListReport_BookingCard();
	private CC_Com_ProfitAndLossReport_Criteria				profitAndLossCriteria			= new CC_Com_ProfitAndLossReport_Criteria();
	private CC_Com_ProfitAndLoss_Report						profitAndLossReport				= new CC_Com_ProfitAndLoss_Report();
	private CC_Com_ThirdPartySupplierPayable_criteria		thirdPartySupPayableCriteria	= new CC_Com_ThirdPartySupplierPayable_criteria();
	private CC_Com_ThirdPartySupplierPayable_Report			thirdPartySupPayableReport		= new CC_Com_ThirdPartySupplierPayable_Report();
	private CC_Com_BookingConfirmation_criteria 			confirmationMailCriteria		= new CC_Com_BookingConfirmation_criteria();
	private CC_Com_ConfirmationMail_Report_hotel			confirmationMail				= new CC_Com_ConfirmationMail_Report_hotel();					
	private CC_Com_Admin_WebSiteDetails						adminWebSiteDetails				= new CC_Com_Admin_WebSiteDetails();
	private CC_Com_VoucherMail_Hotel						voucher							= new CC_Com_VoucherMail_Hotel();
	private CC_Com_Cancellation_Criteria					cancellationCriteria			= new CC_Com_Cancellation_Criteria();
	private CC_Hotel_CancellationPage						cancellation					= new CC_Hotel_CancellationPage();
	private	CC_Com_Cancellation_Report_criteria				cancellationReportCriteria		= new CC_Com_Cancellation_Report_criteria();
	private CC_Com_HomePage									ccHome							= new CC_Com_HomePage();
	private CC_Hotel_ResultsPage							ccResultsPage					= new CC_Hotel_ResultsPage();
	private CC_Hotel_PaymentPage							ccPaymentPage					= new CC_Hotel_PaymentPage();
	private CC_Hotel_ConfirmationPage						ccConfirmationPage				= new CC_Hotel_ConfirmationPage();      
	private CC_Hotel_Cancellation_Report					cancellationReport				= new CC_Hotel_Cancellation_Report();
	
	
	//Extracted data
	private HotelConfirmationPageDetails 					confirmation 					= new HotelConfirmationPageDetails();
	private HotelDetails 									selectedHotel 					= new HotelDetails();
	
	//property files
	private LabelReadProperties 							labelProperty 					= new LabelReadProperties();
	private HashMap<String, String> 						portalSpecificData 				= labelProperty.getPortalSpecificData();
	private HashMap<String, String> 						commonLabels					= labelProperty.getCommonLabels();
	
	//excels
	private String											searchExcel						= portalSpecificData.get("searchExcel");
	private String 											customerExcel					= "Resources/excels/hotel/customer.xls"; // use property files
	private String											occupancyExcel					= portalSpecificData.get("occupancyExcel");
	private String											paymentExcel					= "Resources/excels/hotel/paymentDetails.xls"; // use property files
	
	//supplierwise readers
	private HbReader 										hb 								= new HbReader();
	private GtaReader 										inv27 							= new GtaReader();
	private TouricoReader 									inv13 							= new TouricoReader();
	private RezLiveReader									rezlive							= new RezLiveReader();
	
	//xml objects
	private hotelDetails 									hotelAvailabilityReq 			= new hotelDetails();
	private ArrayList<hotelDetails> 						hotelAvailabilityRes 			= new ArrayList<hotelDetails>();
	private hotelDetails 									roomAvailablityReq 				= new hotelDetails();
	private hotelDetails 									roomAvailablityRes 				= new hotelDetails();
	private hotelDetails 									preReservationReq 				= new hotelDetails();
	private hotelDetails 									preReservationRes 				= new hotelDetails();
	private hotelDetails 									reservationReq 					= new hotelDetails();
	private hotelDetails 									reservationRes 					= new hotelDetails();
	HashMap<String, String> 								paymentDetails      			= new HashMap<>(); // payment log details
	
	//extent report ---
	private 												ReportTemplate					PrintTemplate;
	private 												HtmlReportTemplate				HtmlReportTemplate;
	private 												ExtentReportTemplate 			ExtentReportTemplate;
	ExtentReports 							 				report 							= null;	
	
	//serialized object
	private HotelObject 									allDetails 						= new HotelObject();
	private Serializer										serialize						= new Serializer();
	private Reservation										hotelResObject					= new Reservation();
	
	@Before
	public void setUp() throws Exception {
		// logger = Logger.getLogger(this.getClass());
		// DOMConfigurator.configure("log4j.xml");
		/*PrintWriter = new StringBuffer();
		PrintTemplate = ReportTemplate.getInstance();
		PrintTemplate.Initialize(PrintWriter);*/
		DriverFactory.getInstance().initializeDriver();
		//String today = Calender.getToday("dd-MMM-yyyy");
		String today = Calender.getToday("dd-MMM-yyyy");
		String portal = portalSpecificData.get("Portal.Name");
		report = new ExtentReports("Reports/"+ portal+ "/Hotel-Reservation-"+ today +".html", NetworkMode.OFFLINE);
		PrintTemplate = ReportTemplate.getInstance();
		PrintTemplate.Initialize(/*PrintWriter, report*/);
		HtmlReportTemplate.getInstance().Initialize(PrintWriter);
		ExtentReportTemplate.getInstance().Initialize(report);
		
	}

	@Test
	public void test() throws Exception {
		System.out.println(portalSpecificData.get("hotelUsername"));
		System.out.println(portalSpecificData.get("searchExcel"));
		//get admin config details
		CC_Com_LoginPage login=new CC_Com_LoginPage();
		System.out.println(portalSpecificData.get("PortalUrlCC"));
		DriverFactory.getInstance().getDriver().get(portalSpecificData.get("PortalUrlCC").concat(portalSpecificData.get("login")));
		login.getPortalHeader();
		
		CC_Com_Admin_Home 					adminHome 		= 	(CC_Com_Admin_Home) login.loginAs(UserType.ADMIN, portalSpecificData.get("adminUsername"), portalSpecificData.get("adminPassword"));
		CC_Com_Admin_ConfigurationPage 		configpage 		= 	adminHome.getConfigurationScreen(portalSpecificData.get("PortalUrlCC"));
		HashMap<String, String> 			configDetails 	= 	configpage.getConfigProperties();
																adminWebSiteDetails.getWebSiteDetailsPage(portalSpecificData.get("PortalUrlCC"));
		ArrayList<HashMap<String, String>> 	websiteDetails 	= 	adminWebSiteDetails.getDetails();
		portalSpecificData = labelProperty.changePortalSpecificDetails(configDetails, portalSpecificData);
		//change portal specific details
		
		/*System.out.println("abc");
		xmlPathGeneratorConcurent abc1 = new xmlPathGeneratorConcurent("1");
		abc1.start();
		xmlPathGeneratorConcurent abc2 = new xmlPathGeneratorConcurent("2");
		abc2.start();
		abc1.run();
		abc2.run();*/
		
//		PrintTemplate.markReportBegining("Rezgateway WEB Hotel Flow Smoke Test");
//		PrintTemplate.setInfoTableHeading("Test Basic Details");
		
		Capabilities caps = ((RemoteWebDriver) DriverFactory.getInstance().getDriver()).getCapabilities();
		
		//currency details
		String defaultCC = "";
		HashMap<String, String> 						currencyMap  			= new HashMap<>();
		
		//verify
		Hotel_Verify 									verify 					= new Hotel_Verify();
		
		//get xml property details
		xmlPathGenerator 								xmlUrlGenerator 		= new xmlPathGenerator();
		String 											url 					= "";
		
		String 											canDeadline 			= "";
		Browser = caps.getBrowserName();
		/*PrintTemplate.addToInfoTabel("Browser Type", caps.getBrowserName());
		PrintTemplate.addToInfoTabel("Browser Version", caps.getVersion());
		PrintTemplate.addToInfoTabel("Platform", caps.getPlatform().toString());
		PrintTemplate.markTableEnd();*/
		
		Map<String, String> info = new HashMap<String, String>();
		info.put("BrowserType", caps.getBrowserName());
		info.put("BrowserVersion", caps.getVersion());
		info.put("Platform", caps.getPlatform().toString());
		ReportTemplate.getInstance().setSystemInfo(info);

		
		
		//ExcelReader 									newReader 				= 	new ExcelReader();
		PoiExcelReader									newReader				= 	new PoiExcelReader();
		FileInputStream 								stream 					=	new FileInputStream(new File(searchExcel));
		ArrayList<Map<Integer, String>> 				ContentList 			= 	newReader.reader(stream);
		Map<Integer, String>  							content 				= 	ContentList.get(0);
		Iterator<Map.Entry<Integer, String>>   			mapiterator 			= 	content.entrySet().iterator();
		
		
		while(mapiterator.hasNext())
		{
			try {
				HashMap<String, String> 				xmlUrls 				= 	new HashMap<>();
				Map.Entry<Integer, String> 				Entry 					= 	(Map.Entry<Integer, String>)mapiterator.next();
				String 									Content 				= 	Entry.getValue();
				String[] 								searchDetails 			= 	Content.split(",");
				HotelSearchObject						searchObject			= 	new HotelSearchObject();
				HotelSearchObject 						search 					= 	searchObject.setData(searchDetails);
				if(search.getExecuteStatus().equals("yes"))
				{
					//log and get currency details
					DriverFactory.getInstance().getDriver().get(portalSpecificData.get("PortalUrlCC").concat(portalSpecificData.get("login")));
//					PrintTemplate.setTableHeading("Check login page availability");
					try {
						PrintTemplate.verifyTrue(true, login.isLoginPageAvailable(), "Hotel Login page Availability");
					} catch (Exception e) {
						// TODO: handle exception
						System.out.println(e);
					}
					
					
					ArrayList<HotelOccupancyObject> occupancyList = new ArrayList<HotelOccupancyObject>();
					newReader 		= 	new PoiExcelReader();
					stream 			=	new FileInputStream(new File(occupancyExcel));
					ContentList 	= 	new ArrayList<Map<Integer,String>>();
					ContentList 	= 	newReader.reader(stream);
					content 		= 	new HashMap<Integer, String>();
					content   		= 	ContentList.get(0);
					Iterator<Map.Entry<Integer, String>>   	occupancyIterator 	= 	content.entrySet().iterator();
					
					while(occupancyIterator.hasNext())
					{
						 	Entry 						= 	(Map.Entry<Integer, String>)occupancyIterator.next();
							Content 					= 	Entry.getValue();
							String[] occupancyDetails 	= 	Content.split(",");
							HotelOccupancyObject occ 	= 	new HotelOccupancyObject();
							occupancyList.add(occ.setData(occupancyDetails));	
					}
					
					if(login.isLoginPageAvailable())
					{
						if(search.getOnlineOrOffline().equals("offline"))
						{
							portalSpecificData.put("portal.ccfee", "0");
						}	
						login.typeUserName(portalSpecificData.get("hotelUsername"));
						login.typePassword(portalSpecificData.get("hotelPassword"));
						login.loginSubmit();
						//CC_Com_Admin_Home 				internalHome 		= (CC_Com_Admin_Home) login.loginAs(UserType.INTERNAL, portalSpecificData.get("hotelUsername"), portalSpecificData.get("hotelPassword"));
						
//						PrintTemplate.setTableHeading("Check BO menu page availability");
						PrintTemplate.verifyTrue(true, bomenu.isModuleImageAvailable(), "Hotel Bo menu page Availability");
						if(bomenu.isModuleImageAvailable())
						{
							//get currency details
							DriverFactory.getInstance().getDriver().get(portalSpecificData.get("PortalUrlCC").concat(portalSpecificData.get("currencyUrl")));
							currencyMap = currency.getCCDetails();
							defaultCC = currency.getDefaultCurrecny();
							
							if(search.getChannel().equals("web"))
							{
								DriverFactory.getInstance().getDriver().get(portalSpecificData.get("PortalUrlWeb"));
								if (home.isPageAvailable()) 
								{	
									verify.webHomePage(PrintTemplate, home, labelProperty, search);
									home.searchForHotels(search); // enter search criteria
									home.performTheSearch(); // click search button
									
									HashMap<String, String> labelPropertyMap = labelProperty.getHotelLabelProperties();
									
//									PrintTemplate.setTableHeading("Results Page Verifications");
									PrintTemplate.verifyTrue(true, resultsPage.isHotelResultsPageAvailable(), "Hotel Results Page Availability");
									if(resultsPage.isHotelResultsPageAvailable())
									{
										PrintTemplate.verifyTrue(true, resultsPage.isHotelResultsPageAvailable(), "Results availability verification");
//										PrintTemplate.markTableEnd();
										if(resultsPage.isHotelResultsAvailable())
										{
// temp - to reduce time (comment the line)
//											verify.webResultsPage(PrintTemplate, resultsPage, labelProperty);
											
											// get all hotel details
											ArrayList<HotelDetails> hotelList = resultsPage.getHotelDetails();
											
											// select and get hotel details
											selectedHotel = resultsPage.getSelectedHotelInfo(search, hotelList, labelProperty, PrintTemplate, portalSpecificData);
											
											// get availability xmls
											if(search.getSupplier().equals("hotelbeds_v2"))
											{
												//get availability request
												try {
													url = xmlUrlGenerator.returnUrl(portalSpecificData, search.getSupplier(), "AvailabilityRequest", selectedHotel.getTracerID(), "hotel");
													System.out.println("availability request - ".concat(url));
													hotelAvailabilityReq = hb.availabilityReq(url);
													xmlUrls.put("Availability request", url);
												} catch (Exception e) {
													// TODO: handle exception
													System.out.println("Availability request - " + e);
												}
												
												//get availability response	
												try {
													url			= xmlUrlGenerator.returnUrl(portalSpecificData, search.getSupplier(), "AvailabilityResponse", selectedHotel.getTracerID(), "hotel");	
													System.out.println("availability response - ".concat(url));
													hotelAvailabilityRes = hb.availabilityRes(url);
													xmlUrls.put("Availability response", url);
												} catch (Exception e) {
													// TODO: handle exception
													System.out.println("Availability response - " + e);
												}
											}
											else if(search.getSupplier().equals("INV27"))
											{
												//get availability request
												try {
													url = xmlUrlGenerator.returnUrl(portalSpecificData, search.getSupplier(), "AvailabilityRequest", selectedHotel.getTracerID(), "hotel");
													System.out.println("availability request - ".concat(url));
													hotelAvailabilityReq = inv27.availabilityReq(url);
													xmlUrls.put("Availability request", url);
												} catch (Exception e) {
													// TODO: handle exception
													System.out.println("Availability request - " + e);
												}
												
												
												//get availability response	
												try {
													url			= xmlUrlGenerator.returnUrl(portalSpecificData, search.getSupplier(), "AvailabilityResponse", selectedHotel.getTracerID(), "hotel");
													System.out.println("availability response - ".concat(url));
													hotelAvailabilityRes = inv27.availabilityRes(url);
													xmlUrls.put("Availability response", url);
												} catch (Exception e) {
													// TODO: handle exception
													System.out.println("Availability response - " + e);
												}	
											}
											else if(search.getSupplier().equals("rezlive"))
											{
												//get availability request
												try {
													url = xmlUrlGenerator.returnUrl(portalSpecificData, search.getSupplier(), "AvailabilityRequest", selectedHotel.getTracerID(), "hotel");
													System.out.println("availability request - ".concat(url));
													hotelAvailabilityReq = rezlive.availabilityReq(url);
													xmlUrls.put("Availability request", url);
												} catch (Exception e) {
													// TODO: handle exception
													System.out.println("Availability request - " + e);
												}	
												
												//get availability response	
												try {
													url			= xmlUrlGenerator.returnUrl(portalSpecificData, search.getSupplier(), "AvailabilityResponse", selectedHotel.getTracerID(), "hotel");
													System.out.println("availability response - ".concat(url));
													hotelAvailabilityRes = rezlive.availabilityRes(url);
													xmlUrls.put("Availability response", url);
												} catch (Exception e) {
													// TODO: handle exception
													System.out.println("Availability response - " + e);
												}	
											}
											
											
											
											//get all apps simultaneously
											ArrayList<String> xmlurls = new ArrayList<String>();
											ArrayList<xmlPathGeneratorConcurent> generator = new ArrayList<xmlPathGeneratorConcurent>();
											xmlPathGeneratorConcurent generator1 = new xmlPathGeneratorConcurent();
											System.out.println(selectedHotel.getTracerID());
											if(portalSpecificData.get("portal.NumberofxmlApps").equals("1"))
											{
												
											}
											else
											{
												for(int i = 1 ; i <= Integer.parseInt(portalSpecificData.get("portal.NumberofxmlApps")) ; i++)
												{
													try {
														generator.get(i).run(portalSpecificData.get("hotelXmlHomeUrl1.".concat(String.valueOf(i))), portalSpecificData.get("hotelXmlHomeUrl2.".concat(String.valueOf(i))), search.getSupplier(), "AvailabilityRequest", selectedHotel.getTracerID(), "hotel", portalSpecificData);
													} catch (Exception e) {
														// TODO: handle exception
														System.out.println(e);
													}
												}
											}
											
											//verify availability req against entered details
											verify.availabilityReqAgainstEnstredDate(PrintTemplate, hotelAvailabilityReq, search);
											
											//verify availability res against results
											verify.availabilityResAgainstResults(PrintTemplate, hotelList, hotelAvailabilityRes, search);
											
											// verify selected hotel
											verify.availabilityResAgainstSelectedHotel(PrintTemplate, hotelAvailabilityRes, selectedHotel, currencyMap, defaultCC, portalSpecificData);
											
			//**************************************************payment page*********************************************************************								
											//get room availability xmls
											if(search.getSupplier().equals("hotelbeds_v2"))
											{
												//get room availability request
												try {
													url			= xmlUrlGenerator.returnUrl(portalSpecificData, search.getSupplier(), "RoomAvailabilityRequest", selectedHotel.getTracerID(), "hotel");
													System.out.println("room availability request - ".concat(url));
													roomAvailablityReq = hb.roomAvailabilityReq(url);
													xmlUrls.put("Room availability request", url);
												} catch (Exception e) {
													// TODO: handle exception
													System.out.println("Room availability request - " + e);
												}
												
												
												//get room availability response
												try {
													url			= xmlUrlGenerator.returnUrl(portalSpecificData, search.getSupplier(), "RoomAvailabilityResponse", selectedHotel.getTracerID(), "hotel");
													System.out.println("room availability response - ".concat(url));
													roomAvailablityRes = hb.roomAvailabilityRes(url);
													xmlUrls.put("Room availability response", url);
												} catch (Exception e) {
													// TODO: handle exception
													System.out.println("Room availability response - " + e);
												}
												 
											}
											else if(search.getSupplier().equals("INV27"))
											{
												//get room availability request
												try {
													url			= xmlUrlGenerator.returnUrl(portalSpecificData, search.getSupplier(), "RoomAvailabilityRequest", selectedHotel.getTracerID(), "hotel");	
													System.out.println("room availability request - ".concat(url));
													roomAvailablityReq = inv27.roomAvailabilityReq(url); 
													xmlUrls.put("Room availability request", url);
												} catch (Exception e) {
													// TODO: handle exception
													System.out.println("Room availability request - " + e);
												}
												
												
												//get room availability response
												try {
													url			= xmlUrlGenerator.returnUrl(portalSpecificData, search.getSupplier(), "RoomAvailabilityResponse", selectedHotel.getTracerID(), "hotel");	
													System.out.println("room availability response - ".concat(url));
													roomAvailablityRes = inv27.roomAvailabilityRes(url); 
													xmlUrls.put("Room availability response", url);
												} catch (Exception e) {
													// TODO: handle exception
													System.out.println("Room availability response - " + e);
												}
												
											}
											else if(search.getSupplier().equals("rezlive"))
											{
												//get room availability request
												try {
													url			= xmlUrlGenerator.returnUrl(portalSpecificData, search.getSupplier(), "RoomAvailabilityRequest", selectedHotel.getTracerID(), "hotel");	
													System.out.println("room availability request - ".concat(url));
													roomAvailablityReq = rezlive.roomAvailabilityReq(url); 
													xmlUrls.put("Room availability request", url);
												} catch (Exception e) {
													// TODO: handle exception
													System.out.println("Room availability request - " + e);
												}
												
												
												//get room availability response
												try {
													url			= xmlUrlGenerator.returnUrl(portalSpecificData, search.getSupplier(), "RoomAvailabilityResponse", selectedHotel.getTracerID(), "hotel");	
													System.out.println("room availability response - ".concat(url));
													roomAvailablityRes = rezlive.roomAvailabilityRes(url); 
													xmlUrls.put("Room availability response", url);
												} catch (Exception e) {
													// TODO: handle exception
													System.out.println("Room availability response - " + e);
												}
												
											}
											
											
											//get cancellation deadline
											HotelCancellationPolicyProcesser canDeadlineCreator = new HotelCancellationPolicyProcesser();
											canDeadline = canDeadlineCreator.getCancellationDeadline(search.getSupplier(), roomAvailablityRes);
											
											
											// get payment page more details
											HotelPaymentPageMoreDetails ppMoreDetails = paymentPageMoreDetails.getDetails(search);
											
											//verify selected hotel details
											verify.selectedHotelAgainstAvailabliltyAndRoomAvailabilityResponses(selectedHotel, hotelAvailabilityRes, roomAvailablityRes, PrintTemplate, search, portalSpecificData, currencyMap, defaultCC);
											
											
											//verify payment page more details
											verify.paymentPageMoreDetailsVerification(ppMoreDetails, roomAvailablityRes, PrintTemplate, defaultCC, currencyMap, portalSpecificData, canDeadline, search);
											
//											PrintTemplate.setTableHeading("Payment Page Verifications - Customer Details");
											PrintTemplate.verifyTrue(true, cusDetails.isCustomerDetailsAvailable(), "Check customer details Availability");
//											PrintTemplate.markTableEnd();
											
											if(cusDetails.isCustomerDetailsAvailable())
											{
												verify.webCustomerDetails(PrintTemplate, cusDetails, labelPropertyMap, search);
											//	upselling.checkHotelUpSelling(search);
											//	upselling.checkFlightUpSelling();
											//	upselling.checkActivityUpSelling();
												cusDetails.enterCustomerDetails(portalSpecificData, "hotel");
												cusDetails.proceedToOccupancy();
											}
											else
											{
//												PrintTemplate.markTableEnd();
											}
											// payment page -> occupancy details
//											PrintTemplate.setTableHeading("Payment Page Verifications - Occupancy Details");
											PrintTemplate.verifyTrue(true, occupancyPage.isPaymentPageOccupancyDetailsAvailable(), "Check occupancy details Availability");
//											PrintTemplate.markTableEnd();
											
											if(occupancyPage.isPaymentPageOccupancyDetailsAvailable())
											{
												
												verify.webOccupancyDetails(PrintTemplate, occupancyPage, labelPropertyMap, portalSpecificData, roomAvailablityRes);
												occupancyPage.enterHotelOccupancyDetails(occupancyList);
												occupancyPage.next();
											}
											else
											{
//												PrintTemplate.markTableEnd();
											}
											
											//billing information
											
//											PrintTemplate.setTableHeading("Payment Page Verifications - Billing information");
											PrintTemplate.verifyTrue(true, billinginfo.isBillingInformationsAvailable(), "Check billing information Availability");
//											PrintTemplate.markTableEnd();
											
											if(billinginfo.isBillingInformationsAvailable())
											{
												CommonBillingInfoRates rates = billinginfo.getRates();
												verify.webBillingInfo(PrintTemplate, billinginfo, labelPropertyMap, rates, roomAvailablityRes, portalSpecificData, defaultCC, currencyMap);
												billinginfo.fillSpecialNotes();
												
												
											}
											// Check notes availability
//											PrintTemplate.setTableHeading("Payment Page Verifications - Notes");
											PrintTemplate.verifyTrue(true, notes.isSpecialNotesAvailable(), "Check notes information Availability");
//											PrintTemplate.markTableEnd();
											if(notes.isSpecialNotesAvailable())
											{
												verify.webNotes(PrintTemplate, notes, labelPropertyMap);
												notes.enterHotelNotes(portalSpecificData);
												notes.loadTermsandConditions();
											}
											// Check terms and conditions availability
//											PrintTemplate.setTableHeading("Payment Page Verifications - Terms and conditions");
											PrintTemplate.verifyTrue(true, terms.isTermsAndConditionsAvailable(), "Check terms and conditions Availability");
//											PrintTemplate.markTableEnd();
											
											if(terms.isTermsAndConditionsAvailable())
											{
												terms.getTCMandatoryInputFields();
												terms.checkAvailability();
												verify.webTermsAndConditions(PrintTemplate, terms, labelPropertyMap, canDeadline, portalSpecificData);
												
												
											}
											
											// Check verify selected products availability
//											PrintTemplate.setTableHeading("Payment Page Verifications - Verify selected products");
											PrintTemplate.verifyTrue(true, verifyProducts.isVerifyProductsAvailable(), "Check Verify selected products Availability");
//											PrintTemplate.markTableEnd();
											System.out.println("Verify products");
											ArrayList<Common_verify_selected_products> productList = new ArrayList<Common_verify_selected_products>();
											Thread.sleep(5000);
											if(verifyProducts.isVerifyProductsAvailable())
											{
											//	productList = verifyProducts.getDetails(labelProperty);
											}
											try {
												verifyProducts.continueToPG();	
												Thread.sleep(5000);
											} catch (Exception e) {
												// TODO: handle exception
											}
													
											// Check pg availability
											if(search.getOnlineOrOffline().equals("online"))
											{
												System.out.println("Payment Page Check payment gateway");
												PrintTemplate.verifyTrue(true, pg.isPaymentGatewayLoaded(configDetails), "Check payment gateway Availability");
												if(pg.isPaymentGatewayLoaded(configDetails))
												{
													try {		
														pg.enterCardDetails(portalSpecificData);
														pg.confirm();
													} catch (Exception e) {
														// TODO: handle exception
														System.out.println(e);				
													}	
												}
											}
											
											
											// Check confirmation page availability
//											PrintTemplate.setTableHeading("Confirmation page verification");
											PrintTemplate.verifyTrue(true, confirmationPage.isAvailable(), "Check confirmation page Availability");
//											PrintTemplate.markTableEnd();
											
											if(confirmationPage.isAvailable())
											{
												// verify confirmation page labels
												HashMap<String, String> confirmationPageLabels = confirmationPage.getHotelLabels(selectedHotel, search);
												verify.confirmationPagelabels(confirmationPageLabels, labelPropertyMap, PrintTemplate, portalSpecificData, canDeadline);
												
												//get confirmation page information
												confirmation =confirmationPage.getInfo(search, selectedHotel);
												
												//get pre reservation xmls
												if(search.getSupplier().equals("hotelbeds_v2"))
												{
													//get pre reservation request
													try {
														url			= xmlUrlGenerator.returnUrl(portalSpecificData, search.getSupplier(), "PreReservationRequest", selectedHotel.getTracerID(), "hotel");
														System.out.println("pre reservation request - ".concat(url));
														preReservationReq = hb.preReservationReq(url);
														xmlUrls.put("Pre reservation request", url);
													} catch (Exception e) {
														// TODO: handle exception
														System.out.println("Pre reservation request - " + e);
													}
													
													
													//get pre reservation response
													try {
														url			= xmlUrlGenerator.returnUrl(portalSpecificData, search.getSupplier(), "PreReservationResponse", selectedHotel.getTracerID(), "hotel");	
														System.out.println("pre reservation response - ".concat(url));
														preReservationRes = hb.preReservationResponse(url);
														xmlUrls.put("Pre reservation response", url);
													} catch (Exception e) {
														// TODO: handle exception
														System.out.println("Pre reservation response - " + e);
													}
													
												}
												else if(search.getSupplier().equals("INV27"))
												{
													//get pre reservation request
													try {
														url			= xmlUrlGenerator.returnUrl(portalSpecificData, search.getSupplier(), "PreReservationRequest", selectedHotel.getTracerID(), "hotel");
														System.out.println("pre reservation request - ".concat(url));
														preReservationReq = inv27.preReservationReq(url);
														xmlUrls.put("Pre reservation request", url);
													} catch (Exception e) {
														// TODO: handle exception
														System.out.println("Pre reservation request - " + e);
													}
													
													//get pre reservation response
													try {
														url			= xmlUrlGenerator.returnUrl(portalSpecificData, search.getSupplier(), "PreReservationResponse", selectedHotel.getTracerID(), "hotel");
														System.out.println("pre reservation response - ".concat(url));
														preReservationRes = inv27.preReservationResponse(url);
														xmlUrls.put("Pre reservation response", url);
													} catch (Exception e) {
														// TODO: handle exception
														System.out.println("Pre reservation response - " + e);
													}
													
												}
												else if(search.getSupplier().equals("rezlive"))
												{
													//get pre reservation request
													try {
														url			= xmlUrlGenerator.returnUrl(portalSpecificData, search.getSupplier(), "PreReservationRequest", selectedHotel.getTracerID(), "hotel");
														System.out.println("pre reservation request - ".concat(url));
														preReservationReq = rezlive.preReservationReq(url);
														xmlUrls.put("Pre reservation request", url);
													} catch (Exception e) {
														// TODO: handle exception
														System.out.println("Pre reservation request - " + e);
													}
													
													//get pre reservation response
													try {
														url			= xmlUrlGenerator.returnUrl(portalSpecificData, search.getSupplier(), "PreReservationResponse", selectedHotel.getTracerID(), "hotel");
														System.out.println("pre reservation response - ".concat(url));
														preReservationRes = rezlive.preReservationRes(url);
														xmlUrls.put("Pre reservation response", url);
													} catch (Exception e) {
														// TODO: handle exception
														System.out.println("Pre reservation response - " + e);
													}
													
												}
												
												
												//get reservation xmls
												if(search.getSupplier().equals("hotelbeds_v2"))
												{
													//get reservation request
													try {
														url			= xmlUrlGenerator.returnUrl(portalSpecificData, search.getSupplier(), "ReservationRequest", selectedHotel.getTracerID(), "hotel");
														System.out.println("reservation request - ".concat(url));
														reservationReq = hb.reservationReq(url);
														xmlUrls.put("Reservation request", url);
													} catch (Exception e) {
														// TODO: handle exception
														System.out.println("Reservation request - " + e);
													}	
													
													
													//get reservation response
													try {
														url			= xmlUrlGenerator.returnUrl(portalSpecificData, search.getSupplier(), "ReservationResponse", selectedHotel.getTracerID(), "hotel");
														System.out.println("reservation response - ".concat(url));
														reservationRes = hb.reservationResponse(url);
														xmlUrls.put("Reservation response", url);
													} catch (Exception e) {
														// TODO: handle exception
														System.out.println("Reservation response - " + e);
													}
													
												}
												else if(search.getSupplier().equals("INV27"))
												{
													//get reservation request
													try {
														url			= xmlUrlGenerator.returnUrl(portalSpecificData, search.getSupplier(), "ReservationRequest", selectedHotel.getTracerID(), "hotel");
														System.out.println("reservation request - ".concat(url));
														reservationReq = inv27.reservationReq(url);
														xmlUrls.put("Reservation request", url);
													} catch (Exception e) {
														// TODO: handle exception
														System.out.println("Reservation request - " + e);
													}	
													
													
													//get reservation response
													try {
														url			= xmlUrlGenerator.returnUrl(portalSpecificData, search.getSupplier(), "ReservationResponse", selectedHotel.getTracerID(), "hotel");	
														System.out.println("reservation response - ".concat(url));
														reservationRes = inv27.reservationRes(url);
														xmlUrls.put("Reservation response", url);
													} catch (Exception e) {
														// TODO: handle exception
														System.out.println("Reservation response - " + e);
													}
												}
												else if(search.getSupplier().equals("rezlive"))
												{
													//get reservation request
													try {
														url			= xmlUrlGenerator.returnUrl(portalSpecificData, search.getSupplier(), "ReservationRequest", selectedHotel.getTracerID(), "hotel");
														System.out.println("reservation request - ".concat(url));
														reservationReq = rezlive.reservationReq(url);
														xmlUrls.put("Reservation request", url);
													} catch (Exception e) {
														// TODO: handle exception
														System.out.println("Reservation request - " + e);
													}	
													
													
													//get reservation response
													try {
														url			= xmlUrlGenerator.returnUrl(portalSpecificData, search.getSupplier(), "ReservationResponse", selectedHotel.getTracerID(), "hotel");	
														System.out.println("reservation response - ".concat(url));
														reservationRes = rezlive.reservationRes(url);
														xmlUrls.put("Reservation response", url);
													} catch (Exception e) {
														// TODO: handle exception
														System.out.println("Reservation response - " + e);
													}	
												}
												
												//read payment log
												try {
													paymentDetails = xmlUrlGenerator.getPaymentLog(portalSpecificData.get("portal.PaymentLog"), confirmation.getTransactionID(), portalSpecificData);
												} catch (Exception e) {
													// TODO: handle exception
													System.out.println("Payment log error - " + e);
												}
												
												//set serialized details
												String today = Calender.getToday("dd-MMM-yyyy");
												allDetails.setReportDate(today);
												allDetails.setSearch(search);
												allDetails.setSelectedHotel(selectedHotel);
												allDetails.setPortalSpecificData(portalSpecificData);
												allDetails.setHotelLabels(labelPropertyMap);
												allDetails.setCommonLabels(commonLabels);
												allDetails.setReservationRes(reservationRes);
												allDetails.setPaymentDetails(paymentDetails);
												allDetails.setConfigDetails(configDetails);
												allDetails.setWebsiteDetails(websiteDetails);
												allDetails.setCanDeadline(canDeadline);
												allDetails.setDefaultCurrency(defaultCC);
												allDetails.setCurrencyMap(currencyMap);
												allDetails.setConfirmation(confirmation);
												allDetails.setOccupancyList(occupancyList);
												allDetails.setRoomAvailablityRes(roomAvailablityRes);
												allDetails.setHotelAvailabilityRes(hotelAvailabilityRes);
												allDetails.setConfirmed(confirmationPage.isAvailable());
												hotelResObject.setProduct("Hotel");
												hotelResObject.setHotelReservation(allDetails);
												
												serialize.SerializeReservation(hotelResObject, portalSpecificData.get("Portal.Name"), "../Rezrobot_Details/object/");
												
												
												// verify the confirmation page
												verify.confirmationPageAgainsrReservationResponse(PrintTemplate, confirmation, reservationRes, portalSpecificData, currencyMap, defaultCC, occupancyList, search, canDeadline, paymentDetails, hotelAvailabilityRes);		
									
												checkReports(verify, currencyMap, occupancyList, search, defaultCC, today, canDeadline, websiteDetails, xmlUrls);
												/*PrintTemplate.setInfoTableHeading("Xml urls");
												PrintTemplate.addToInfoTabel("Availability request", 		xmlUrls.get("Availability request"));
												PrintTemplate.addToInfoTabel("Availability response", 		xmlUrls.get("Availability response"));
												PrintTemplate.addToInfoTabel("Room availability request", 	xmlUrls.get("Room availability request"));
												PrintTemplate.addToInfoTabel("Room availability response", 	xmlUrls.get("Room availability response"));
												PrintTemplate.addToInfoTabel("Pre reservation request", 	xmlUrls.get("Pre reservation request"));
												PrintTemplate.addToInfoTabel("Pre reservation response", 	xmlUrls.get("Pre reservation response"));
												PrintTemplate.addToInfoTabel("Reservation request", 		xmlUrls.get("Reservation request"));
												PrintTemplate.addToInfoTabel("Reservation response", 		xmlUrls.get("Reservation response"));
												PrintTemplate.markTableEnd();*/
												info.clear();
												info.put("Availability request", xmlUrls.get("Availability request"));
												info.put("Availability response", xmlUrls.get("Availability response"));
												info.put("Room availability request", xmlUrls.get("Room availability request"));
												info.put("Room availability response", xmlUrls.get("Room availability response"));
												info.put("Pre reservation request", xmlUrls.get("Pre reservation request"));
												info.put("Pre reservation response", xmlUrls.get("Pre reservation response"));
												info.put("Reservation request", xmlUrls.get("Reservation request"));
												info.put("Reservation response", xmlUrls.get("Reservation response"));
												ReportTemplate.getInstance().setInfoTable("Xml urls", info);
												if(paymentDetails.containsKey("errorStatus"))
												{
													/*PrintTemplate.setInfoTableHeading("Payment log error");
													PrintTemplate.addToInfoTabel("Status", paymentDetails.get("errorStatus"));
													PrintTemplate.addToInfoTabel("Error", paymentDetails.get("errorDescription"));
													PrintTemplate.markTableEnd();*/
													info.clear();
													info.put("Status", paymentDetails.get("errorStatus"));
													info.put("Error", paymentDetails.get("errorDescription"));
													ReportTemplate.getInstance().setInfoTable("Payment log error", info);
												}
											}
											else
											{
												terms.getError(PrintTemplate);
												try {
													
												} catch (Exception e) {
													// TODO: handle exception
												}
												//get pre reservation xmls
												if(search.getSupplier().equals("hotelbeds_v2"))
												{
													//get pre reservation request
													try {
														url			= xmlUrlGenerator.returnUrl(portalSpecificData, search.getSupplier(), "PreReservationRequest", selectedHotel.getTracerID(), "hotel");
														System.out.println("pre reservation request - ".concat(url));
														preReservationReq = hb.preReservationReq(url);
														xmlUrls.put("Pre reservation request", url);
													} catch (Exception e) {
														// TODO: handle exception
														System.out.println("Pre reservation request - " + e);
													}
													
													
													//get pre reservation response
													try {
														url			= xmlUrlGenerator.returnUrl(portalSpecificData, search.getSupplier(), "PreReservationResponse", selectedHotel.getTracerID(), "hotel");	
														System.out.println("pre reservation response - ".concat(url));
														preReservationRes = hb.preReservationResponse(url);
														xmlUrls.put("Pre reservation response", url);
													} catch (Exception e) {
														// TODO: handle exception
														System.out.println("Pre reservation response - " + e);
													}
													
												}
												else if(search.getSupplier().equals("INV27"))
												{
													//get pre reservation request
													try {
														url			= xmlUrlGenerator.returnUrl(portalSpecificData, search.getSupplier(), "PreReservationRequest", selectedHotel.getTracerID(), "hotel");
														System.out.println("pre reservation request - ".concat(url));
														preReservationReq = inv27.preReservationReq(url);
														xmlUrls.put("Pre reservation request", url);
													} catch (Exception e) {
														// TODO: handle exception
														System.out.println("Pre reservation request - " + e);
													}
													
													//get pre reservation response
													try {
														url			= xmlUrlGenerator.returnUrl(portalSpecificData, search.getSupplier(), "PreReservationResponse", selectedHotel.getTracerID(), "hotel");
														System.out.println("pre reservation response - ".concat(url));
														preReservationRes = inv27.preReservationResponse(url);
														xmlUrls.put("Pre reservation response", url);
													} catch (Exception e) {
														// TODO: handle exception
														System.out.println("Pre reservation response - " + e);
													}
													
												}
												else if(search.getSupplier().equals("rezlive"))
												{
													//get pre reservation request
													try {
														url			= xmlUrlGenerator.returnUrl(portalSpecificData, search.getSupplier(), "PreReservationRequest", selectedHotel.getTracerID(), "hotel");
														System.out.println("pre reservation request - ".concat(url));
														preReservationReq = rezlive.preReservationReq(url);
														xmlUrls.put("Pre reservation request", url);
													} catch (Exception e) {
														// TODO: handle exception
														System.out.println("Pre reservation request - " + e);
													}
													
													//get pre reservation response
													try {
														url			= xmlUrlGenerator.returnUrl(portalSpecificData, search.getSupplier(), "PreReservationResponse", selectedHotel.getTracerID(), "hotel");
														System.out.println("pre reservation response - ".concat(url));
														preReservationRes = rezlive.preReservationRes(url);
														xmlUrls.put("Pre reservation response", url);
													} catch (Exception e) {
														// TODO: handle exception
														System.out.println("Pre reservation response - " + e);
													}
													
												}
												
												
												//get reservation xmls
												if(search.getSupplier().equals("hotelbeds_v2"))
												{
													//get reservation request
													try {
														url			= xmlUrlGenerator.returnUrl(portalSpecificData, search.getSupplier(), "ReservationRequest", selectedHotel.getTracerID(), "hotel");
														System.out.println("reservation request - ".concat(url));
														reservationReq = hb.reservationReq(url);
														xmlUrls.put("Reservation request", url);
													} catch (Exception e) {
														// TODO: handle exception
														System.out.println("Reservation request - " + e);
													}	
													
													
													//get reservation response
													try {
														url			= xmlUrlGenerator.returnUrl(portalSpecificData, search.getSupplier(), "ReservationResponse", selectedHotel.getTracerID(), "hotel");
														System.out.println("reservation response - ".concat(url));
														reservationRes = hb.reservationResponse(url);
														xmlUrls.put("Reservation response", url);
													} catch (Exception e) {
														// TODO: handle exception
														System.out.println("Reservation response - " + e);
													}
													
												}
												else if(search.getSupplier().equals("INV27"))
												{
													//get reservation request
													try {
														url			= xmlUrlGenerator.returnUrl(portalSpecificData, search.getSupplier(), "ReservationRequest", selectedHotel.getTracerID(), "hotel");
														System.out.println("reservation request - ".concat(url));
														reservationReq = inv27.reservationReq(url);
														xmlUrls.put("Reservation request", url);
													} catch (Exception e) {
														// TODO: handle exception
														System.out.println("Reservation request - " + e);
													}	
													
													
													//get reservation response
													try {
														url			= xmlUrlGenerator.returnUrl(portalSpecificData, search.getSupplier(), "ReservationResponse", selectedHotel.getTracerID(), "hotel");	
														System.out.println("reservation response - ".concat(url));
														reservationRes = inv27.reservationRes(url);
														xmlUrls.put("Reservation response", url);
													} catch (Exception e) {
														// TODO: handle exception
														System.out.println("Reservation response - " + e);
													}	
												}
												else if(search.getSupplier().equals("rezlive"))
												{
													//get reservation request
													try {
														url			= xmlUrlGenerator.returnUrl(portalSpecificData, search.getSupplier(), "ReservationRequest", selectedHotel.getTracerID(), "hotel");
														System.out.println("reservation request - ".concat(url));
														reservationReq = rezlive.reservationReq(url);
														xmlUrls.put("Reservation request", url);
													} catch (Exception e) {
														// TODO: handle exception
														System.out.println("Reservation request - " + e);
													}	
													
													
													//get reservation response
													try {
														url			= xmlUrlGenerator.returnUrl(portalSpecificData, search.getSupplier(), "ReservationResponse", selectedHotel.getTracerID(), "hotel");	
														System.out.println("reservation response - ".concat(url));
														reservationRes = rezlive.reservationRes(url);
														xmlUrls.put("Reservation response", url);
													} catch (Exception e) {
														// TODO: handle exception
														System.out.println("Reservation response - " + e);
													}	
												}
												
												
												//read payment log
												try {
													paymentDetails = xmlUrlGenerator.getPaymentLog(portalSpecificData.get("portal.PaymentLog"), confirmation.getTransactionID(), portalSpecificData);
												} catch (Exception e) {
													// TODO: handle exception
													System.out.println("Payment log error - " + e);
												}
												/*PrintTemplate.setInfoTableHeading("Xml urls");
												PrintTemplate.addToInfoTabel("Availability request", 		xmlUrls.get("Availability request"));
												PrintTemplate.addToInfoTabel("Availability response", 		xmlUrls.get("Availability response"));
												PrintTemplate.addToInfoTabel("Room availability request", 	xmlUrls.get("Room availability request"));
												PrintTemplate.addToInfoTabel("Room availability response", 	xmlUrls.get("Room availability response"));
												PrintTemplate.addToInfoTabel("Pre reservation request", 	xmlUrls.get("Pre reservation request"));
												PrintTemplate.addToInfoTabel("Pre reservation response", 	xmlUrls.get("Pre reservation response"));
												PrintTemplate.addToInfoTabel("Reservation request", 		xmlUrls.get("Reservation request"));
												PrintTemplate.addToInfoTabel("Reservation response", 		xmlUrls.get("Reservation response"));
												PrintTemplate.markTableEnd();*/
												info.clear();
												info.put("Availability request", xmlUrls.get("Availability request"));
												info.put("Availability response", xmlUrls.get("Availability response"));
												info.put("Room availability request", xmlUrls.get("Room availability request"));
												info.put("Room availability response", xmlUrls.get("Room availability response"));
												info.put("Pre reservation request", xmlUrls.get("Pre reservation request"));
												info.put("Pre reservation response", xmlUrls.get("Pre reservation response"));
												info.put("Reservation request", xmlUrls.get("Reservation request"));
												info.put("Reservation response", xmlUrls.get("Reservation response"));
												ReportTemplate.getInstance().setInfoTable("Xml urls", info);
											}
										}
										else
										{
//											PrintTemplate.markTableEnd();
										}
									}
									else
									{
//										PrintTemplate.markTableEnd();
									}
								} else {
//									PrintTemplate.markTableEnd();
								}
							}
							else if(search.getChannel().equals("cc"))
							{
								DriverFactory.getInstance().getDriver().get(portalSpecificData.get("PortalUrlCC") + portalSpecificData.get("searchUrl"));
								ccHome.enterHotelSearchCriteria(search);
								ccHome.performHotelSarch();
								
								PrintTemplate.verifyTrue(true, ccResultsPage.isAvailable(), "Verify results availability");
								if(ccResultsPage.isAvailable())
								{
									// get hotellist and select a hotel
									System.out.println("get hotellist and select a hotel");
									selectedHotel = ccResultsPage.selectHotel(ccResultsPage.gethotelDetails(), search);
									System.out.println("Tracer ID - " + selectedHotel.getTracerID());
//------------------------------------------------ get availability xmls---------------------------------------------------------------
									System.out.println("get availability xmls");
									if(search.getSupplier().equals("hotelbeds_v2"))
									{
										//get availability request
										try {
											url = xmlUrlGenerator.returnUrl(portalSpecificData, search.getSupplier(), "AvailabilityRequest", selectedHotel.getTracerID(), "hotel");
											System.out.println("availability request - ".concat(url));
											hotelAvailabilityReq = hb.availabilityReq(url);
											xmlUrls.put("Availability request", url);
										} catch (Exception e) {
											// TODO: handle exception
											System.out.println("Availability request - " + e);
										}
										
										//get availability response	
										try {
											url			= xmlUrlGenerator.returnUrl(portalSpecificData, search.getSupplier(), "AvailabilityResponse", selectedHotel.getTracerID(), "hotel");	
											System.out.println("availability response - ".concat(url));
											hotelAvailabilityRes = hb.availabilityRes(url);
											xmlUrls.put("Availability response", url);
										} catch (Exception e) {
											// TODO: handle exception
											System.out.println("Availability response - " + e);
										}
									}
									else if(search.getSupplier().equals("INV27"))
									{
										//get availability request
										try {
											url = xmlUrlGenerator.returnUrl(portalSpecificData, search.getSupplier(), "AvailabilityRequest", selectedHotel.getTracerID(), "hotel");
											System.out.println("availability request - ".concat(url));
											hotelAvailabilityReq = inv27.availabilityReq(url);
											xmlUrls.put("Availability request", url);
										} catch (Exception e) {
											// TODO: handle exception
											System.out.println("Availability request - " + e);
										}
										
										
										//get availability response	
										try {
											url			= xmlUrlGenerator.returnUrl(portalSpecificData, search.getSupplier(), "AvailabilityResponse", selectedHotel.getTracerID(), "hotel");
											System.out.println("availability response - ".concat(url));
											hotelAvailabilityRes = inv27.availabilityRes(url);
											xmlUrls.put("Availability response", url);
										} catch (Exception e) {
											// TODO: handle exception
											System.out.println("Availability response - " + e);
										}	
									}
									else if(search.getSupplier().equals("rezlive"))
									{
										//get availability request
										try {
											url = xmlUrlGenerator.returnUrl(portalSpecificData, search.getSupplier(), "AvailabilityRequest", selectedHotel.getTracerID(), "hotel");
											System.out.println("availability request - ".concat(url));
											hotelAvailabilityReq = rezlive.availabilityReq(url);
											xmlUrls.put("Availability request", url);
										} catch (Exception e) {
											// TODO: handle exception
											System.out.println("Availability request - " + e);
										}	
										
										//get availability response	
										try {
											url			= xmlUrlGenerator.returnUrl(portalSpecificData, search.getSupplier(), "AvailabilityResponse", selectedHotel.getTracerID(), "hotel");
											System.out.println("availability response - ".concat(url));
											hotelAvailabilityRes = rezlive.availabilityRes(url);
											xmlUrls.put("Availability response", url);
										} catch (Exception e) {
											// TODO: handle exception
											System.out.println("Availability response - " + e);
										}	
									}
									
//-----------------------------------------get room availability xmls---------------------------------------------------------------
									System.out.println("get room availability xmls");
									if(search.getSupplier().equals("hotelbeds_v2"))
									{
										//get room availability request
										try {
											url			= xmlUrlGenerator.returnUrl(portalSpecificData, search.getSupplier(), "RoomAvailabilityRequest", selectedHotel.getTracerID(), "hotel");
											System.out.println("room availability request - ".concat(url));
											roomAvailablityReq = hb.roomAvailabilityReq(url);
											xmlUrls.put("Room availability request", url);
										} catch (Exception e) {
											// TODO: handle exception
											System.out.println("Room availability request - " + e);
										}
										
										
										//get room availability response
										try {
											url			= xmlUrlGenerator.returnUrl(portalSpecificData, search.getSupplier(), "RoomAvailabilityResponse", selectedHotel.getTracerID(), "hotel");
											System.out.println("room availability response - ".concat(url));
											roomAvailablityRes = hb.roomAvailabilityRes(url);
											xmlUrls.put("Room availability response", url);
										} catch (Exception e) {
											// TODO: handle exception
											System.out.println("Room availability response - " + e);
										}
										 
									}
									else if(search.getSupplier().equals("INV27"))
									{
										//get room availability request
										try {
											url			= xmlUrlGenerator.returnUrl(portalSpecificData, search.getSupplier(), "RoomAvailabilityRequest", selectedHotel.getTracerID(), "hotel");	
											System.out.println("room availability request - ".concat(url));
											roomAvailablityReq = inv27.roomAvailabilityReq(url); 
											xmlUrls.put("Room availability request", url);
										} catch (Exception e) {
											// TODO: handle exception
											System.out.println("Room availability request - " + e);
										}
										
										
										//get room availability response
										try {
											url			= xmlUrlGenerator.returnUrl(portalSpecificData, search.getSupplier(), "RoomAvailabilityResponse", selectedHotel.getTracerID(), "hotel");	
											System.out.println("room availability response - ".concat(url));
											roomAvailablityRes = inv27.roomAvailabilityRes(url); 
											xmlUrls.put("Room availability response", url);
										} catch (Exception e) {
											// TODO: handle exception
											System.out.println("Room availability response - " + e);
										}
										
									}
									else if(search.getSupplier().equals("rezlive"))
									{
										//get room availability request
										try {
											url			= xmlUrlGenerator.returnUrl(portalSpecificData, search.getSupplier(), "RoomAvailabilityRequest", selectedHotel.getTracerID(), "hotel");	
											System.out.println("room availability request - ".concat(url));
											roomAvailablityReq = rezlive.roomAvailabilityReq(url); 
											xmlUrls.put("Room availability request", url);
										} catch (Exception e) {
											// TODO: handle exception
											System.out.println("Room availability request - " + e);
										}
										
										
										//get room availability response
										try {
											url			= xmlUrlGenerator.returnUrl(portalSpecificData, search.getSupplier(), "RoomAvailabilityResponse", selectedHotel.getTracerID(), "hotel");	
											System.out.println("room availability response - ".concat(url));
											roomAvailablityRes = rezlive.roomAvailabilityRes(url); 
											xmlUrls.put("Room availability response", url);
										} catch (Exception e) {
											// TODO: handle exception
											System.out.println("Room availability response - " + e);
										}
										
									}
									
									//get cancellation deadline
									System.out.println("get cancellation deadline");
									HotelCancellationPolicyProcesser canDeadlineCreator = new HotelCancellationPolicyProcesser();
									canDeadline = canDeadlineCreator.getCancellationDeadline(search.getSupplier(), roomAvailablityRes);
									
									// verify selsected hotel
									System.out.println("verify selsected hotel");
									verify.selectedHotelAgainstAvailabliltyAndRoomAvailabilityResponses(selectedHotel, hotelAvailabilityRes, roomAvailablityRes, PrintTemplate, search, portalSpecificData, currencyMap, defaultCC);
									
									//get shopping cart values
									System.out.println("get shopping cart values");
									HashMap<String, String> shoppingCartValues = ccResultsPage.getCartValues();
									
									//verify shopping cart values
									System.out.println("verify shopping cart values");
									verify.shoppingCartVerificationCC(PrintTemplate, defaultCC, currencyMap, portalSpecificData, roomAvailablityRes, shoppingCartValues, selectedHotel);
									
									PrintTemplate.verifyTrue(true, ccPaymentPage.isAvailabile(), "Verify payment page availability");
									if(ccPaymentPage.isAvailabile())
									{
										//payments page
										System.out.println("Getting payment page values");
										HashMap<String, String> paymentPageValues = ccPaymentPage.getValues(search);
										ccPaymentPage.enterDetails(portalSpecificData, occupancyList, search, selectedHotel);
										
										// verify payment page
										verify.paymentPageVerificationCC(paymentPageValues, portalSpecificData, currencyMap, defaultCC, search, PrintTemplate, canDeadline, roomAvailablityRes, hotelAvailabilityRes);
										
										if(search.getOnlineOrOffline().equals("online"))
										{
											System.out.println("Payment Page Check payment gateway");
//											PrintTemplate.setTableHeading("Payment Page Check payment gateway");
											PrintTemplate.verifyTrue(true, pg.isPaymentGatewayLoaded(configDetails), "Check payment gateway Availability");
//											PrintTemplate.markTableEnd();
											if(pg.isPaymentGatewayLoaded(configDetails))
											{
												try {		
													pg.enterCardDetails(portalSpecificData);
													pg.confirm(portalSpecificData.get("PaymentGateway_CardType"));
												} catch (Exception e) {
													// TODO: handle exception
													System.out.println(e);				
												}	
											}
										}
										PrintTemplate.verifyTrue(true, ccConfirmationPage.isAvailable(), "Verify confirmation page availability");
										if(ccConfirmationPage.isAvailable())
										{
											System.out.println("Confirmation page available");
											//Get confirmation page labels
											HashMap<String, String> labelPropertyMap = new HashMap<>();
											//Get confirmation page values
											confirmation = ccConfirmationPage.getValues(search, selectedHotel);
											//get pre reservation xmls
											if(search.getSupplier().equals("hotelbeds_v2"))
											{
												//get pre reservation request
												try {
													url			= xmlUrlGenerator.returnUrl(portalSpecificData, search.getSupplier(), "PreReservationRequest", selectedHotel.getTracerID(), "hotel");
													System.out.println("pre reservation request - ".concat(url));
													preReservationReq = hb.preReservationReq(url);
													xmlUrls.put("Pre reservation request", url);
												} catch (Exception e) {
													// TODO: handle exception
													System.out.println("Pre reservation request - " + e);
												}
												
												
												//get pre reservation response
												try {
													url			= xmlUrlGenerator.returnUrl(portalSpecificData, search.getSupplier(), "PreReservationResponse", selectedHotel.getTracerID(), "hotel");	
													System.out.println("pre reservation response - ".concat(url));
													preReservationRes = hb.preReservationResponse(url);
													xmlUrls.put("Pre reservation response", url);
												} catch (Exception e) {
													// TODO: handle exception
													System.out.println("Pre reservation response - " + e);
												}
												
											}
											else if(search.getSupplier().equals("INV27"))
											{
												//get pre reservation request
												try {
													url			= xmlUrlGenerator.returnUrl(portalSpecificData, search.getSupplier(), "PreReservationRequest", selectedHotel.getTracerID(), "hotel");
													System.out.println("pre reservation request - ".concat(url));
													preReservationReq = inv27.preReservationReq(url);
													xmlUrls.put("Pre reservation request", url);
												} catch (Exception e) {
													// TODO: handle exception
													System.out.println("Pre reservation request - " + e);
												}
												
												//get pre reservation response
												try {
													url			= xmlUrlGenerator.returnUrl(portalSpecificData, search.getSupplier(), "PreReservationResponse", selectedHotel.getTracerID(), "hotel");
													System.out.println("pre reservation response - ".concat(url));
													preReservationRes = inv27.preReservationResponse(url);
													xmlUrls.put("Pre reservation response", url);
												} catch (Exception e) {
													// TODO: handle exception
													System.out.println("Pre reservation response - " + e);
												}
												
											}
											else if(search.getSupplier().equals("rezlive"))
											{
												//get pre reservation request
												try {
													url			= xmlUrlGenerator.returnUrl(portalSpecificData, search.getSupplier(), "PreReservationRequest", selectedHotel.getTracerID(), "hotel");
													System.out.println("pre reservation request - ".concat(url));
													preReservationReq = rezlive.preReservationReq(url);
													xmlUrls.put("Pre reservation request", url);
												} catch (Exception e) {
													// TODO: handle exception
													System.out.println("Pre reservation request - " + e);
												}
												
												//get pre reservation response
												try {
													url			= xmlUrlGenerator.returnUrl(portalSpecificData, search.getSupplier(), "PreReservationResponse", selectedHotel.getTracerID(), "hotel");
													System.out.println("pre reservation response - ".concat(url));
													preReservationRes = rezlive.preReservationRes(url);
													xmlUrls.put("Pre reservation response", url);
												} catch (Exception e) {
													// TODO: handle exception
													System.out.println("Pre reservation response - " + e);
												}
												
											}
											
											
											//get reservation xmls
											if(search.getSupplier().equals("hotelbeds_v2"))
											{
												//get reservation request
												try {
													url			= xmlUrlGenerator.returnUrl(portalSpecificData, search.getSupplier(), "ReservationRequest", selectedHotel.getTracerID(), "hotel");
													System.out.println("reservation request - ".concat(url));
													reservationReq = hb.reservationReq(url);
													xmlUrls.put("Reservation request", url);
												} catch (Exception e) {
													// TODO: handle exception
													System.out.println("Reservation request - " + e);
												}	
												
												
												//get reservation response
												try {
													url			= xmlUrlGenerator.returnUrl(portalSpecificData, search.getSupplier(), "ReservationResponse", selectedHotel.getTracerID(), "hotel");
													System.out.println("reservation response - ".concat(url));
													reservationRes = hb.reservationResponse(url);
													xmlUrls.put("Reservation response", url);
												} catch (Exception e) {
													// TODO: handle exception
													System.out.println("Reservation response - " + e);
												}
												
											}
											else if(search.getSupplier().equals("INV27"))
											{
												//get reservation request
												try {
													url			= xmlUrlGenerator.returnUrl(portalSpecificData, search.getSupplier(), "ReservationRequest", selectedHotel.getTracerID(), "hotel");
													System.out.println("reservation request - ".concat(url));
													reservationReq = inv27.reservationReq(url);
													xmlUrls.put("Reservation request", url);
												} catch (Exception e) {
													// TODO: handle exception
													System.out.println("Reservation request - " + e);
												}	
												
												
												//get reservation response
												try {
													url			= xmlUrlGenerator.returnUrl(portalSpecificData, search.getSupplier(), "ReservationResponse", selectedHotel.getTracerID(), "hotel");	
													System.out.println("reservation response - ".concat(url));
													reservationRes = inv27.reservationRes(url);
													xmlUrls.put("Reservation response", url);
												} catch (Exception e) {
													// TODO: handle exception
													System.out.println("Reservation response - " + e);
												}	
											}
											else if(search.getSupplier().equals("rezlive"))
											{
												//get reservation request
												try {
													url			= xmlUrlGenerator.returnUrl(portalSpecificData, search.getSupplier(), "ReservationRequest", selectedHotel.getTracerID(), "hotel");
													System.out.println("reservation request - ".concat(url));
													reservationReq = rezlive.reservationReq(url);
													xmlUrls.put("Reservation request", url);
												} catch (Exception e) {
													// TODO: handle exception
													System.out.println("Reservation request - " + e);
												}	
												
												
												//get reservation response
												try {
													url			= xmlUrlGenerator.returnUrl(portalSpecificData, search.getSupplier(), "ReservationResponse", selectedHotel.getTracerID(), "hotel");	
													System.out.println("reservation response - ".concat(url));
													reservationRes = rezlive.reservationRes(url);
													xmlUrls.put("Reservation response", url);
												} catch (Exception e) {
													// TODO: handle exception
													System.out.println("Reservation response - " + e);
												}	
											}
											
											
											//read payment log
											try {
												paymentDetails = xmlUrlGenerator.getPaymentLog(portalSpecificData.get("portal.PaymentLog"), confirmation.getTransactionID(), portalSpecificData);
											} catch (Exception e) {
												// TODO: handle exception
												System.out.println("Payment log error - " + e);
											}
											
											//set serialized details
											String today = Calender.getToday("dd-MMM-yyyy");
											allDetails.setReportDate(today);
											allDetails.setSearch(search);
											allDetails.setSelectedHotel(selectedHotel);
											allDetails.setPortalSpecificData(portalSpecificData);
											allDetails.setHotelLabels(labelPropertyMap);
											allDetails.setCommonLabels(commonLabels);
											allDetails.setReservationRes(reservationRes);
											allDetails.setPaymentDetails(paymentDetails);
											allDetails.setConfigDetails(configDetails);
											allDetails.setWebsiteDetails(websiteDetails);
											allDetails.setCanDeadline(canDeadline);
											allDetails.setDefaultCurrency(defaultCC);
											allDetails.setCurrencyMap(currencyMap);
											allDetails.setConfirmation(confirmation);
											allDetails.setOccupancyList(occupancyList);
											allDetails.setRoomAvailablityRes(roomAvailablityRes);
											allDetails.setHotelAvailabilityRes(hotelAvailabilityRes);
											allDetails.setConfirmed(confirmationPage.isAvailable());
											hotelResObject.setProduct("Hotel");
											hotelResObject.setHotelReservation(allDetails);
											
											serialize.SerializeReservation(hotelResObject, portalSpecificData.get("Portal.Name"), "../Rezrobot_Details/object/");
											
											//verify confirmation page
											verify.confirmationPageAgainsrReservationResponse(PrintTemplate, confirmation, reservationRes, portalSpecificData, currencyMap, defaultCC, occupancyList, search, canDeadline, paymentDetails, hotelAvailabilityRes);
											
											//check reports
											checkReports(verify, currencyMap, occupancyList, search, defaultCC, today, canDeadline, websiteDetails, xmlUrls);
										
											info.clear();
											info.put("Availability request", xmlUrls.get("Availability request"));
											info.put("Availability response", xmlUrls.get("Availability response"));
											info.put("Room availability request", xmlUrls.get("Room availability request"));
											info.put("Room availability response", xmlUrls.get("Room availability response"));
											info.put("Pre reservation request", xmlUrls.get("Pre reservation request"));
											info.put("Pre reservation response", xmlUrls.get("Pre reservation response"));
											info.put("Reservation request", xmlUrls.get("Reservation request"));
											info.put("Reservation response", xmlUrls.get("Reservation response"));
											ReportTemplate.getInstance().setInfoTable("Xml urls", info);
											if(paymentDetails.containsKey("errorStatus"))
											{
												/*PrintTemplate.setInfoTableHeading("Payment log error");
												PrintTemplate.addToInfoTabel("Status", paymentDetails.get("errorStatus"));
												PrintTemplate.addToInfoTabel("Error", paymentDetails.get("errorDescription"));
												PrintTemplate.markTableEnd();*/
												info.clear();
												info.put("Status", paymentDetails.get("errorStatus"));
												info.put("Error", paymentDetails.get("errorDescription"));
												ReportTemplate.getInstance().setInfoTable("Payment log error", info);
											}
										}
									}
								}
							}
							else
							{
								System.out.println("Invalid option");
							}
						}	
					}	
				}
			
			} catch (Exception e) {
				// TODO: handle exception
				System.out.println("Exit");
				System.out.println("flow error " + e);
				e.printStackTrace();
				
				/*PrintTemplate.setInfoTableHeading("Flow error");
				PrintTemplate.addToInfoTabel("Error", e.toString());
				PrintTemplate.markTableEnd();*/
				info.clear();
				info.put("Error", e.toString());
				ReportTemplate.getInstance().setInfoTable("Flow error", info);
			}
		
		}
	}
	
	public void checkReports(Hotel_Verify verify, HashMap<String, String> currencyMap, ArrayList<HotelOccupancyObject> occupancyList, HotelSearchObject search, String defaultCC, String today, String canDeadline, ArrayList<HashMap<String, String>> websiteDetails, HashMap<String, String> xmlUrls) throws Exception
	{
		//log to check reports
		DriverFactory.getInstance().getDriver().get(portalSpecificData.get("PortalUrlCC").concat(portalSpecificData.get("login")));
		try {
			Alert alert = DriverFactory.getInstance().getDriver().switchTo().alert();
	        alert.accept(); 
		} catch (Exception e) {
			// TODO: handle exception
		}
		
//		PrintTemplate.setTableHeading("Check login page availability (To check reports)");
		PrintTemplate.verifyTrue(true, login.isLoginPageAvailable(), "Hotel Login page Availability");
//		PrintTemplate.markTableEnd();
		if(login.isLoginPageAvailable())
		{
			login.typeUserName(portalSpecificData.get("hotelUsername"));
			login.typePassword(portalSpecificData.get("hotelPassword"));
			login.loginSubmit();
			
//			PrintTemplate.setTableHeading("Check BO menu page availability");
			PrintTemplate.verifyTrue(true, bomenu.isModuleImageAvailable(), "Hotel Bo menu page Availability");
//			PrintTemplate.markTableEnd();
			if(bomenu.isModuleImageAvailable())
			{
				//reservation report criteria
				DriverFactory.getInstance().getDriver().get(portalSpecificData.get("PortalUrlCC").concat(portalSpecificData.get("reservationReport")));
				HashMap<String, Boolean> 	resReportCriteriaComboboxAvailability 			= resReportCriteria.checkComboboxAvailability();
				HashMap<String, Boolean> 	resReportCriteriaLabelsAvailability  			= resReportCriteria.checkLabelAvailability();
				HashMap<String, Boolean> 	resReportCriteriaRadioButtonsAvailability  		= resReportCriteria.checkRadioButtonAvailability();
				HashMap<String, Boolean> 	resReportCriteriaButtonsAvailability  			= resReportCriteria.checkButtonxAvailability();
				HashMap<String, String>		resReportCriteriaLabels							= resReportCriteria.getLabels();
				
				
				//reservation report criteria check element availability
				verify.reservationReportCriteriaButtonsAvailability						(PrintTemplate, resReportCriteriaButtonsAvailability );
				verify.reservationReportCriteriaComboboxAvailability					(PrintTemplate, resReportCriteriaComboboxAvailability );
				verify.reservationReportCriteriaLabelsAvailability						(PrintTemplate, resReportCriteriaLabelsAvailability );
				verify.reservationReportCriteriaRadioButtonsAvailability				(PrintTemplate, resReportCriteriaRadioButtonsAvailability );
				verify.reservationReportCriteriaLabelsVerification						(PrintTemplate, resReportCriteriaLabels, labelProperty.getCommonLabels());
				
				//reservation report - enter criteria
				resReportCriteria.enterCriteria(confirmation.getReservationNumber(), "hotel");
				
				// reservation report
				// reservation report get labels
				HashMap<String, Boolean> 	resReportLabelAvailability 							= resReport.hotelResReportLabelAvailability();
				HashMap<String, String> 	resReportLabels										= resReport.hotelResReportGetLabels();
				
				// reservation report check element availability
				verify.resReportLabelAvailability(PrintTemplate, resReportLabelAvailability);
				//reservation report check labels
				verify.resReportLabelVerification(PrintTemplate, resReportLabels, labelProperty.getCommonLabels(), portalSpecificData);
				
				//reservation report get values
				HashMap<String, Boolean> 	resReportValueAvailability							= resReport.hotelResReportGetValueAvailability();
				HashMap<String, String>		resReportVales										= resReport.getValuesHotel();
				
				//reservation report check values availability
				verify.resReportValueAvailability(PrintTemplate, resReportValueAvailability);	
				
				//reservation report values verification
				verify.reservationReportVerify(PrintTemplate, resReportVales, reservationRes, paymentDetails, currencyMap, portalSpecificData, occupancyList, confirmation, search, defaultCC, today);
				
				//booking list report criteria
				DriverFactory.getInstance().getDriver().get(portalSpecificData.get("PortalUrlCC").concat(portalSpecificData.get("bookingListReport")));
				HashMap<String, Boolean> 	bookingListReportCriteriaLabelAvailability 			= bookingListCriteria.getLabelAvailability();
				HashMap<String, String> 	bookingListCriteriaLabels 							= bookingListCriteria.getLabels();
				verify.bookingListReportCriteriaLabelAvailability(PrintTemplate, bookingListReportCriteriaLabelAvailability);
				verify.bookingListReportCriteriaLabelVerification(PrintTemplate, bookingListCriteriaLabels, labelProperty.getCommonLabels());
				
				// booking list report enter search criteria
				bookingListCriteria.enterCriteria(confirmation.getReservationNumber());
				
				// booking list report get labels
				HashMap<String, Boolean> 	bookingListLabelAvailability 						= bookingList.getHotelLabelsAvailability();
				HashMap<String, String>	 	bookingListLabels 									= bookingList.getHotelLabels();
				HashMap<String, String> 	bookingListValues 									= bookingList.getHotelValues();
				verify.bookingListLabelAvailability(PrintTemplate, bookingListLabelAvailability);
				verify.bookingListLabelVerification(PrintTemplate, bookingListLabels, labelProperty.getCommonLabels());
				verify.bookingListValueVarification(); // todo
				
				//booking list - live booking more info
				HashMap<String, Boolean> 	bookingListLiveLabelAvailability 					= bookingListLiveBooking.getHotelLabelAvailability();
				HashMap<String, String>	 	bookingListLIveLabels 								= bookingListLiveBooking.getHotelLabels();
				HashMap<String, String>	 	bookingListLIveValues 								= bookingListLiveBooking.getHotelValues();
				bookingListLiveBooking.closeHotelMoreInfo();
				bookingList.clickHotelReservationNumber();
				verify.bookingListLiveLabelAvailability(PrintTemplate, bookingListLiveLabelAvailability);
				verify.bookingListLiveLabelVerification(PrintTemplate, bookingListLIveLabels, labelProperty.getHotelLabelProperties(), search);
				verify.bookingListLiveValueVerification(PrintTemplate, bookingListLIveValues, reservationRes, search, defaultCC, currencyMap, bookingListLIveLabels, portalSpecificData);
				
				// booking list - booking card
				HashMap<String, Boolean> 	bookingListBookingCardlabelAvailability  			= bookingListBookingCard.getHotelLabelAvailability();
				HashMap<String, String>	 	bookingListBookingCardLabels 						= bookingListBookingCard.getHotelLabels();
				HashMap<String, String>	 	bookingListBookingCarddValues 						= bookingListBookingCard.getHotelValues();
				verify.bookingListBookingCardLabelAvailability(PrintTemplate, bookingListBookingCardlabelAvailability);
				verify.bookingListBookingCardLabelVarify(PrintTemplate, labelProperty.getHotelLabelProperties(), bookingListBookingCardLabels, search);
				verify.bookingListBookingCardValueVerify(PrintTemplate, bookingListBookingCarddValues, search, reservationRes, defaultCC, currencyMap, portalSpecificData, confirmation, paymentDetails, canDeadline, roomAvailablityRes, occupancyList, today);
				
				// profit and loss report - Enter criteria
				DriverFactory.getInstance().getDriver().get(portalSpecificData.get("PortalUrlCC").concat(portalSpecificData.get("profitNlossReportUrl")));
				profitAndLossCriteria.enterCriteria(Calender.getToday("dd-MMM-yyyy"));
				// profit and loss report - get value
				HashMap<String, String> 	profitAndLossReportValues 							= profitAndLossReport.getValues(confirmation.getReservationNumber());
				//profit and loss report - value verification
				verify.profitAndLossReportValuesVerification(PrintTemplate, profitAndLossReportValues, search, portalSpecificData, defaultCC, currencyMap, reservationRes, confirmation, today, occupancyList);
			
				// third party supplier payable report - enter criteria
				DriverFactory.getInstance().getDriver().get(portalSpecificData.get("PortalUrlCC").concat(portalSpecificData.get("thirdPartySupplierPayableUrl")));
				thirdPartySupPayableCriteria.enterCriteria(Calender.getToday("dd-MMM-yyyy"));
				// third party supplier payable report - get values
				HashMap<String, String> 	thirdPartySupplierValues 							= thirdPartySupPayableReport.getValues(confirmation);
				// third party supplier payable report - values verifications
				verify.thirdPartySupplierPayableValuesVerification(PrintTemplate, reservationRes, thirdPartySupplierValues, search, defaultCC, currencyMap, portalSpecificData, confirmation, occupancyList, today);
			
				// confirmation mail enter criteria
				DriverFactory.getInstance().getDriver().get(portalSpecificData.get("PortalUrlCC").concat(portalSpecificData.get("confirmationreportUrl")));
				confirmationMailCriteria.getHotelConfirmationMail(confirmation.getReservationNumber());
				
				// confirmation mail read
				HashMap<String, String> confrmationMail = confirmationMail.readHotel();
				verify.confirmationMail(PrintTemplate, confrmationMail, portalSpecificData, reservationRes, websiteDetails, confirmation, currencyMap, defaultCC, hotelAvailabilityRes, paymentDetails, occupancyList, search, roomAvailablityRes);
			
				//voucher mail
				HashMap<String, String> voucherMail = voucher.getValues();
				verify.voucherMail(PrintTemplate, voucherMail, confirmation, occupancyList, websiteDetails, portalSpecificData, reservationRes, search, roomAvailablityRes);
			
				if(search.getCancel().equals("yes"))
				{
					// cancellation criteria
					DriverFactory.getInstance().getDriver().get(portalSpecificData.get("PortalUrlCC").concat(portalSpecificData.get("cancellationUrl")));
					cancellationCriteria.enterCriteria(confirmation.getReservationNumber());
					
					// cancellation get values
					HashMap<String, String> cancellationDetails = cancellation.getValues();
					
					// varify cancellation details
					verify.cancellationValuesVerification(cancellationDetails, confirmation, occupancyList, portalSpecificData, reservationRes, PrintTemplate, today, search, currencyMap, defaultCC, resReportVales.get("sellingCurrency"), canDeadline, roomAvailablityRes);
				
					// cancellation report criteria
					DriverFactory.getInstance().getDriver().get(portalSpecificData.get("PortalUrlCC").concat(portalSpecificData.get("cancellationReportUrl")));
					cancellationReportCriteria.enterCriteria(confirmation.getReservationNumber());	
					
					// cancellation report enter criteria
					cancellationReportCriteria.enterCriteria(confirmation.getReservationNumber());
					HashMap<String, String> cancellationReportValues = cancellationReport.getValues();
					
					// cancellation report verification
					verify.cancellationReportValues(cancellationReportValues, portalSpecificData, PrintTemplate, reservationRes, canDeadline, currencyMap, defaultCC, confirmation, search, occupancyList);
				}
			}	
		}
	}

	@After
	public void tearDown() throws Exception {
		//DriverFactory.getInstance().getDriver().quit();
		System.out.println("quit");
		/*PrintWriter.append("</body></html>");
		BufferedWriter sbwr = new BufferedWriter(new FileWriter(new File("Reports/" + this.getClass().getSimpleName() + "_"
						+ Browser + ".html")));
		sbwr.write(PrintWriter.toString());
		sbwr.flush();
		sbwr.close();*/
		//PrintWriter.setLength(0);
	}
	
}


