package com.rezrobot.common.objects;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;

import com.rezrobot.common.pojo.hotelDetails;
import com.rezrobot.dataobjects.HotelConfirmationPageDetails;
import com.rezrobot.dataobjects.HotelOccupancyObject;
import com.rezrobot.dataobjects.Vacation_Rate_Details;
import com.rezrobot.dataobjects.Vacation_Search_object;
import com.rezrobot.flight_circuitry.xml_objects.ReservationResponse;

public class VacationObject implements Serializable{

	Vacation_Search_object 				search 				= new Vacation_Search_object();
	HashMap<String, String> 			portalSpecificData 	= new HashMap<>();
	HashMap<String, String> 			hotelLabels 		= new HashMap<>();
	HashMap<String, String> 			commonLabels 		= new HashMap<>();
	hotelDetails 						reservationRes 		= new hotelDetails();
	HashMap<String, String> 			paymentDetails      = new HashMap<>();
	HashMap<String, String> 			configDetails		= new HashMap<>();
	ArrayList<HashMap<String, String>> 	websiteDetails 		= new ArrayList<HashMap<String, String>>();
	String 								hotelCanDeadline 	= "";
	String 								flightCanDeadline 	= "";
	String 								canDeadline 		= "";
	HashMap<String, String> 			currencyMap  		= new HashMap<>();
	HashMap<String, String>		 		confirmation 		= new HashMap<>();
	ArrayList<HotelOccupancyObject> 	occupancyList 		= new ArrayList<HotelOccupancyObject>();
	hotelDetails						roomAvailablityRes 	= new hotelDetails();
	ArrayList<hotelDetails> 			hotelAvailabilityRes= new ArrayList<hotelDetails>();
	boolean								isConfirmed			= false;
	String								reportDate			= "";
	ReservationResponse					flightRes			= new ReservationResponse();
	String								defaultCurrency		= "";
	Vacation_Rate_Details				rateDetails			= new Vacation_Rate_Details();
	
	
	public Vacation_Rate_Details getRateDetails() {
		return rateDetails;
	}
	public void setRateDetails(Vacation_Rate_Details rateDetails) {
		this.rateDetails = rateDetails;
	}
	public String getDefaultCurrency() {
		return defaultCurrency;
	}
	public void setDefaultCurrency(String defaultCurrency) {
		this.defaultCurrency = defaultCurrency;
	}
	public Vacation_Search_object getSearch() {
		return search;
	}
	public void setSearch(Vacation_Search_object search) {
		this.search = search;
	}
	public HashMap<String, String> getPortalSpecificData() {
		return portalSpecificData;
	}
	public void setPortalSpecificData(HashMap<String, String> portalSpecificData) {
		this.portalSpecificData = portalSpecificData;
	}
	public HashMap<String, String> getHotelLabels() {
		return hotelLabels;
	}
	public void setHotelLabels(HashMap<String, String> hotelLabels) {
		this.hotelLabels = hotelLabels;
	}
	public HashMap<String, String> getCommonLabels() {
		return commonLabels;
	}
	public void setCommonLabels(HashMap<String, String> commonLabels) {
		this.commonLabels = commonLabels;
	}
	public hotelDetails getReservationRes() {
		return reservationRes;
	}
	public void setReservationRes(hotelDetails reservationRes) {
		this.reservationRes = reservationRes;
	}
	public HashMap<String, String> getPaymentDetails() {
		return paymentDetails;
	}
	public void setPaymentDetails(HashMap<String, String> paymentDetails) {
		this.paymentDetails = paymentDetails;
	}
	public HashMap<String, String> getConfigDetails() {
		return configDetails;
	}
	public void setConfigDetails(HashMap<String, String> configDetails) {
		this.configDetails = configDetails;
	}
	public ArrayList<HashMap<String, String>> getWebsiteDetails() {
		return websiteDetails;
	}
	public void setWebsiteDetails(ArrayList<HashMap<String, String>> websiteDetails) {
		this.websiteDetails = websiteDetails;
	}
	public String getHotelCanDeadline() {
		return hotelCanDeadline;
	}
	public void setHotelCanDeadline(String hotelCanDeadline) {
		this.hotelCanDeadline = hotelCanDeadline;
	}
	public String getFlightCanDeadline() {
		return flightCanDeadline;
	}
	public void setFlightCanDeadline(String flightCanDeadline) {
		this.flightCanDeadline = flightCanDeadline;
	}
	public String getCanDeadline() {
		return canDeadline;
	}
	public void setCanDeadline(String canDeadline) {
		this.canDeadline = canDeadline;
	}
	public HashMap<String, String> getCurrencyMap() {
		return currencyMap;
	}
	public void setCurrencyMap(HashMap<String, String> currencyMap) {
		this.currencyMap = currencyMap;
	}
	public HashMap<String, String> getConfirmation() {
		return confirmation;
	}
	public void setConfirmation(HashMap<String, String> confirmation) {
		this.confirmation = confirmation;
	}
	public ArrayList<HotelOccupancyObject> getOccupancyList() {
		return occupancyList;
	}
	public void setOccupancyList(ArrayList<HotelOccupancyObject> occupancyList) {
		this.occupancyList = occupancyList;
	}
	public hotelDetails getRoomAvailablityRes() {
		return roomAvailablityRes;
	}
	public void setRoomAvailablityRes(hotelDetails roomAvailablityRes) {
		this.roomAvailablityRes = roomAvailablityRes;
	}
	public ArrayList<hotelDetails> getHotelAvailabilityRes() {
		return hotelAvailabilityRes;
	}
	public void setHotelAvailabilityRes(ArrayList<hotelDetails> hotelAvailabilityRes) {
		this.hotelAvailabilityRes = hotelAvailabilityRes;
	}
	public boolean isConfirmed() {
		return isConfirmed;
	}
	public void setConfirmed(boolean isConfirmed) {
		this.isConfirmed = isConfirmed;
	}
	public String getReportDate() {
		return reportDate;
	}
	public void setReportDate(String reportDate) {
		this.reportDate = reportDate;
	}
	public ReservationResponse getFlightRes() {
		return flightRes;
	}
	public void setFlightRes(ReservationResponse flightRes) {
		this.flightRes = flightRes;
	}
	
	
	
}
