package com.rezrobot.common.objects;

import java.io.Serializable;

import com.rezrobot.dataobjects.ActivityDetails;
import com.rezrobot.dataobjects.Activity_WEB_Search;

public class ActivityReservation implements Serializable{
	
	private String reservationNo = "";
	private boolean	isBookingConfirmed = false;
	private Activity_WEB_Search searchObject;
	private ActivityDetails activityDetails;
	
	public String getReservationNo() {
		return reservationNo;
	}
	public void setReservationNo(String reservationNo) {
		this.reservationNo = reservationNo;
	}
	public boolean isBookingConfirmed() {
		return isBookingConfirmed;
	}
	public void setBookingConfirmed(boolean isBookingConfirmed) {
		this.isBookingConfirmed = isBookingConfirmed;
	}
	public Activity_WEB_Search getSearchObject() {
		return searchObject;
	}
	public void setSearchObject(Activity_WEB_Search searchObject) {
		this.searchObject = searchObject;
	}
	public ActivityDetails getActivityDetails() {
		return activityDetails;
	}
	public void setActivityDetails(ActivityDetails activityDetails) {
		this.activityDetails = activityDetails;
	}
	
	

}
