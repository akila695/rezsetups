package com.rezrobot.core;
/**
 * @author Dulan
 *
 */
public enum  RefType {
ID,NAME,XPATH,CLASSNAME,LINKTEXT,PARTIALLINKTEXT,CSS,TAGNAME;

public static RefType getType(String ref_type){

	switch (ref_type){
	case("1"): 
	    return ID;
	case("2"):
		return NAME;
	case("3"): 
		return XPATH;
	case("4"): 
		return CLASSNAME;
	case("5"): 
	    return LINKTEXT;
	case("6"): 
	    return PARTIALLINKTEXT;
	case("7"): 
	    return CSS;
	case("8"): 
	    return TAGNAME;
	default  :
		return XPATH;
	
	
	}
}

}
