package com.rezrobot.xml_readers;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.InetSocketAddress;
import java.net.MalformedURLException;
import java.net.Proxy;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.concurrent.TimeUnit;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;


import com.rezrobot.common.pojo.discount;
import com.rezrobot.common.pojo.errorList;
import com.rezrobot.common.pojo.hbAdditionalCost;
import com.rezrobot.common.pojo.hotelDetails;
import com.rezrobot.common.pojo.occupancyDetails;
import com.rezrobot.common.pojo.roomDetails;
import com.rezrobot.utill.Calender;
import com.rezrobot.utill.SetProxyPort;

public class HbReader {

	public hotelDetails availabilityReq(String pageSource) throws IOException, InterruptedException
	{
		SetProxyPort 			proxyAndPort	= new SetProxyPort();
		Document 				doc  			= Jsoup.parse(proxyAndPort.setProxy(pageSource));
		Thread.sleep(2000);
		hotelDetails			hotelDetails		= new hotelDetails();
		
		hotelDetails.setUser				(doc.getElementsByTag("User").first().text());
		hotelDetails.setPassword			(doc.getElementsByTag("Password").first().text());
		hotelDetails.setCheckin				(doc.getElementsByTag("CheckInDate").first().attr("date"));
		//System.out.println(hotelDetails.getCheckin());
		hotelDetails.setCheckout			(doc.getElementsByTag("CheckOutDate").first().attr("date"));
		//System.out.println(hotelDetails.getCheckout());
		hotelDetails.setCityCode			(doc.getElementsByTag("Destination").first().attr("code"));
		hotelDetails.setRoomCount			(String.valueOf(doc.getElementsByTag("RoomCount").size()));
		//hotelDetails.setAdultCount		(doc.getElementsByTag("AdultCount").first().text());
		//hotelDetails.setChildCount		(doc.getElementsByTag("ChildCount").first().text());
		ArrayList<String> a =  new ArrayList<String>();
		Iterator<Element> 		guestElements		= doc.getElementsByTag("HotelOccupancy").iterator();
		ArrayList<roomDetails> 	roomDetailsList		=  new ArrayList<roomDetails>();
		while(guestElements.hasNext())
		{
			Element	currentElement					= guestElements.next();
			roomDetails 				roomDetails	=  new roomDetails();
			roomDetails.setAdultCount			(currentElement.getElementsByTag("AdultCount").first().text());
			roomDetails.setChildCount			(currentElement.getElementsByTag("ChildCount").first().text());
			Iterator<Element> 			customerElements	= currentElement.getElementsByTag("Customer").iterator();
			ArrayList<occupancyDetails>    		customer = new ArrayList<occupancyDetails>();
			while(customerElements.hasNext())
			{
				Element	customerElement							= customerElements.next();
				occupancyDetails			cus = new  occupancyDetails();
				cus.setType					(customerElement.getElementsByTag("Customer").first().attr("type"));
				cus.setAge					(customerElement.getElementsByTag("Age").first().text());
				customer.add(cus);
			}
			roomDetails.setOccupancyDetails(customer);
			roomDetailsList.add(roomDetails);
		}
		hotelDetails.setRoomDetails(roomDetailsList);
		return hotelDetails;
	
	}
	
	public ArrayList<hotelDetails> availabilityRes(String pageSource) throws IOException, InterruptedException
	{
		SetProxyPort 			proxyAndPort	= new SetProxyPort();
		Document 				doc  			= Jsoup.parse(proxyAndPort.setProxy(pageSource));
		Thread.sleep(2000);
		ArrayList<hotelDetails> 		hotelDetailsList	= new ArrayList<hotelDetails>();
		
		String					count			= doc.getElementsByTag("HotelValuedAvailRS").first().attr("totalItems");
		int     				icount			= Integer.parseInt(count);
		if(icount != 0)
		{
			Element 				auditData	= doc.getElementsByTag("AuditData").first();
			Iterator<Element> serviceHotelIterator			= doc.getElementsByTag("ServiceHotel").iterator();
			while(serviceHotelIterator.hasNext())
			{
				try {

					Element			aaa					= serviceHotelIterator.next();
					hotelDetails 	hotelDetails		= new hotelDetails();
					Element 		hotelInfo			= aaa.getElementsByTag("HotelInfo").first();
					Element			destinationInfo		= hotelInfo.getElementsByTag("Destination").first();
					
					hotelDetails.setHotelType		(hotelInfo.getElementsByTag("HotelInfo").first().attr("xsi:type"));
					hotelDetails.setHotelCode		(hotelInfo.getElementsByTag("Code").first().text());
					hotelDetails.setHotel			(hotelInfo.getElementsByTag("Name").first().text());
					hotelDetails.setStarRating		(hotelInfo.getElementsByTag("Category").first().text());
					hotelDetails.setStarRatingCode	(hotelInfo.getElementsByTag("Category").first().attr("code").replace("EST", ""));
					hotelDetails.setCurrencyCode	(aaa.getElementsByTag("Currency").first().attr("code"));
					hotelDetails.setCheckin			(aaa.getElementsByTag("DateFrom").first().attr("date"));
					hotelDetails.setCheckout		(aaa.getElementsByTag("DateTo").first().attr("date"));
					
					
					hotelDetails.setLocationCode	(destinationInfo.getElementsByTag("Destination").first().attr("code"));
					hotelDetails.setCityCode		(destinationInfo.getElementsByTag("Destination").first().attr("code"));
					hotelDetails.setCity			(destinationInfo.getElementsByTag("Destination").first().attr("Name"));
					hotelDetails.setZone			(destinationInfo.getElementsByTag("Zone").first().text());
					hotelDetails.setChildAgeFrom	(hotelInfo.getElementsByTag("ChildAge").first().attr("ageFrom"));
					hotelDetails.setChildAgeTo		(hotelInfo.getElementsByTag("ChildAge").first().attr("ageTo"));
					
					Iterator<Element> roomOccupancy				= aaa.getElementsByTag("AvailableRoom").iterator();
					ArrayList<roomDetails>	occupancyDetailsList	= new ArrayList<roomDetails>();
					while(roomOccupancy.hasNext())
					{
						Element	currentElement = roomOccupancy.next();
						roomDetails occupancyDetails = new roomDetails();
						
						occupancyDetails.setRoomCount		(String.valueOf(aaa.getElementsByTag("RoomCount").size()));
						occupancyDetails.setAdultCount		(currentElement.getElementsByTag("AdultCount").first().text());
						occupancyDetails.setChildCount		(currentElement.getElementsByTag("ChildCount").first().text());
						occupancyDetails.setAvailability	(currentElement.getElementsByTag("HotelRoom").first().attr("onRequest"));
						occupancyDetails.setBoard			(currentElement.getElementsByTag("Board").first().text());
						occupancyDetails.setRoomType		(currentElement.getElementsByTag("RoomType").first().text());
						occupancyDetails.setPrice			(currentElement.getElementsByTag("Amount").first().text());
						
						occupancyDetailsList.add(occupancyDetails);
					}
					hotelDetails.setRoomDetails(occupancyDetailsList);
					hotelDetailsList.add(hotelDetails);
				
				} catch (Exception e) {
					// TODO: handle exception
					System.out.println(e);
				}
			}	
		}
		else
		{
			System.out.println("No results available");
		}
		
		return hotelDetailsList;
	}
	
	public hotelDetails roomAvailabilityReq(String pageSource) throws IOException
	{

		SetProxyPort 			proxyAndPort	= new SetProxyPort();
		Document 				doc  			= Jsoup.parse(proxyAndPort.setProxy(pageSource));
		
		hotelDetails			hotelDetails 	= new hotelDetails();
		hotelDetails.setUser					(doc.getElementsByTag("User").first().text());
		//System.out.println(hb.getUser());
		hotelDetails.setPassword				(doc.getElementsByTag("Password").first().text());
		//System.out.println(hb.getPassword());
		hotelDetails.setCheckin					(doc.getElementsByTag("DateFrom").first().attr("date"));
		//System.out.println(hb.getCheckin());
		hotelDetails.setCheckout				(doc.getElementsByTag("DateTo").first().attr("date"));
		//System.out.println(hb.getCheckout());
		hotelDetails.setCity					(doc.getElementsByTag("Destination").first().attr("code"));
		//System.out.println(hb.getDestination());
		hotelDetails.setRoomCount				(String.valueOf(doc.getElementsByTag("RoomCount").size()));
		//System.out.println(hb.getRoomCount());
		//hb.setAdultCount		(doc.getElementsByTag("AdultCount").first().text());
		//hb.setChildCount		(doc.getElementsByTag("ChildCount").first().text());
		
		Iterator<Element> 		guestElements			= doc.getElementsByTag("HotelOccupancy").iterator();
		ArrayList<roomDetails> 	roomDetailsList			= new ArrayList<roomDetails>();
		while(guestElements.hasNext())
		{
			Element			currentElement				= guestElements.next();
			roomDetails 	roomDetails 				= new roomDetails();
			roomDetails.setAdultCount			(currentElement.getElementsByTag("AdultCount").first().text());
			roomDetails.setChildCount			(currentElement.getElementsByTag("ChildCount").first().text());
			Iterator<Element> 					customerElements	= currentElement.getElementsByTag("Customer").iterator();
			ArrayList<occupancyDetails>    		occupancyDetailsList 			= new ArrayList<occupancyDetails>();
			while(customerElements.hasNext())
			{
				Element	customerElement					= customerElements.next();
				occupancyDetails 	occupancyDetails 	= new occupancyDetails();
				occupancyDetails.setType					(customerElement.getElementsByTag("Customer").first().attr("type"));
				//System.out.println(cus.getType());
				occupancyDetails.setAge					(customerElement.getElementsByTag("Age").first().text());
				//System.out.println(cus.getAge());
				occupancyDetailsList.add(occupancyDetails);
			}
			roomDetails.setOccupancyDetails(occupancyDetailsList);
			roomDetailsList.add(roomDetails);
		}
		hotelDetails.setRoomDetails(roomDetailsList);
		return hotelDetails;
	
	}
	
	public hotelDetails roomAvailabilityRes(String pageSource) throws IOException, InterruptedException
	{

		//System.out.println("test -- 3.0.1");
		SetProxyPort 			proxyAndPort	= new SetProxyPort();
		Document 				doc  			= Jsoup.parse(proxyAndPort.setProxy(pageSource));
		Thread.sleep(2000);
		hotelDetails			hotelDetails = new hotelDetails();
		Element 				availability	= doc.getElementsByTag("ServiceAddRS").first();
		//System.out.println("test -- 3.0.2");
		try {
			try {
				hotelDetails.setError					(doc.getElementsByTag("DetailedMessage").first().text());
				//System.out.println(hotel.getError());
			} catch (Exception e) {
				// TODO: handle exception
			}
			Element				serviceElement		= doc.getElementsByTag("ServiceList").first(); 
			Element				hotelInfoElement	= doc.getElementsByTag("HotelInfo").first();
			hotelDetails.setAvailabilityStatus		(serviceElement.getElementsByTag("Status").first().text());
			//System.out.println("test -- 3.0.5");
			hotelDetails.setIncomingOfficeCode		(serviceElement.getElementsByTag("IncomingOffice").first().text());
			try {
				hotelDetails.setDescription			(serviceElement.getElementsByTag("Comment").first().text());
				hotelDetails.setDescriptionType		(serviceElement.getElementsByTag("Comment").first().attr("type"));

			} catch (Exception e) {
				// TODO: handle exception
			}
			//System.out.println("test -- 3.0.6");
			try {
				hotelDetails.setProvider				(serviceElement.getElementsByTag("Supplier").first().attr("name"));
				hotelDetails.setCheckin					(serviceElement.getElementsByTag("DateFrom").first().attr("date"));
				hotelDetails.setCheckout				(serviceElement.getElementsByTag("DateTo").first().attr("date"));
				SimpleDateFormat myFormat = new SimpleDateFormat("yyyyMMdd");
				Date date1 = myFormat.parse(hotelDetails.getCheckin());
				Date date2 = myFormat.parse(hotelDetails.getCheckout());
				long diff = date1.getTime() - date2.getTime();
				hotelDetails.setNights(String.valueOf(TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS)));
				//hotel.setCurrency						(serviceElement.getElementsByTag("Currency").first().text());
				hotelDetails.setCurrencyCode			(serviceElement.getElementsByTag("Currency").first().attr("code"));
				hotelDetails.setPrice					(serviceElement.getElementsByTag("TotalAmount").first().text());
				//System.out.println(hotel.getTotalAmount());
			} catch (Exception e) {
				// TODO: handle exception
			}
			
			//System.out.println("test -- 3.0.7");
			ArrayList<hbAdditionalCost>  	additionalCostList 	= new ArrayList<hbAdditionalCost>();
			Iterator<Element> additionalCostIterator			= doc.getElementsByTag("AdditionalCost").iterator();
			while(additionalCostIterator.hasNext())
			{
				Element							costElement		= additionalCostIterator.next();
				hbAdditionalCost 				cost 			= new hbAdditionalCost();
				cost.setCostType	(costElement.getElementsByTag("AdditionalCost").first().attr("type"));
				cost.setPrice				(costElement.getElementsByTag("Amount").first().text());
				additionalCostList.add(cost);
			}
			hotelDetails.setHbAdditionalCost(additionalCostList);
			
			//discount
			ArrayList<discount> 		discountList = new ArrayList<discount>();
			try {
				Element 					discountElement  = doc.getElementsByTag("DiscountList").first();
				Iterator<Element> 			discountIterator = discountElement.getElementsByTag("Price").iterator();
				while(discountIterator.hasNext())
				{
					Element 	currentDiscount = discountIterator.next();
					discount discount = new discount();
					discount.setUnitCount			(currentDiscount.getElementsByTag("Price").first().attr("unitCount"));
					discount.setPaxCount			(currentDiscount.getElementsByTag("Price").first().attr("paxCount"));
					discount.setAmount				(currentDiscount.getElementsByTag("Amount").first().text());
					discount.setCheckin				(currentDiscount.getElementsByTag("DateTimeFrom").first().attr("date"));
					discount.setCheckout			(currentDiscount.getElementsByTag("DateTimeTo").first().attr("date"));
					discount.setDescription			(currentDiscount.getElementsByTag("Description").first().text());	
					discountList.add(discount);
				}
			} catch (Exception e) {
				// TODO: handle exception
			}
			hotelDetails.setDiscount(discountList);
			
			//System.out.println("test -- 3.0.8");
			try {
				hotelDetails.setHotelType				(serviceElement.getElementsByTag("HotelInfo").first().attr("xsi:type"));
				hotelDetails.setHotelCode				(serviceElement.getElementsByTag("Code").first().text());
				hotelDetails.setHotel					(hotelInfoElement.getElementsByTag("Name").first().text());
				hotelDetails.setCategory				(serviceElement.getElementsByTag("Category").first().text());
				hotelDetails.setCityCode				(serviceElement.getElementsByTag("Destination").first().attr("code"));
				hotelDetails.setCity					(serviceElement.getElementsByTag("Name").first().text());
				hotelDetails.setZoneCode				(serviceElement.getElementsByTag("Zone").first().attr("code"));
				hotelDetails.setZoneType				(serviceElement.getElementsByTag("Zone").first().attr("type"));
			} catch (Exception e) {
				// TODO: handle exception
			}
			
			//System.out.println("test -- 3.0.9");
			ArrayList<roomDetails>  	roomDetailsList 	= new ArrayList<roomDetails>();
			hotelDetails.setRoomCount(String.valueOf(doc.getElementsByTag("AvailableRoom").size()));
			Iterator<Element> 				avaialableRoomIterator		= doc.getElementsByTag("AvailableRoom").iterator();
			//System.out.println(doc.getElementsByTag("HotelOccupancy").size());
			while(avaialableRoomIterator.hasNext())
			{
				Element						currentElement	= avaialableRoomIterator.next();
				roomDetails 				roomDetails = new roomDetails();
				roomDetails.setAdultCount				(currentElement.getElementsByTag("AdultCount").first().text());
				roomDetails.setChildCount				(currentElement.getElementsByTag("ChildCount").first().text());
				roomDetails.setRoomCount				(currentElement.getElementsByTag("RoomCount").first().text());
				
				
				ArrayList<occupancyDetails>  			occupancyList		= new ArrayList<occupancyDetails>();
				Iterator<Element> 				guestIterator	= currentElement.getElementsByTag("Customer").iterator();
				while(guestIterator.hasNext())
				{
					Element							newElement	= guestIterator.next();
					occupancyDetails occupancy = new occupancyDetails();
					
					occupancy.setAge				(newElement.getElementsByTag("Age").first().text());
					//System.out.println(guest.getCustomerAge());
					occupancy.setId			(newElement.getElementsByTag("CustomerId").first().text());
					//System.out.println(guest.getCustomerid());
					occupancy.setType		(newElement.getElementsByTag("Customer").first().attr("type"));
					//System.out.println(guest.getCustomerType());
					
					occupancyList.add(occupancy);
				}
				roomDetails.setOccupancyDetails(occupancyList);
				//-----------------------------------------------------------------------------------------------------------------
				
				roomDetails.setRoomAvailableCount	(currentElement.getElementsByTag("HotelRoom").first().attr("availCount"));
				roomDetails.setAvailability			(currentElement.getElementsByTag("HotelRoom").first().attr("status"));
				roomDetails.setBoard				(currentElement.getElementsByTag("Board").first().text());
				roomDetails.setBoardCode			(currentElement.getElementsByTag("Board").first().attr("code"));
				roomDetails.setBoardType			(currentElement.getElementsByTag("Board").first().attr("type"));
				roomDetails.setRoomType				(currentElement.getElementsByTag("RoomType").first().attr("type"));
				roomDetails.setRoomID				(currentElement.getElementsByTag("RoomType").first().attr("code"));
				roomDetails.setRoomCharacteristics	(currentElement.getElementsByTag("RoomType").first().attr("characteristic"));
				roomDetails.setRoomName				(currentElement.getElementsByTag("RoomType").first().text());
				roomDetails.setPrice				(currentElement.getElementsByTag("Amount").first().text());
				//System.out.println(hotelRoom.getPrice());
				
				Element cancelElement				= currentElement.getElementsByTag("CancellationPolicy").first();
				roomDetails.setCancellationAmount	(cancelElement.getElementsByTag("Amount").first().text());
				roomDetails.setCancellationFrom		(cancelElement.getElementsByTag("DateTimeFrom").first().attr("date"));
				roomDetails.setCanellationTo		(cancelElement.getElementsByTag("DateTimeTo").first().attr("date"));
				roomDetails.setCancellationTime		(cancelElement.getElementsByTag("DateTimeTo").first().attr("time"));
				roomDetailsList.add(roomDetails);
			}
			//System.out.println("test -- 3.0.10");
			hotelDetails.setRoomDetails(roomDetailsList);
		} catch (Exception e) {
			// TODO: handle exception
		}
			return hotelDetails;
	}
	
	public hotelDetails preReservationReq(String pageSource) throws IOException, InterruptedException
	{
		SetProxyPort 			proxyAndPort	= new SetProxyPort();
		Document 				doc  			= Jsoup.parse(proxyAndPort.setProxy(pageSource));
		Thread.sleep(2000);
		hotelDetails				hotelDetails = new hotelDetails();
		Element availability		= doc.getElementsByTag("ServiceAddRQ").first();
		
		
		hotelDetails.setUser				(availability.getElementsByTag("User").first().text());
		hotelDetails.setPassword			(availability.getElementsByTag("Password").first().text());
		hotelDetails.setCheckin				(availability.getElementsByTag("DateFrom").first().attr("date"));
		hotelDetails.setCheckout			(availability.getElementsByTag("DateTo").first().attr("date"));
		hotelDetails.setHotelType			(availability.getElementsByTag("HotelInfo").first().attr("xsi:type"));
		hotelDetails.setHotelCode			(availability.getElementsByTag("Code").first().text());
		hotelDetails.setCity				(availability.getElementsByTag("Destination ").first().attr("code"));
		
		
		ArrayList<roomDetails>  	roomList = new ArrayList<roomDetails>();
		Iterator<Element> roomIterator		= doc.getElementsByTag("AvailableRoom").iterator();
		while(roomIterator.hasNext())
		{
			Element	roomElement					= roomIterator.next();
			roomDetails room = new roomDetails();
			
			room.setRoomCount			(roomElement.getElementsByTag("RoomCount").first().text());
			room.setAdultCount			(roomElement.getElementsByTag("AdultCount").first().text());
			room.setChildCount			(roomElement.getElementsByTag("ChildCount").first().text());
			room.setBoardCode			(roomElement.getElementsByTag("Board").first().attr("code"));
			room.setBoardType			(roomElement.getElementsByTag("Board").first().attr("type"));
			room.setRoomID				(roomElement.getElementsByTag("RoomType").first().attr("code"));
			room.setRoomType			(roomElement.getElementsByTag("RoomType").first().attr("type"));
			
			ArrayList<occupancyDetails> occupancyList		= new ArrayList<occupancyDetails>();
			Iterator<Element> guestIterator 	= roomElement.getElementsByTag("Customer").iterator();
			while(guestIterator.hasNext())
			{
				Element				guest		= guestIterator.next();
				occupancyDetails occupancy = new occupancyDetails();
				
				occupancy.setAge			(guest.getElementsByTag("Age").first().text());
				occupancy.setType			(guest.getElementsByTag("Customer").first().attr("type"));
				occupancyList.add(occupancy);
			}
			room.setOccupancyDetails(occupancyList);
			roomList.add(room);
			
			
		}
		
		hotelDetails.setRoomDetails(roomList);
		return hotelDetails;
	}
	
	public hotelDetails preReservationResponse(String pageSource) throws IOException, InterruptedException
	{

		//System.out.println("test -- 3.0.1");
		SetProxyPort 			proxyAndPort	= new SetProxyPort();
		Document 				doc  			= Jsoup.parse(proxyAndPort.setProxy(pageSource));
		Thread.sleep(2000);
		hotelDetails hotelDetails = new hotelDetails();
		Element 				availability	= doc.getElementsByTag("ServiceAddRS").first();
		//System.out.println("test -- 3.0.2");
		try {
			try {
				hotelDetails.setError					(doc.getElementsByTag("DetailedMessage").first().text());
				//System.out.println(hotel.getError());
			} catch (Exception e) {
				// TODO: handle exception
			}
			
			Element				serviceElement		= doc.getElementsByTag("ServiceList").first(); 
			Element				hotelInfoElement	= doc.getElementsByTag("HotelInfo").first();
			hotelDetails.setAvailabilityStatus		(serviceElement.getElementsByTag("Status").first().text());
			//System.out.println("test -- 3.0.5");
			hotelDetails.setIncomingOfficeCode		(serviceElement.getElementsByTag("IncomingOffice").first().text());
			try {
				hotelDetails.setDescription			(serviceElement.getElementsByTag("Comment").first().text());
				hotelDetails.setDescriptionType		(serviceElement.getElementsByTag("Comment").first().attr("type"));

			} catch (Exception e) {
				// TODO: handle exception
			}
			//System.out.println("test -- 3.0.6");
			try {
				hotelDetails.setProvider				(serviceElement.getElementsByTag("Supplier").first().attr("name"));
				hotelDetails.setCheckin					(serviceElement.getElementsByTag("DateFrom").first().attr("date"));
				hotelDetails.setCheckout				(serviceElement.getElementsByTag("DateTo").first().attr("date"));
				//hotel.setCurrency				(serviceElement.getElementsByTag("Currency").first().text());
				hotelDetails.setCurrencyCode			(serviceElement.getElementsByTag("Currency").first().attr("code"));
				hotelDetails.setPrice					(serviceElement.getElementsByTag("TotalAmount").first().text());
				//System.out.println(hotel.getTotalAmount());
			} catch (Exception e) {
				// TODO: handle exception
			}
			
			//System.out.println("test -- 3.0.7");
			ArrayList<hbAdditionalCost>  	additionalCostList 	= new ArrayList<hbAdditionalCost>();
			Iterator<Element> additionalCostIterator		= doc.getElementsByTag("AdditionalCost").iterator();
			while(additionalCostIterator.hasNext())
			{
				Element	cost								= additionalCostIterator.next();
				hbAdditionalCost additionalCost = new hbAdditionalCost();
				additionalCost.setCostType			(cost.getElementsByTag("AdditionalCost").first().attr("type"));
				additionalCost.setPrice				(cost.getElementsByTag("Amount").first().text());
				additionalCostList.add(additionalCost);
			}
			hotelDetails.setHbAdditionalCost(additionalCostList);
			
			//discount
			ArrayList<discount> 		discountList = new ArrayList<discount>();
			try {
				Element 					discountElement  = doc.getElementsByTag("DiscountList").first();
				Iterator<Element> 			discountIterator = discountElement.getElementsByTag("Price").iterator();
				while(discountIterator.hasNext())
				{
					Element 	currentDiscount = discountIterator.next();
					discount discount = new discount();
					discount.setUnitCount			(currentDiscount.getElementsByTag("Price").first().attr("unitCount"));
					discount.setPaxCount			(currentDiscount.getElementsByTag("Price").first().attr("paxCount"));
					discount.setAmount				(currentDiscount.getElementsByTag("Amount").first().text());
					discount.setCheckin				(currentDiscount.getElementsByTag("DateTimeFrom").first().attr("date"));
					discount.setCheckout			(currentDiscount.getElementsByTag("DateTimeTo").first().attr("date"));
					discount.setDescription			(currentDiscount.getElementsByTag("Description").first().text());	
					discountList.add(discount);
				}
			} catch (Exception e) {
				// TODO: handle exception
			}
			hotelDetails.setDiscount(discountList);
			
			//System.out.println("test -- 3.0.8");
			try {
				hotelDetails.setHotelCode				(serviceElement.getElementsByTag("Code").first().text());
				hotelDetails.setHotel					(hotelInfoElement.getElementsByTag("Name").first().text());
				hotelDetails.setCategory				(serviceElement.getElementsByTag("Category").first().text());
				hotelDetails.setCityCode				(serviceElement.getElementsByTag("Destination").first().attr("code"));
				hotelDetails.setCity					(serviceElement.getElementsByTag("Name").first().text());
				hotelDetails.setZoneCode					(serviceElement.getElementsByTag("Zone").first().attr("code"));
				hotelDetails.setZoneType					(serviceElement.getElementsByTag("Zone").first().attr("type"));
			} catch (Exception e) {
				// TODO: handle exception
			}
			
			//System.out.println("test -- 3.0.9");
			ArrayList<roomDetails>  	roomList 	= new ArrayList<roomDetails>();
			Iterator<Element> 				avaialableRoomIterator		= doc.getElementsByTag("HotelOccupancy").iterator();
			//System.out.println(doc.getElementsByTag("HotelOccupancy").size());
			while(avaialableRoomIterator.hasNext())
			{
				Element						currentElement	= avaialableRoomIterator.next();
				roomDetails			room			= new roomDetails();
				room.setAdultCount				(currentElement.getElementsByTag("AdultCount").first().text());
				room.setChildCount				(currentElement.getElementsByTag("ChildCount").first().text());
				room.setRoomCount				(currentElement.getElementsByTag("RoomCount").first().text());
				
				
				ArrayList<occupancyDetails>  	occupancyList		= new ArrayList<occupancyDetails>();
				Iterator<Element> 				guestIterator	= currentElement.getElementsByTag("Customer").iterator();
				while(guestIterator.hasNext())
				{
												currentElement	= guestIterator.next();
					occupancyDetails			occupancy			= new occupancyDetails();
					
					occupancy.setAge			(currentElement.getElementsByTag("Age").first().text());
					occupancy.setId				(currentElement.getElementsByTag("CustomerId").first().text());
					occupancy.setType			(currentElement.getElementsByTag("Customer").first().attr("type"));
					
					occupancyList.add(occupancy);
				}
				room.setOccupancyDetails(occupancyList);
				room.setRoomAvailableCount	(currentElement.getElementsByTag("HotelRoom").first().attr("availCount"));
				room.setAvailability		(currentElement.getElementsByTag("HotelRoom").first().attr("status"));
				room.setBoard				(currentElement.getElementsByTag("Board").first().text());
				room.setBoardCode			(currentElement.getElementsByTag("Board").first().attr("code"));
				room.setBoardType			(currentElement.getElementsByTag("Board").first().attr("type"));
				room.setRoomType			(currentElement.getElementsByTag("RoomType").first().attr("type"));
				room.setRoomID				(currentElement.getElementsByTag("RoomType").first().attr("code"));
				room.setRoomCharacteristics	(currentElement.getElementsByTag("RoomType").first().attr("characteristic"));
				room.setRoomName			(currentElement.getElementsByTag("RoomType").first().text());
				room.setPrice				(currentElement.getElementsByTag("Amount").first().text());
				Element cancelElement			= currentElement.getElementsByTag("CancellationPolicy").first();
				room.setCancellationAmount	(cancelElement.getElementsByTag("Amount").first().text());
				room.setCancellationFrom	(cancelElement.getElementsByTag("DateTimeFrom").first().attr("date"));
				//System.out.println(hotelRoom.getDateTimeFromDate());
				room.setCanellationTo		(cancelElement.getElementsByTag("DateTimeTo").first().attr("date"));
				roomList.add(room);
			}
			
			hotelDetails.setRoomDetails(roomList);
				
		} catch (Exception e) {
			// TODO: handle exception
		}
		
			return hotelDetails;
	}
	
	public hotelDetails reservationReq(String pageSource) throws IOException, InterruptedException
	{

		SetProxyPort 			proxyAndPort	= new SetProxyPort();
		Document 				doc  			= Jsoup.parse(proxyAndPort.setProxy(pageSource));
		Thread.sleep(2000);
		hotelDetails hotel = new hotelDetails();
		Element availability		= doc.getElementsByTag("PurchaseConfirmRQ").first();
		
		try {
			hotel.setUser				(availability.getElementsByTag("User").first().text());
			hotel.setPassword			(availability.getElementsByTag("Password").first().text());
		} catch (Exception e) {
			// TODO: handle exception
		}
		hotel.setFirstName			(availability.getElementsByTag("Name").first().text());
		hotel.setLastName			(availability.getElementsByTag("LastName").first().text());
		
		ArrayList<occupancyDetails>  	occupancyList = new ArrayList<occupancyDetails>();
		Iterator<Element> roomIterator		= doc.getElementsByTag("Customer").iterator();
		while(roomIterator.hasNext())
		{
			Element	room					= roomIterator.next();
			occupancyDetails			occupancy		= new occupancyDetails();
			
			occupancy.setType				(room.getElementsByTag("Customer").first().attr("type"));
			occupancy.setId					(room.getElementsByTag("CustomerId").first().text());
			occupancy.setAge				(room.getElementsByTag("Age").first().text());
			occupancy.setFirstName			(room.getElementsByTag("Name").first().text());
			occupancy.setLastName			(room.getElementsByTag("LastName").first().text());	
			occupancyList.add(occupancy);
		}
		
		hotel.setOccupancy(occupancyList);
		
		//System.out.println(hotel.getDateFromDate());
		//System.out.println(hotel.getRoomAvailability().get(0).getGuestList().get(0).getCustomerAge());
		return hotel;
		
	
	}
	
	public hotelDetails reservationResponse(String pageSource) throws IOException, InterruptedException
	{


		SetProxyPort 			proxyAndPort	= new SetProxyPort();
		Document 				doc  			= Jsoup.parse(proxyAndPort.setProxy(pageSource));
		Thread.sleep(2000);
		hotelDetails			hotel			= new hotelDetails(); 
		Element 				availability 	= doc.getElementsByTag("PurchaseConfirmRS").first();
		Calender				date			= new Calender();
		
		try {
			hotel.setError				(availability.getElementsByTag("DetailedMessage").first().text());
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		try {
			try {
				hotel.setSupConfirmationNumber	(doc.getElementsByTag("BookingReference").get(1).text());
			} catch (Exception e) {
				// TODO: handle exception
			}
			hotel.setRoomCount				(String.valueOf(doc.getElementsByTag("HotelRoom").size()));
			hotel.setFirstName				(availability.getElementsByTag("Name").first().text());
			hotel.setLastName				(availability.getElementsByTag("LastName").first().text());
			String supConNum = availability.getElementsByTag("IncomingOffice").first().attr("code").concat("-").concat(availability.getElementsByTag("FileNumber").first().text());
			hotel.setSupConfirmationNumber	(supConNum);
			
			Element				serviceElement		= doc.getElementsByTag("ServiceList").first(); 
			Element				hotelInfoElement	= doc.getElementsByTag("HotelInfo").first();
			hotel.setAvailabilityStatus				(serviceElement.getElementsByTag("Status").first().text());
		
			hotel.setIncomingOfficeCode		(serviceElement.getElementsByTag("IncomingOffice").first().text());
			try {
				hotel.setDescription			(serviceElement.getElementsByTag("Comment").first().text());
				hotel.setDescriptionType		(serviceElement.getElementsByTag("Comment").first().attr("type"));

			} catch (Exception e) {
				// TODO: handle exception
			}
			
			hotel.setProvider				(serviceElement.getElementsByTag("Supplier").first().attr("name"));
			hotel.setCheckin				(serviceElement.getElementsByTag("DateFrom").first().attr("date"));
			hotel.setCheckout				(serviceElement.getElementsByTag("DateTo").first().attr("date"));
			SimpleDateFormat 	sdf 		= new SimpleDateFormat("yyyyMMdd");
			Date day1 = sdf.parse(hotel.getCheckin());
			Date day2 = sdf.parse(hotel.getCheckout());
			hotel.setNights(date.differenceBetweenDays(day1, day2));
			
			
			hotel.setCurrency				(serviceElement.getElementsByTag("Currency").first().text());
			hotel.setCurrencyCode			(serviceElement.getElementsByTag("Currency").first().attr("code"));
			hotel.setPrice					(serviceElement.getElementsByTag("TotalAmount").first().text());
			
			ArrayList<hbAdditionalCost>  	additionalCost 	= new ArrayList<hbAdditionalCost>();
			Iterator<Element> additionalCostIterator		= doc.getElementsByTag("AdditionalCost").iterator();
			while(additionalCostIterator.hasNext())
			{
				Element	cost								= additionalCostIterator.next();
				hbAdditionalCost 			addcost			= new hbAdditionalCost();
				addcost.setCostType				(cost.getElementsByTag("AdditionalCost").first().attr("type"));
				addcost.setPrice				(cost.getElementsByTag("Amount").first().text());
				additionalCost.add(addcost);
			}
			hotel.setHbAdditionalCost(additionalCost);
			
			ArrayList<errorList> errorList = new ArrayList<errorList>();
			Iterator<Element> errorListIterator = doc.getElementsByTag("ErrorList").iterator();
			try {
				while(errorListIterator.hasNext())
				{
					errorList error = new errorList();
					Element errorElement = errorListIterator.next();
					error.setErrorCode		(errorElement.getElementsByTag("Code").first().text());
					error.setTimeStamp		(errorElement.getElementsByTag("Timestamp").first().text());
					error.setErrorType		(errorElement.getElementsByTag("Message").first().text());
					error.setErrorMessage	(errorElement.getElementsByTag("DetailedMessage").first().text());
					errorList.add(error);
				}
			} catch (Exception e) {
				// TODO: handle exception
			}
			hotel.setErrorList(errorList);
			
			
			
			hotel.setHotelType					(serviceElement.getElementsByTag("HotelInfo").first().attr("xsi:type"));
			hotel.setHotelCode					(serviceElement.getElementsByTag("Code").first().text());
			hotel.setHotel						(hotelInfoElement.getElementsByTag("Name").first().text());
			hotel.setStarRating					(serviceElement.getElementsByTag("Category").first().text());
			hotel.setCityCode					(serviceElement.getElementsByTag("Destination").first().attr("code"));
			Element destinationElement			= serviceElement.getElementsByTag("Destination").first();
			hotel.setCity						(destinationElement.getElementsByTag("Name").first().text());
			hotel.setZoneCode					(serviceElement.getElementsByTag("Zone").first().attr("code"));
			hotel.setZoneType					(serviceElement.getElementsByTag("Zone").first().attr("type"));
			
			ArrayList<roomDetails>  	availableRoom 	= new ArrayList<roomDetails>();
			Iterator<Element> 				avaialableRoomIterator		= doc.getElementsByTag("AvailableRoom").iterator();
			//System.out.println(doc.getElementsByTag("HotelOccupancy").size());
			while(avaialableRoomIterator.hasNext())
			{
				Element						currentElement	= avaialableRoomIterator.next();
				System.out.println(currentElement.getElementsByTag("AdultCount").first().text());
				
				roomDetails			room			= new roomDetails();
				room.setAdultCount			(currentElement.getElementsByTag("AdultCount").first().text());
				room.setChildCount			(currentElement.getElementsByTag("ChildCount").first().text());
				room.setRoomCount			(currentElement.getElementsByTag("RoomCount").first().text());
				room.setRoomCount			(currentElement.getElementsByTag("HotelRoom").first().attr("availCount"));
				room.setStatus				(currentElement.getElementsByTag("HotelRoom").first().attr("status"));
				room.setBoard				(currentElement.getElementsByTag("Board").first().text());
				room.setBoardCode			(currentElement.getElementsByTag("Board").first().attr("code"));
				room.setBoardType			(currentElement.getElementsByTag("Board").first().attr("type"));
				room.setRoomType			(currentElement.getElementsByTag("RoomType").first().attr("type"));
				room.setRoomID				(currentElement.getElementsByTag("RoomType").first().attr("code"));
				room.setRoomCharacteristics	(currentElement.getElementsByTag("RoomType").first().attr("characteristic"));
				room.setRoomName			(currentElement.getElementsByTag("RoomType").first().text());
				room.setPrice				(currentElement.getElementsByTag("Amount").first().text());
				
				
				ArrayList<occupancyDetails>  		guestList		= new ArrayList<occupancyDetails>();
				Iterator<Element> 				guestIterator	= currentElement.getElementsByTag("Customer").iterator();
				while(guestIterator.hasNext())
				{
					Element						guestElement	= guestIterator.next();
					occupancyDetails			guest			= new occupancyDetails();
					
					guest.setAge				(guestElement.getElementsByTag("Age").first().text());
					//System.out.println(guest.getCustomerAge());
					guest.setId					(guestElement.getElementsByTag("CustomerId").first().text());
					//System.out.println(guest.getCustomerid());
					guest.setType				(guestElement.getElementsByTag("Customer").first().attr("type"));
					//System.out.println(guest.getCustomerType());
					guest.setFirstName			(guestElement.getElementsByTag("Name").first().text());
					guest.setLastName			(guestElement.getElementsByTag("LastName").first().text());
					
					guestList.add(guest);
				}
				room.setOccupancyDetails(guestList);
			
				try {
					Element cancelElement		= currentElement.getElementsByTag("CancellationPolicy").first();
					room.setCancellationAmount	(cancelElement.getElementsByTag("Amount").first().text());
					room.setCancellationFrom	(cancelElement.getElementsByTag("DateTimeFrom").first().attr("date"));
					room.setCanellationTo		(cancelElement.getElementsByTag("DateTimeTo").first().attr("date"));
				} catch (Exception e) {
					// TODO: handle exception
				}
				
				availableRoom.add(room);
			}
			
			hotel.setRoomDetails(availableRoom);	

		} catch (Exception e) {
			// TODO: handle exception
		}
					//System.out.println(roomAvailability.get(0).getCancellationCodet());
//			
//			System.out.println(hotel.getAgencyCode());
//			System.out.println(hotel.getHotelRoom().get(0).getPrice());
			return hotel;
	
	
	}
}
