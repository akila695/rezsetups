package com.rezrobot.processor;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;

import org.apache.log4j.Logger;

import com.rezrobot.common.pojo.hotelDetails;
/*import com.types.ChargeByType;

import app.support.CancellationPolicy;
import app.support.DataLoader;
import app.support.RoomType;
import app.support.SearchType;*/
import com.rezrobot.utill.Calender;

public class HotelCancellationPolicyProcesser {

/*	private SearchType CurrentSearch;
	private Date BookingDate;
	private Date CurrentDate;
	private CancellationPolicy ExpectedPolicy;
    private Logger logger;
	private ArrayList<HotelPolicy> HotelPolicyList;
	private SimpleDateFormat sdf;
	private ArrayList<String> Localpolicies;
	private Hotel SubjectedHotel;
	private DataLoader Loader;
	private String NoshowStr;

	public HotelCancellationPolicyProcesser(SearchType search, Hotel CurrentHotel) {

		
		logger             = Logger.getLogger(this.getClass());
		logger.info("Initializing Class");
		sdf                = new SimpleDateFormat("dd-MMM-yyyy");
		CurrentSearch      = search;
		ExpectedPolicy     = new CancellationPolicy();
		SubjectedHotel     = CurrentHotel;
		HotelPolicyList    = SubjectedHotel.getHotelPolicies();
		

	}

	public void ProcessPolicyList(Date BookingDate) {
		HashMap<Integer, HotelPolicy> HotelPolicyMap = new HashMap<Integer, HotelPolicy>();
		Iterator<HotelPolicy> it = HotelPolicyList.iterator();
		Date LocalDeadLine = new Date();

		while (it.hasNext()) {
			HotelPolicy CurrentPolicy = it.next();
			int ApplyBefore = Integer.parseInt(CurrentPolicy.getArrivalLessThan());
			int Buffer = Integer.parseInt(CurrentPolicy.getCancellatrionBuffer());
			int ActualApplied = ApplyBefore + Buffer;

			Calendar cal = Calendar.getInstance();
			CurrentDate = cal.getTime();
			cal.setTime(BookingDate);
			cal.add(Calendar.DATE, -(ActualApplied + 1));
			cal.set(Calendar.HOUR_OF_DAY, 23);
			cal.set(Calendar.MINUTE, 59);
			cal.set(Calendar.SECOND, 59);
			cal.set(Calendar.MILLISECOND, 0);

			LocalDeadLine = cal.getTime();
			CurrentPolicy.setPolicyStartDate(LocalDeadLine);
			HotelPolicyMap.put(ActualApplied, CurrentPolicy);

		}

		ArrayList<Integer> keyset = new ArrayList<Integer>(HotelPolicyMap.keySet());
		Collections.sort(keyset, Collections.reverseOrder());
		HotelPolicyList.clear();

		for (Integer key : keyset) {
			HotelPolicyList.add(HotelPolicyMap.get(key));
		}

	}

	public void setRefundString() throws ParseException {
		Localpolicies      = new ArrayList<String>();
		HotelPolicy EarliestPolicy = HotelPolicyList.get(0);

		if (EarliestPolicy.getPolicyStartDate().after(CurrentDate)) {
			// Eliminate time factor and compare

			Date newCurrentDate = sdf.parse(sdf.format(CurrentDate));
			Date newPolicyDate = sdf.parse(sdf.format(EarliestPolicy.getPolicyStartDate()));

			if (newCurrentDate.equals(newPolicyDate)) {
				String RefundString = "If you cancel on " + sdf.format(CurrentDate) + " you will be refunded your purchase price.";
				Localpolicies.add(RefundString);
			} else {
				String RefundString = "If you cancel between " + sdf.format(CurrentDate) + " and " + sdf.format(EarliestPolicy.getPolicyStartDate()) + " you will be refunded your purchase price.";
				Localpolicies.add(RefundString);
			}

			ExpectedPolicy.setDeadlineString("The last date to cancel this hotel is : " + sdf.format(EarliestPolicy.getPolicyStartDate()));
			ExpectedPolicy.setDeadline(sdf.format(EarliestPolicy.getPolicyStartDate()));
			ExpectedPolicy.setRefundable("Fully refundable if cancelled on or before :" + sdf.format(EarliestPolicy.getPolicyStartDate()));
			ExpectedPolicy.setWithinCancellation(false);
		} else {
			ExpectedPolicy.setWithinCancellation(true);
			ExpectedPolicy.setDeadlineString("This hotel is within cancellation");
			ExpectedPolicy.setDeadline(sdf.format(EarliestPolicy.getPolicyStartDate()));
			ExpectedPolicy.setRefundable("N/A");
		}

	}

	public void CalculateNoShow() {
		HotelPolicy LastPolicy = HotelPolicyList.get(HotelPolicyList.size() - 1);
		String SearchCurrency = CurrentSearch.getSearchCurrency();
		String SupplierCurrency = SubjectedHotel.getHotelCurrency();
		ChargeByType NoShowChargeByType = LastPolicy.getNoShowChargeByType();
		ArrayList<RoomType> SelectedRooms = CurrentSearch.getSelectedRoomObj();
		Double NoShowVal = 0.00;
		Double TotalBookingVal = 0.00;
		Double Value = 0.00;

		for (RoomType roomType : SelectedRooms) {
			TotalBookingVal += Double.parseDouble(roomType.getTotalRate());
		}

		if (NoShowChargeByType == ChargeByType.PERCENTAGE) {
			Value = Math.ceil(TotalBookingVal * ((Double.parseDouble(LastPolicy.getNoshowValue())) / 100));
		} else if (NoShowChargeByType == ChargeByType.VALUE) {
			Value = Double.parseDouble(LastPolicy.getNoshowValue());
		} else if (NoShowChargeByType == ChargeByType.NIGHTRATE) {
			if (Integer.parseInt(CurrentSearch.getNights()) > Integer.parseInt(LastPolicy.getNoshowValue())) {

				int nights = Integer.parseInt(LastPolicy.getNoshowValue());
				for (RoomType roomType : SelectedRooms) {
					ArrayList<String> DailyRates = roomType.getDailyRates();
					for (int i = 0; i < nights; i++) {
						Value += Double.parseDouble(DailyRates.get(i));
					}
				}
			}
		} else {
			Value = TotalBookingVal;

		}

		NoShowVal = Math.ceil(Loader.convertCurrency(Double.toString(Value), SupplierCurrency, SearchCurrency));
		NoshowStr = "If cancelled on or after the " + sdf.format(BookingDate) + " No Show Fee " + SearchCurrency + " " + String.format("%.2f",NoShowVal) + " applies.";

	}

	public void CalculateNoShow(Double ServiceFee) {
		HotelPolicy LastPolicy = HotelPolicyList.get(HotelPolicyList.size() - 1);
		String SearchCurrency = CurrentSearch.getSearchCurrency();
		String SupplierCurrency = SubjectedHotel.getHotelCurrency();
	
		ChargeByType NoShowChargeByType = LastPolicy.getNoShowChargeByType();
		ArrayList<RoomType> SelectedRooms = CurrentSearch.getSelectedRoomObj();
		Double NoShowVal = 0.00;
		Double TotalBookingVal = 0.00;
		Double Value = 0.00;

		for (RoomType roomType : SelectedRooms) {
			TotalBookingVal += Double.parseDouble(roomType.getTotalRate());
		}

		if (NoShowChargeByType == ChargeByType.PERCENTAGE) {
			Value = Math.ceil(TotalBookingVal * ((Double.parseDouble(LastPolicy.getNoshowValue())) / 100));
		} else if (NoShowChargeByType == ChargeByType.VALUE) {
			Value = Double.parseDouble(LastPolicy.getNoshowValue());
		} else if (NoShowChargeByType == ChargeByType.NIGHTRATE) {
			if (Integer.parseInt(CurrentSearch.getNights()) > Integer.parseInt(LastPolicy.getNoshowValue())) {

				int nights = Integer.parseInt(LastPolicy.getNoshowValue());
				for (RoomType roomType : SelectedRooms) {
					ArrayList<String> DailyRates = roomType.getDailyRates();
					for (int i = 0; i < nights; i++) {
						Value += Double.parseDouble(DailyRates.get(i));
					}
				}
			}
		} else {
			Value = TotalBookingVal;

		}

	
		NoShowVal                  = Math.ceil(Loader.convertCurrency(Double.toString(Value), SupplierCurrency, SearchCurrency)+ ServiceFee) ;
		NoshowStr                  = "If cancelled on or after the " + sdf.format(BookingDate) + " No Show Fee " + SearchCurrency + " " + String.format("%.2f",NoShowVal) + " applies.";

	}

	public void ProcessMiddlePolicies() throws ParseException {
		
		for (int i = 0; i < HotelPolicyList.size(); i++) {

			HotelPolicy CurrentPolicy = HotelPolicyList.get(i);
			HotelPolicy NextPolicy = null;

			try {
				NextPolicy = HotelPolicyList.get(i + 1);
			} catch (Exception e) {
				// TODO: handle exception
			}

			if (CurrentDate.after(CurrentPolicy.getPolicyStartDate())) {
				Calendar cal = Calendar.getInstance();
				cal.setTime(CurrentDate);
				cal.add(Calendar.DATE, -1);
				CurrentPolicy.setPolicyStartDate(cal.getTime());
			}

			if (NextPolicy != null) {

				if (!(CurrentDate.after(NextPolicy.getPolicyStartDate())))
					CurrentPolicy.setPolicyEndDate(NextPolicy.getPolicyStartDate());
				else
					CurrentPolicy.setPolicyApplicable(false);

			} else {
				Calendar cal = Calendar.getInstance();
				cal.setTime(BookingDate);
				cal.add(Calendar.DATE, -1);
				CurrentPolicy.setPolicyEndDate(cal.getTime());
			}

			if (CurrentPolicy.isPolicyApplicable()) {

				Calendar cal = Calendar.getInstance();
				cal.setTime(CurrentPolicy.getPolicyStartDate());
				cal.add(Calendar.DATE, 1);
				Date newStartDate = sdf.parse(sdf.format(cal.getTime()));
				Date newEndDate = sdf.parse(sdf.format(CurrentPolicy.getPolicyEndDate()));

				String SearchCurrency = CurrentSearch.getSearchCurrency();
				String SupplierCurrency = SubjectedHotel.getHotelCurrency();
				ChargeByType chargeByType = CurrentPolicy.getStdChargeByType();
				String PolicyString = "";

				if (newStartDate.equals(newEndDate)) {
					if (chargeByType == ChargeByType.PERCENTAGE) {
						PolicyString = "If you cancel on " + sdf.format(newStartDate) + " you will be charged " + CurrentPolicy.getStdValue() + " % of the purchase price.";
					} else if (chargeByType == ChargeByType.NIGHTRATE) {
						PolicyString = "If you cancel on " + sdf.format(newStartDate) + " you will be charged " + CurrentPolicy.getStdValue() + " nights room and tax charge";
					} else if (chargeByType == ChargeByType.VALUE) {

						Double ProfVal = getApplicableMarkUp();
						Double AppVal = Double.parseDouble(CurrentPolicy.getStdValue());
						String ActVal = CurrentPolicy.getStdValue();

						if (ProfVal > 0.0)
							ActVal = Double.toString(AppVal + ProfVal);
						else
							ActVal = Double.toString(AppVal * (1 + ProfVal)); // new BigDecimal(Math.ceil(Loader.convertCurrency(ActVal, SupplierCurrency,
																				// SearchCurrency))).stripTrailingZeros().toPlainString()

						PolicyString = "If you cancel on " + sdf.format(newStartDate) + " you will be charged " + SearchCurrency + " "
								+ new BigDecimal(Math.ceil(Loader.convertCurrency(ActVal, SupplierCurrency, SearchCurrency))).stripTrailingZeros().toPlainString() + " of the room and tax charge.";
					}
				} else {

					if (chargeByType == ChargeByType.PERCENTAGE) {
						PolicyString = "If you cancel between " + sdf.format(newStartDate) + " and " + sdf.format(newEndDate) + " you will be charged " + CurrentPolicy.getStdValue() + "% of the purchase price.";
					} else if (chargeByType == ChargeByType.NIGHTRATE) {
						PolicyString = "If you cancel between " + sdf.format(newStartDate) + " and " + sdf.format(newEndDate) + " you will be charged " + CurrentPolicy.getStdValue() + " nights room and tax charge";
					} else if (chargeByType == ChargeByType.VALUE) {

						Double ProfVal = getApplicableMarkUp();
						Double AppVal = Double.parseDouble(CurrentPolicy.getStdValue());
						String ActVal = CurrentPolicy.getStdValue();

						if (ProfVal > 1)
							ActVal = Double.toString(AppVal + ProfVal);
						else
							ActVal = Double.toString(AppVal * (1 + ProfVal)); // new BigDecimal(Math.ceil(Loader.convertCurrency(ActVal, SupplierCurrency,
																				// SearchCurrency))).stripTrailingZeros().toPlainString()

						PolicyString = "If you cancel between " + sdf.format(newStartDate) + " and " + sdf.format(newEndDate) + " you will be charged " + SearchCurrency + " "
								+ new BigDecimal(Math.ceil(Loader.convertCurrency(ActVal, SupplierCurrency, SearchCurrency))).stripTrailingZeros().toPlainString() + " of the room and tax charge.";
					}
				}
				Localpolicies.add(PolicyString);
			}
		}
	}

	public void ProcessMiddlePolicies(Double ServiceFee) throws ParseException {
		
		for (int i = 0; i < HotelPolicyList.size(); i++) {

			HotelPolicy CurrentPolicy = HotelPolicyList.get(i);
			HotelPolicy NextPolicy = null;

			try {
				NextPolicy = HotelPolicyList.get(i + 1);
			} catch (Exception e) {
				// TODO: handle exception
			}

			if (CurrentDate.after(CurrentPolicy.getPolicyStartDate())) {
				Calendar cal = Calendar.getInstance();
				cal.setTime(CurrentDate);
				cal.add(Calendar.DATE, -1);
				CurrentPolicy.setPolicyStartDate(cal.getTime());
			}

			if (NextPolicy != null) {

				if (!(CurrentDate.after(NextPolicy.getPolicyStartDate())))
					CurrentPolicy.setPolicyEndDate(NextPolicy.getPolicyStartDate());
				else
					CurrentPolicy.setPolicyApplicable(false);

			} else {
				Calendar cal = Calendar.getInstance();
				cal.setTime(BookingDate);
				cal.add(Calendar.DATE, -1);
				CurrentPolicy.setPolicyEndDate(cal.getTime());
			}

			if (CurrentPolicy.isPolicyApplicable()) {

				Calendar cal = Calendar.getInstance();
				cal.setTime(CurrentPolicy.getPolicyStartDate());
				cal.add(Calendar.DATE, 1);
				Date newStartDate = sdf.parse(sdf.format(cal.getTime()));
				Date newEndDate = sdf.parse(sdf.format(CurrentPolicy.getPolicyEndDate()));

				String SearchCurrency = CurrentSearch.getSearchCurrency();
				String SupplierCurrency = SubjectedHotel.getHotelCurrency();
				ChargeByType chargeByType = CurrentPolicy.getStdChargeByType();
				String PolicyString = "";

				if (newStartDate.equals(newEndDate)) {
					if (chargeByType == ChargeByType.PERCENTAGE) {
						PolicyString = "If you cancel on " + sdf.format(newStartDate) + " you will be charged " + CurrentPolicy.getStdValue() + " % of the purchase price.";
					} else if (chargeByType == ChargeByType.NIGHTRATE) {
						PolicyString = "If you cancel on " + sdf.format(newStartDate) + " you will be charged " + CurrentPolicy.getStdValue() + " nights room and tax charge";
					} else if (chargeByType == ChargeByType.VALUE) {

						Double ProfVal = getApplicableMarkUp();
						Double AppVal = Double.parseDouble(CurrentPolicy.getStdValue());
						String ActVal = CurrentPolicy.getStdValue();

						if (ProfVal > 0.0)
							ActVal = Double.toString(AppVal + ProfVal);
						else
							ActVal = Double.toString(AppVal * (1 + ProfVal)); // new BigDecimal(Math.ceil(Loader.convertCurrency(ActVal, SupplierCurrency,
																				// SearchCurrency))).stripTrailingZeros().toPlainString()

						PolicyString = "If you cancel on " + sdf.format(newStartDate) + " you will be charged " + SearchCurrency + " "
								+ new BigDecimal(Math.ceil(Loader.convertCurrency(ActVal, SupplierCurrency, SearchCurrency)+ ServiceFee)).stripTrailingZeros().toPlainString() + " of the room and tax charge.";
					}
				} else {

					if (chargeByType == ChargeByType.PERCENTAGE) {
						PolicyString = "If you cancel between " + sdf.format(newStartDate) + " and " + sdf.format(newEndDate) + " you will be charged " + CurrentPolicy.getStdValue() + "% of the purchase price.";
					} else if (chargeByType == ChargeByType.NIGHTRATE) {
						PolicyString = "If you cancel between " + sdf.format(newStartDate) + " and " + sdf.format(newEndDate) + " you will be charged " + CurrentPolicy.getStdValue() + " nights room and tax charge";
					} else if (chargeByType == ChargeByType.VALUE) {

						Double ProfVal = getApplicableMarkUp();
						Double AppVal = Double.parseDouble(CurrentPolicy.getStdValue());
						String ActVal = CurrentPolicy.getStdValue();

						if (ProfVal > 1)
							ActVal = Double.toString(AppVal + ProfVal);
						else
							ActVal = Double.toString(AppVal * (1 + ProfVal)); // new BigDecimal(Math.ceil(Loader.convertCurrency(ActVal, SupplierCurrency,
																				// SearchCurrency))).stripTrailingZeros().toPlainString()

						PolicyString = "If you cancel between " + sdf.format(newStartDate) + " and " + sdf.format(newEndDate) + " you will be charged " + SearchCurrency + " "
								+ new BigDecimal(Math.ceil(Loader.convertCurrency(ActVal, SupplierCurrency, SearchCurrency)+ ServiceFee)).stripTrailingZeros().toPlainString() + " of the room and tax charge.";
					}
				}
				Localpolicies.add(PolicyString);
			}
		}
	}

	public CancellationPolicy getExpectedCancellationPolicy(DataLoader loader) throws ParseException {
		SimpleDateFormat sdf1 = new SimpleDateFormat("MM/dd/yyyy");
		Loader = loader;
		try {
			BookingDate = sdf1.parse(CurrentSearch.getArrivalDate().trim());
		} catch (Exception e) {
			logger.fatal("Exception in parsing " + CurrentSearch.getArrivalDate() + " " + e.toString());
		}

		ProcessPolicyList(BookingDate);
		setRefundString();
		CalculateNoShow();
		ProcessMiddlePolicies();
		Localpolicies.add(NoshowStr);
		ExpectedPolicy.setPolicies(Localpolicies);
		return ExpectedPolicy;
	}

	public double getApplicableMarkUp() throws ParseException {
		ArrayList<ProfitMarkup> markups = SubjectedHotel.getHotelProfitMarkUps();
		ProfitMarkup ApplicableMarkup = null;
		Double ApplicableMarkupValue = 0.0;

		for (ProfitMarkup profitMarkup : markups) {
			Date StartDate = sdf.parse(profitMarkup.getFrom());
			Date EndDate = sdf.parse(profitMarkup.getTo());

			if ((BookingDate.equals(StartDate) || BookingDate.after(StartDate)) && ((BookingDate.equals(EndDate) || BookingDate.before(EndDate)))) {
				ApplicableMarkup = profitMarkup;
				break;
			}
		}

		if (ApplicableMarkup != null) {
			String ApplicablePattern = ApplicableMarkup.getApplicablePattern();
			String MarkUp = ApplicableMarkup.getAdultProfitMarkup();

			if (ApplicablePattern.trim().equalsIgnoreCase("Sun to Thu / Fri to Sat")) {
				String[] str = MarkUp.split("/");
				SimpleDateFormat sdf = new SimpleDateFormat("EEE");
				String DateString = sdf.format(BookingDate).trim();

				if (DateString.equalsIgnoreCase("Fri") || DateString.equalsIgnoreCase("Sat"))
					MarkUp = str[1];
				else
					MarkUp = str[0];
			}

			if (ApplicableMarkup.getChargeType() == ChargeByType.PERCENTAGE)
				ApplicableMarkupValue = Double.parseDouble(MarkUp.trim()) / 100;
			else
				ApplicableMarkupValue = Double.parseDouble(MarkUp.trim());
		}

		return ApplicableMarkupValue;
	}

	public CancellationPolicy getExpectedCancellationPolicy(DataLoader loader,Double ServiceFee) throws ParseException {
		ExpectedPolicy = new CancellationPolicy();
    	SimpleDateFormat sdf1 = new SimpleDateFormat("MM/dd/yyyy");
		Loader = loader;
		try {
			BookingDate = sdf1.parse(CurrentSearch.getArrivalDate().trim());
		} catch (Exception e) {
			logger.fatal("Exception in parsing " + CurrentSearch.getArrivalDate() + " " + e.toString());
		}

		ProcessPolicyList(BookingDate);
		setRefundString();
		CalculateNoShow(ServiceFee);
		ProcessMiddlePolicies(ServiceFee);
		Localpolicies.add(NoshowStr);
		ExpectedPolicy.setPolicies(Localpolicies);
		return ExpectedPolicy;
	}
	*/
	
	public String getCancellationDeadline(String sup, hotelDetails res) throws ParseException
	{
		String deadline = "";
		Calender cal = new Calender();
		if(sup.equals("hotelbeds_v2"))
		{
			//20160920
			deadline = cal.getDate("yyyyMMdd", "dd-MMM-yyyy", res.getRoomDetails().get(0).getCancellationFrom());
		}
		else if(sup.equals("INV27"))
		{
			for(int i = 0 ; i < res.getCancellation().size() ; i++)
			{
				if(res.getCancellation().get(i).getDeadline().isEmpty())
					break;
				deadline = res.getCancellation().get(i).getDeadline();
				deadline = cal.getDate("yyyy-MM-dd", "dd-MMM-yyyy", deadline);
			}
		}
		else if(sup.equals("rezlive"))
		{
			try {
				String[] date = res.getCancellation().get(0).getStartDate().split(" ");
				deadline = date[0].concat("-").concat(date[1]).concat("-").concat(date[2]);
			} catch (Exception e) {
				// TODO: handle exception
			}
		}
		int buffer = 3;
		if(sup.equals("hotelbeds_v2"))
		{
			if(!res.getRoomDetails().get(0).getCancellationTime().equals("2359"))
			{
				buffer = 4;
			}
		}
		Calendar d = Calendar.getInstance();
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yyyy");
		d.setTime(sdf.parse(deadline));
		d.add(Calendar.DATE, -buffer);
		deadline = sdf.format(d.getTime());
		System.out.println("Cancellation deadline - " + deadline);
		return deadline;
	}
}
