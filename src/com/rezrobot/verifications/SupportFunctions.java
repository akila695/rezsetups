package com.rezrobot.verifications;

import java.text.SimpleDateFormat;
import java.util.Calendar;

public class SupportFunctions {
	
	
public String getFPDepartureMonths(){

	Calendar cal = Calendar.getInstance();
	String ReturnVal = "Any";
	SimpleDateFormat sdf = new SimpleDateFormat("MMMM yyyy");
	ReturnVal=ReturnVal+" "+sdf.format(cal.getTime());
	for (int i = 0; i < 11; i++) {
	cal.add(Calendar.MONTH, 1);
	ReturnVal=ReturnVal+" "+sdf.format(cal.getTime());
	}
	
	return ReturnVal;
}


public static void main(String[] args) {
	SupportFunctions sup = new SupportFunctions();
	
	System.out.println(sup.getFPDepartureMonths());
}
}
