package com.rezrobot.verifications.HotelSetup;

import java.util.ArrayList;
import java.util.HashMap;

import com.rezrobot.flight_circuitry.UIFlightItinerary;
import com.rezrobot.pages.web.common.Web_Com_PaymentsPage;
import com.rezrobot.utill.ReportTemplate;

public class Verify_Flight_PaymentPage {
	
	public static void generalPaymentPageValidaiton(ReportTemplate PrintTemplate, Web_Com_PaymentsPage paymentPage)
	{
		//PrintTemplate.setTableHeading("General Results Page Validations");		
		PrintTemplate.verifyTrue(true, paymentPage.isPaymentsPageAvailable(),"Payment Page Availability");
		//PrintTemplate.markTableEnd();
	}

	public static void paymentPageBasketAvailability(ReportTemplate PrintTemplate, Web_Com_PaymentsPage paymentPage)
	{
		//PrintTemplate.setTableHeading("Payment Page Basket Availabiltiy Validations");		
		PrintTemplate.verifyTrue(true, paymentPage.isMyBasketLoaded(),"Payment Page Availability");
		PrintTemplate.verifyTrue(true, paymentPage.isMyBasketBillingInfoSectionLoaded(),"Payment Page Availability");
		PrintTemplate.verifyTrue(true, paymentPage.isPaymentsPageAvailable(),"Payment Page Availability");
		//PrintTemplate.markTableEnd();
	}
	
	
	public static void commonLabelValidation(ReportTemplate PrintTemplate, HashMap<String, String> actualCommonLabels, HashMap<String, String> flightLabels)
	{
		
		//PrintTemplate.setTableHeading("PAYMENT PAGE COMMON LABEL TEXT VALIDATIONS");
		PrintTemplate.verifyContains(flightLabels.get(""), actualCommonLabels.get("L_AddHotels"), "Outbound Duration Label in Itinerary");
		PrintTemplate.verifyContains(flightLabels.get(""), actualCommonLabels.get("L_AddFlights"), "Outbound No. of Stops Label in Itinerary");
		PrintTemplate.verifyContains(flightLabels.get(""), actualCommonLabels.get("L_AddActivities"), "Outbound Layover Label in Itinerary");
		PrintTemplate.verifyContains(flightLabels.get(""), actualCommonLabels.get("L_AddCars"), "Inbound Duration Label in Itinerary");
		PrintTemplate.verifyContains(flightLabels.get(""), actualCommonLabels.get("L_AddTransfers"), "Inbound No. of Stops Label in Itinerary");
		PrintTemplate.verifyContains(flightLabels.get(""), actualCommonLabels.get("L_MyBasket"), "Inbound Layover Label in Itinerary");
		PrintTemplate.verifyContains(flightLabels.get(""), actualCommonLabels.get("L_BillingInformation"), "Itinerary More Details Table Header Label");
		PrintTemplate.verifyContains(flightLabels.get(""), actualCommonLabels.get("L_mybasketSubTotal_BillingInfo"), "Itinerary More Details Table Header Label");
		PrintTemplate.verifyContains(flightLabels.get(""), actualCommonLabels.get("L_mybasketTotalText"), "Itinerary More Details Outbound Non Stop Label");
		PrintTemplate.verifyContains(flightLabels.get(""), actualCommonLabels.get("L_mybasketTotal"), "Itinerary More Details Outbound Cabin Class Label");
		PrintTemplate.verifyContains(flightLabels.get(""), actualCommonLabels.get("L_mybasketAmountBeingProcessed"), "Itinerary More Details Outbound Cabin Class Label");
		PrintTemplate.verifyContains(flightLabels.get(""), actualCommonLabels.get("L_mybasketAmountDue"), "Itinerary More Details Outbound Cabin Class Label");
		//PrintTemplate.markTableEnd();
		
	}
	
	
	
}
