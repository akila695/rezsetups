package com.rezrobot.verifications;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;

import org.junit.Assert;

import com.rezrobot.dataobjects.ActivityDetails;
import com.rezrobot.dataobjects.Activity_WEB_ActivityInfo;
import com.rezrobot.dataobjects.Activity_WEB_PaymentDetails;
import com.rezrobot.dataobjects.Activity_WEB_Search;
import com.rezrobot.enumtypes.WEB_TourOperatorType;
import com.rezrobot.pages.CC.common.CC_Com_BookingListReport_BookingCard;
import com.rezrobot.pages.CC.common.CC_Com_BookingListReport_criteria;
import com.rezrobot.pages.CC.common.CC_Com_BookingList_Report;
import com.rezrobot.pages.web.activity.Web_Act_ConfirmationPage;
import com.rezrobot.pages.web.activity.Web_Act_QuotationConfirmationPage;
import com.rezrobot.pages.web.activity.Web_Act_ResultsPage;
import com.rezrobot.pages.web.common.Web_Com_BillingInformationPage;
import com.rezrobot.pages.web.common.Web_Com_CustomerDetailsPage;
import com.rezrobot.pages.web.common.Web_Com_HomePage;
import com.rezrobot.pages.web.common.Web_Com_NotesPage;
import com.rezrobot.pages.web.common.Web_Com_OccupancyDetailsPage;
import com.rezrobot.pages.web.common.Web_Com_PaymentsPage;
import com.rezrobot.pages.web.common.Web_Com_TermsPage;
import com.rezrobot.pojo.ComboBox;
import com.rezrobot.processor.LabelReadProperties;
import com.rezrobot.utill.Calender;
import com.rezrobot.utill.ReportTemplate;
import com.rezrobot.utill.CurrencyConverter;

public class Activity_WEB_Validations {
	
	private ReportTemplate PrintTemplate;
	private Activity_WEB_Search searchObject;
	private ActivityDetails activityDetails;
	private HashMap<String, String> labelPropertyMap;
	private LabelReadProperties labelProperty;
	private int paxCnt, discountedValue;
	private double sellRate, profitMarkup, netRate;
	private int subTotal, activityRate, toAgentCommission;
	private WEB_TourOperatorType UserTypeDC_B2B;
	private HashMap<String, String> currencyMap;
	
	public Activity_WEB_Validations(ReportTemplate pTemplate, Activity_WEB_Search sObject, HashMap<String, String> labelPropMap, LabelReadProperties labelProp, HashMap<String, String> currencyMp){
		this.PrintTemplate = pTemplate;
		this.searchObject = sObject;
		this.labelPropertyMap = labelPropMap;
		this.labelProperty = labelProp;
		this.currencyMap=currencyMp;		
	}
	
	public ActivityDetails getRatesCalculations(ActivityDetails activityDetails){
		
		try {			
			paxCnt = Integer.parseInt(searchObject.getAdults()) + Integer.parseInt(searchObject.getChildren());	
			CurrencyConverter currencyConvert = new CurrencyConverter();
			
			ArrayList<Activity_WEB_ActivityInfo> activityInfoList = searchObject.getInventoryInfo();
			
			for (Activity_WEB_ActivityInfo actDetails : activityInfoList) {
				
				UserTypeDC_B2B = actDetails.getUserTypeDC_B2B();
				
				if (UserTypeDC_B2B == WEB_TourOperatorType.WEB_DC) {
					profitMarkup = Double.parseDouble(actDetails.getProfir_Markup());
				}else{
					profitMarkup = Double.parseDouble(actDetails.getToActivityPM());
				}
				
				netRate = Double.parseDouble(actDetails.getActivityNetRate());
				
				activityRate = (int) Math.ceil(netRate* ((100 + profitMarkup) / 100));
				sellRate = activityRate * paxCnt;	
				double totalNetRate = (netRate * paxCnt);
							
				double totalNetRate_SalesTax = 0;
				double totalNetRate_MisFees = 0;
				
				if (actDetails.getActivity_SalesTaxType().equalsIgnoreCase("percentage")) {					
					double salesTax = Double.parseDouble(actDetails.getActivity_SalesTaxValue());
					totalNetRate_SalesTax = (totalNetRate* ((salesTax) / 100));					
				}else{
					double salesTax = Double.parseDouble(actDetails.getActivity_SalesTaxValue());
					totalNetRate_SalesTax = salesTax;
				}
				
				if (actDetails.getActivity_MisFeeType().equalsIgnoreCase("percentage")) {					
					double misFees = Double.parseDouble(actDetails.getActivity_MisFeeValue());
					totalNetRate_MisFees = (totalNetRate* ((misFees) / 100));				
				}else{
					double misFees = Double.parseDouble(actDetails.getActivity_MisFeeValue());
					totalNetRate_MisFees = misFees;
				}
								
				subTotal = (int) sellRate;
				int totalTax = (int) Math.ceil(totalNetRate_SalesTax + totalNetRate_MisFees);
				int creditCardFee = 0;
				
				if (actDetails.getPaymentType().equalsIgnoreCase("Pay Online")) {
					creditCardFee = Integer.parseInt(actDetails.getCreditCardFee());
				} 
				
				activityRate = Integer.parseInt(currencyConvert.convert(labelProperty.getPortalSpecificData().get("portal.Currency"), Integer.toString(activityRate), searchObject.getSellingCurrency(), currencyMap, actDetails.getSupplier_Currency()));
				subTotal = Integer.parseInt(currencyConvert.convert(labelProperty.getPortalSpecificData().get("portal.Currency"), Integer.toString(subTotal), searchObject.getSellingCurrency(), currencyMap, actDetails.getSupplier_Currency()));
				totalTax = Integer.parseInt(currencyConvert.convert(labelProperty.getPortalSpecificData().get("portal.Currency"), Integer.toString(totalTax), searchObject.getSellingCurrency(), currencyMap, actDetails.getSupplier_Currency()));
				creditCardFee = Integer.parseInt(currencyConvert.convert(labelProperty.getPortalSpecificData().get("portal.Currency"), Integer.toString(creditCardFee), searchObject.getSellingCurrency(), currencyMap, labelProperty.getPortalSpecificData().get("portal.pgCurrency")));
					
				int totalBookingValueWToutCCFee = subTotal + totalTax;
				int totalBookingValue = subTotal + totalTax + creditCardFee;
				int disValue = 0;
				int amountDue = 0;
				discountedValue = 0;
				toAgentCommission = 0;
				
				if (UserTypeDC_B2B == WEB_TourOperatorType.WEB_COMCASH || UserTypeDC_B2B == WEB_TourOperatorType.WEB_COMCREDITLPONO || UserTypeDC_B2B == WEB_TourOperatorType.WEB_COMCREDITLPOY) {
					
					String toAgentTypePerOrValue = labelProperty.getPortalSpecificData().get(UserTypeDC_B2B.toString() + "#Type");
					
					if (toAgentTypePerOrValue.equalsIgnoreCase("percentage")) {						
						double agentPercentage = Double.parseDouble(labelProperty.getPortalSpecificData().get(UserTypeDC_B2B.toString() + "#Value"));
						toAgentCommission = (int)(subTotal* ((agentPercentage) / 100));	
					} else {						
						double agentCommValue = Double.parseDouble(labelProperty.getPortalSpecificData().get(UserTypeDC_B2B.toString() + "#Value"));
						toAgentCommission = (int)(agentCommValue) ;	
					}
				}
				
				if (actDetails.getDiscountYesORNo().equalsIgnoreCase("Yes")) {
					
					if (labelProperty.getPortalSpecificData().get("Discount_Type").equalsIgnoreCase("percentage")) {						
						disValue = Integer.parseInt(labelProperty.getPortalSpecificData().get("Discount_Value"));
						discountedValue = ((subTotal * disValue)/100);							
					}else{
						disValue = Integer.parseInt(labelProperty.getPortalSpecificData().get("Discount_Value"));
						disValue = Integer.parseInt(currencyConvert.convert(labelProperty.getPortalSpecificData().get("portal.Currency"), Integer.toString(disValue), searchObject.getSellingCurrency(), currencyMap, labelProperty.getPortalSpecificData().get("portal.Currency")));							
						discountedValue = disValue;					
					}		
				}
				
				activityDetails.setActivityRate(activityRate);
				activityDetails.setSubTotalValue(subTotal);
				activityDetails.setTax(totalTax);
				activityDetails.setCreditCardFee(creditCardFee);
				activityDetails.setTotalBookingValue(totalBookingValue);
				activityDetails.setTotalWToutCCFee(totalBookingValueWToutCCFee);
				activityDetails.setAmountProcessedNow(totalBookingValue - toAgentCommission);
				activityDetails.setAmountDue(amountDue);
				activityDetails.setDiscountedValue(discountedValue);
				
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return activityDetails;
			
	}
	
	
	public ActivityDetails getCancellationPolicies(ActivityDetails activityDetails){
		
		try {
			
			CurrencyConverter currencyConvert = new CurrencyConverter();
			DateFormat dateFormatcurrentDate = new SimpleDateFormat("dd-MMM-yyyy");
			
			ArrayList<Activity_WEB_ActivityInfo> activityInfoList = searchObject.getInventoryInfo();
			
			for (Activity_WEB_ActivityInfo actDetails : activityInfoList) {
			
				int bufferDates = Integer.parseInt(actDetails.getApplyIfLessThan()) + Integer.parseInt(actDetails.getCancelBuffer());
				
				double pMarkup = 0;
				
				if (UserTypeDC_B2B == WEB_TourOperatorType.WEB_DC) {
					pMarkup = Double.parseDouble(actDetails.getProfir_Markup());
				}else{
					pMarkup = Double.parseDouble(actDetails.getToActivityPM());
				}
				
				String reservation_Date = searchObject.getActivityDate();
				
				Date instanceMainPol = dateFormatcurrentDate.parse(reservation_Date); 
				Calendar calMainPolicy = Calendar.getInstance();
				calMainPolicy.setTime(instanceMainPol);
				calMainPolicy.add(Calendar.DATE, -1);			
				String beforeReservation_date_4 = dateFormatcurrentDate.format(calMainPolicy.getTime());
				
				Date instanceMainPolicy5 = dateFormatcurrentDate.parse(reservation_Date);
				Calendar policy1 = Calendar.getInstance();
				policy1.setTime(instanceMainPolicy5);
				policy1.add(Calendar.DATE, -(bufferDates+1));
				String cancellationDeadline_date_2 = dateFormatcurrentDate.format(policy1.getTime());
				activityDetails.setCancellationDeadline(cancellationDeadline_date_2);
				
				Date instanceMainPolicy6 = dateFormatcurrentDate.parse(cancellationDeadline_date_2);
				Calendar policy12 = Calendar.getInstance();
				policy12.setTime(instanceMainPolicy6);
				policy12.add(Calendar.DATE, +1);
				String beforeReservation_date_3 = dateFormatcurrentDate.format(policy12.getTime());
				
				double StandardCancellation_value = 0;
				double Noshow_value = 0;
				
				if (actDetails.getStandardCancell_Type().equalsIgnoreCase("Percentage")) {
					StandardCancellation_value = (Double.parseDouble(actDetails.getStandardCancell_Value()));
				}
				
				if (actDetails.getStandardCancell_Type().equalsIgnoreCase("value")) {
					StandardCancellation_value = Math.ceil(Double.parseDouble(actDetails.getStandardCancell_Value()) + (Double.parseDouble(actDetails.getStandardCancell_Value()) * ((double)pMarkup/100)));
					StandardCancellation_value = Double.parseDouble(currencyConvert.convert(labelProperty.getPortalSpecificData().get("portal.Currency"), Double.toString(StandardCancellation_value), searchObject.getSellingCurrency(), currencyMap, actDetails.getSupplier_Currency()));					
				}
				
				if (actDetails.getNoShow_Type().equalsIgnoreCase("Percentage")) {
					Noshow_value = Math.ceil((subTotal) * ((double)pMarkup/100));
				}
				
				if (actDetails.getNoShow_Type().equalsIgnoreCase("value")) {
					Noshow_value = Math.ceil(Double.parseDouble(actDetails.getNoShow_Value()) + (Double.parseDouble(actDetails.getNoShow_Value()) * ((double)pMarkup/100)));
					Noshow_value = Double.parseDouble(currencyConvert.convert(labelProperty.getPortalSpecificData().get("portal.Currency"), Double.toString(Noshow_value), searchObject.getSellingCurrency(), currencyMap, actDetails.getSupplier_Currency()));					
				}
				
				String selling_Currency = searchObject.getSellingCurrency();
				
				String activityPolicy_1;
				String activityPolicy_2;
				String noShowPolicy;
				
				activityPolicy_1 = "If you cancel between "+activityDetails.getReservationDateToday()+" and "+cancellationDeadline_date_2+" you will be refunded your purchase price.";
				
				if (actDetails.getStandardCancell_Type().equalsIgnoreCase("Percentage")) {
					activityPolicy_2 = "If you cancel between "+beforeReservation_date_3+" and "+beforeReservation_date_4+" you will be charged "+(int)StandardCancellation_value+" % of the purchase price.";
				}else{
					activityPolicy_2 = "If you cancel between "+beforeReservation_date_3+" and "+beforeReservation_date_4+" you will be charged "+selling_Currency+" "+StandardCancellation_value+" .";
				}
				
				noShowPolicy = "If cancelled on or after the "+reservation_Date+" No Show Fee "+selling_Currency+" "+Noshow_value+" applies.";
				
				
				ArrayList<String> cancellationPolicy = new ArrayList<String>();
				cancellationPolicy.add(activityPolicy_1);
				cancellationPolicy.add(activityPolicy_2);
				cancellationPolicy.add(noShowPolicy);
				
				activityDetails.setCancellationPoliciesList(cancellationPolicy);
				
			}
						
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return activityDetails;
	}
	
	
	public void getActivityBookingEngineValidations(Web_Com_HomePage acthomePage) throws Exception{
		
		try {
			//PrintTemplate.setTableHeading("Booking Engine Validations");
			PrintTemplate.verifyTrue(true, acthomePage.getBECAvailability(), "Default Booking Engine Availability - Checking Flight Booking Engine");			
			PrintTemplate.verifyTrue(true, acthomePage.isActivityBECLoaded(), "Activity Booking Engine Availability");
			
			HashMap<String, Boolean> availMap = acthomePage.getActivityAvailabilityOfInputFields();
			
			PrintTemplate.verifyTrue(true, availMap.get("CountryOfResidence"), "Availability Of CountryOfResidence field");			
			PrintTemplate.verifyTrue(true, availMap.get("DestinationCity"), "Availability Of City field");
			PrintTemplate.verifyTrue(true, availMap.get("DateFrom"), "Availability Of DateFrom field");
			PrintTemplate.verifyTrue(true, availMap.get("DateTo"), "Availability Of DateTo field");
			PrintTemplate.verifyTrue(true, availMap.get("Adults"), "Availability Of Adults DropDown");
			PrintTemplate.verifyTrue(true, availMap.get("Children"), "Availability Of Children DropDown");
			PrintTemplate.verifyTrue(true, availMap.get("ChildrenAge"), "Availability Of Child Age DropDown");
			PrintTemplate.verifyTrue(true, availMap.get("ActivityType"), "Availability Of ActivityType field");
			PrintTemplate.verifyTrue(true, availMap.get("ProgramCategory"), "Availability Of Program Category DropDown");
			PrintTemplate.verifyTrue(true, availMap.get("PreferCurrency"), "Availability Of Prefer Currency DropDown");
			PrintTemplate.verifyTrue(true, availMap.get("DiscountCoupon"), "Availability Of Discount Coupon field");		
			//PrintTemplate.markTableEnd();
							
			// Label Text validations should go here
			
			HashMap<String, String> labelMap = acthomePage.getActivityBECLabels();
			
			//PrintTemplate.setTableHeading("Booking Engine - Label Text Validations");			 			
			PrintTemplate.verifyTrue(labelPropertyMap.get("Bec_CountryofResidence"), labelMap.get("COR") , "Country of Residence label validation");
			PrintTemplate.verifyTrue(labelPropertyMap.get("Bec_WhereAreYouGoing"), labelMap.get("Where") , "Where Are You Going label validation");
			PrintTemplate.verifyTrue(labelPropertyMap.get("Bec_WhenAreYouGoing"), labelMap.get("When") , "When Are You Going label validation");
			PrintTemplate.verifyTrue(labelPropertyMap.get("Bec_HowManyPeople"), labelMap.get("People") , "How Many People label validation");
			PrintTemplate.verifyTrue(labelPropertyMap.get("Bec_ShowAdditionalSearchOptions"), labelMap.get("ShowAdd") , "Show Additional Search Options label validation");
			PrintTemplate.verifyTrue(labelPropertyMap.get("Bec_HideAdditionalSearchOptions"), labelMap.get("HideAdd") , "Hide Additional Search Options label validation");
			//PrintTemplate.markTableEnd();
								
			// Combobox validations should go here

			HashMap<String, String> comboMap = acthomePage.getActivityComboBoxDataList();
			
			//PrintTemplate.setTableHeading("Booking Engine - Combo Box Attributes Validation");
			PrintTemplate.verifyTrue("Albania", comboMap.get("CountryListLoadedCheck") , "Country List Loaded Check validation");
			PrintTemplate.verifyTrue("Theme Parks", comboMap.get("ProgramCategoryCheck") , "Program Type Loaded Check validation");
			PrintTemplate.verifyTrue("AED", comboMap.get("prefercurencyLoadedCheck") , "Prefer Currency Loaded Check validation");	
			PrintTemplate.verifyTrue(labelProperty.getPortalSpecificData().get("Activity_Program_Category"), comboMap.get("ActivityProgramCatList") , "Program Type list match");
			//PrintTemplate.markTableEnd();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	
	
	public void getResultsPageValidations(Web_Act_ResultsPage actResultsPage) throws Exception{
		
		try {			
			HashMap<String, Boolean> availableResultsMap = actResultsPage.getAvailabilityFieldsForActivityResultsPage();
			
			PrintTemplate.verifyTrue(true, availableResultsMap.get("AddHotels"), "Availability Of Add Hotel link");
			PrintTemplate.verifyTrue(true, availableResultsMap.get("AddFlights"), "Availability Of Add Flights link");
			PrintTemplate.verifyTrue(true, availableResultsMap.get("AddActivities"), "Availability Of Add Activities link");
			PrintTemplate.verifyTrue(true, availableResultsMap.get("AddCars"), "Availability Of Add AddCars link");
			PrintTemplate.verifyTrue(true, availableResultsMap.get("AddTransfers"), "Availability Of Add Transfers link");						
			PrintTemplate.verifyTrue(true, availableResultsMap.get("destinationCity"), "Availability Of Destination City field");
			PrintTemplate.verifyTrue(true, availableResultsMap.get("DateRange"), "Availability Of Date Range field");
			PrintTemplate.verifyTrue(true, availableResultsMap.get("AdultChildCount"), "Availability Of Adult Child Count field");
			PrintTemplate.verifyTrue(true, availableResultsMap.get("ShowAdditionFilter"), "Availability Of Show Additional Filter link");
			PrintTemplate.verifyTrue(true, availableResultsMap.get("ProgramCategory"), "Availability Of ProgramCategory Dropdown");
			PrintTemplate.verifyTrue(true, availableResultsMap.get("PreferCurrency"), "Availability Of Prefer Currency Dropdown");
			PrintTemplate.verifyTrue(true, availableResultsMap.get("DiscountCoupon"), "Availability Of Discount Coupon field");
			PrintTemplate.verifyTrue(true, availableResultsMap.get("HideAdditionFilter"), "Availability Of Hide Additional Filter link");
			PrintTemplate.verifyTrue(true, availableResultsMap.get("SearchAgainButton"), "Availability Of Search Again Button");			
			PrintTemplate.verifyTrue(true, availableResultsMap.get("ActivitiesAvailable"), "Availability Of Activities Available label");
			PrintTemplate.verifyTrue(true, availableResultsMap.get("PriceLabel"), "Availability Of Price Label");
			PrintTemplate.verifyTrue(true, availableResultsMap.get("SearchActivity"), "Availability Of Activity Search Filter field");
			PrintTemplate.verifyTrue(true, availableResultsMap.get("ResetSortButton"), "Availability Of Reset Sort Button");
			PrintTemplate.verifyTrue(true, availableResultsMap.get("PriceRangeLabel"), "Availability Of Price Range Label");
			PrintTemplate.verifyTrue(true, availableResultsMap.get("PriceRangeText"), "Availability Of Price Range Text label");
			PrintTemplate.verifyTrue(true, availableResultsMap.get("PriceSlider"), "Availability Of Price Slider");
			PrintTemplate.verifyTrue(true, availableResultsMap.get("ProgramAvailability"), "Availability Of Program Availability label");
			PrintTemplate.verifyTrue(true, availableResultsMap.get("ProgramAvailabilityDropD"), "Availability Of Program Availability dropdown");
			PrintTemplate.verifyTrue(true, availableResultsMap.get("ProgramType"), "Availability Of Program Type label");
			PrintTemplate.verifyTrue(true, availableResultsMap.get("ResetFiltersButton"), "Availability Of Reset Filter Button");					
			//PrintTemplate.markTableEnd();
			
			HashMap<String, String> resultsPageLabelMap = actResultsPage.getActivityResultsPageLabels();
			
			//PrintTemplate.setTableHeading("Results Page - Label Text Validations");			 			
			PrintTemplate.verifyTrue(labelPropertyMap.get("Results_AddHotels"), resultsPageLabelMap.get("AddHotels") , "Add Hotels label validation");
			PrintTemplate.verifyTrue(labelPropertyMap.get("Results_AddFlights"), resultsPageLabelMap.get("AddFlights") , "Add Flights label validation");
			PrintTemplate.verifyTrue(labelPropertyMap.get("Results_AddActivities"), resultsPageLabelMap.get("AddActivities") , "Add Activities label validation");
			PrintTemplate.verifyTrue(labelPropertyMap.get("Results_AddCars"), resultsPageLabelMap.get("AddCars") , "Add Cars label validation");
			PrintTemplate.verifyTrue(labelPropertyMap.get("Results_AddTransfers"), resultsPageLabelMap.get("AddTransfers") , "Add Transfers label validation");					
			PrintTemplate.verifyTrue(labelPropertyMap.get("Results_ShowAdditionalSearchOptions"), resultsPageLabelMap.get("ShowAdditionFilter") , "Show Addition Filter label validation");
			PrintTemplate.verifyTrue(labelPropertyMap.get("Results_ProgramCategory"), resultsPageLabelMap.get("ProgramCategory") , "Program Category label validation");
			PrintTemplate.verifyTrue(labelPropertyMap.get("Results_PreferCurrency"), resultsPageLabelMap.get("PreferCurrency") , "Prefer Currency label validation");
			PrintTemplate.verifyTrue(labelPropertyMap.get("Results_DiscountCoupon"), resultsPageLabelMap.get("DiscountCoupon") , "DiscountCoupon label validation");
			PrintTemplate.verifyTrue(labelPropertyMap.get("Results_HideAdditionalSearchOptions"), resultsPageLabelMap.get("HideAdditionFilter") , "Hide Addition Filter label validation");
			PrintTemplate.verifyContains(labelPropertyMap.get("Results_ActivitiesAvailable"), resultsPageLabelMap.get("ActivitiesAvailable") , "Activities Available label validation");
			PrintTemplate.verifyTrue(labelPropertyMap.get("Results_PriceLabel"), resultsPageLabelMap.get("PriceLabel") , "Price label validation");
			PrintTemplate.verifyTrue(labelPropertyMap.get("Results_PriceRangeLabel"), resultsPageLabelMap.get("PriceRangeLabel") , "Price Range label validation");
			PrintTemplate.verifyTrue(labelPropertyMap.get("Results_ProgramAvailability"), resultsPageLabelMap.get("ProgramAvailability") , "Program Availability label validation");
			PrintTemplate.verifyTrue(labelPropertyMap.get("Results_ProgramType"), resultsPageLabelMap.get("ProgramType") , "Program Type label validation");					
			//PrintTemplate.markTableEnd();
			
			HashMap<String, String> resultsPageComboListMap = actResultsPage.getActivityResultsComboBoxDataList();
			
			//PrintTemplate.setTableHeading("Results Page - Combo Box Attributes Validation");
			PrintTemplate.verifyTrue("Theme Parks", resultsPageComboListMap.get("ProgramCategoryCheck") , "Program Type's Loaded Check validation");
			PrintTemplate.verifyTrue(labelProperty.getPortalSpecificData().get("Activity_Program_Category"), resultsPageComboListMap.get("ActivityProgramCatList") , "Program Type list Loaded Check validation");
			PrintTemplate.verifyTrue("AED", resultsPageComboListMap.get("prefercurencyLoadedCheck") , "Prefer Currencies Loaded Check validation");						
			PrintTemplate.verifyTrue("ALL", resultsPageComboListMap.get("programTypeDefault") , "Program Type Default selection");			
			PrintTemplate.verifyTrue("On Request", resultsPageComboListMap.get("programType_Onreq") , "Program Type OnRequest option");			
			PrintTemplate.verifyTrue("Available", resultsPageComboListMap.get("programType_Available") , "Program Type Available option");								
			//PrintTemplate.markTableEnd();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
			
	}
	
	
	public void getPaymentsPageValidations(Web_Com_PaymentsPage actPaymentsPage){
		
		try {		
			HashMap<String, String> paymentCommonLabelMap = actPaymentsPage.getPaymentsPageCommonLabels();	
			HashMap<String, Boolean> paymentButtonMap = actPaymentsPage.getActivity_AvailabileButtons();
			HashMap<String, String> paymentLabelMap = actPaymentsPage.getActivity_MoreDetailsSetionLabels();
									
			PrintTemplate.verifyTrue(true, actPaymentsPage.isMyBasketLoaded(), "Availability Of My Basket");
			
			if (actPaymentsPage.isMyBasketLoaded()) {
				
				PrintTemplate.verifyTrue(true, paymentButtonMap.get("IN_button_my_basket"), "Availability Of Activity more details section expand button");							
				PrintTemplate.verifyTrue(true, actPaymentsPage.isMyBasketActivityMoreDetailsSectionLoaded(), "Availability Of Activity More Details Section");
				
				if (actPaymentsPage.isMyBasketActivityMoreDetailsSectionLoaded()) {
					PrintTemplate.verifyTrue(true, paymentButtonMap.get("IN_button_my_basket_Close"), "Availability Of Activity More Details Close Button");	
				}							
			}
			
			PrintTemplate.verifyTrue(true, actPaymentsPage.isMyBasketBillingInfoSectionLoaded(), "Availability Of My Basket - Billing Informations");
			//PrintTemplate.markTableEnd();
			
			//Payments Page labels
			
			//PrintTemplate.setTableHeading("Payments Page - Label Text Validations");					
			PrintTemplate.verifyTrue(labelPropertyMap.get("Payments_AddHotels"), paymentCommonLabelMap.get("L_AddHotels") , "Add Hotels label validation");
			PrintTemplate.verifyTrue(labelPropertyMap.get("Payments_AddFlights"), paymentCommonLabelMap.get("L_AddFlights") , "Add Flights label validation");
			PrintTemplate.verifyTrue(labelPropertyMap.get("Payments_AddActivities"), paymentCommonLabelMap.get("L_AddActivities") , "Add Activities label validation");
			PrintTemplate.verifyTrue(labelPropertyMap.get("Payments_AddCars"), paymentCommonLabelMap.get("L_AddCars") , "Add Cars label validation");
			PrintTemplate.verifyTrue(labelPropertyMap.get("Payments_AddTransfers"), paymentCommonLabelMap.get("L_AddTransfers") , "Add Transfers label validation");					
														
			if (actPaymentsPage.isMyBasketLoaded()) {
				
				PrintTemplate.verifyTrue(labelPropertyMap.get("Payments_MyBasket"), paymentCommonLabelMap.get("L_MyBasket") , "My Basket label validation");					
			
				if (actPaymentsPage.isMyBasketActivityMoreDetailsSectionLoaded()) {
				
					PrintTemplate.verifyTrue(labelPropertyMap.get("Payments_MBM_usable_on"), paymentLabelMap.get("L_MD_usable_on") , "Usable On label validation");					
					PrintTemplate.verifyTrue(labelPropertyMap.get("Payments_MBM_period_session"), paymentLabelMap.get("L_MD_period_session") , "Period / Session label validation");					
					PrintTemplate.verifyTrue(labelPropertyMap.get("Payments_MBM_rate_type"), paymentLabelMap.get("L_MD_rate_type") , "Rate Type label validation");					
					PrintTemplate.verifyTrue(labelPropertyMap.get("Payments_MBM_rate"), paymentLabelMap.get("L_MD_rate") , "Rate label validation");					
					PrintTemplate.verifyTrue(labelPropertyMap.get("Payments_MBM_qty"), paymentLabelMap.get("L_MD_qty") , "Qty label validation");					
					PrintTemplate.verifyTrue(labelPropertyMap.get("Payments_MBM_total"), paymentLabelMap.get("L_MD_total") , "Total label validation");					
					PrintTemplate.verifyTrue(labelPropertyMap.get("Payments_MBM_subtotal"), paymentLabelMap.get("L_MD_subtotal") , "Sub Total label validation");					
					PrintTemplate.verifyTrue(labelPropertyMap.get("Payments_MBM_tax_and_charges"), paymentLabelMap.get("L_MD_tax_and_charges") , "Tax & Charges label validation");					
					PrintTemplate.verifyTrue(labelPropertyMap.get("Payments_MBM_totalVal"), paymentLabelMap.get("L_MD_total") , "Total label validation");					
					PrintTemplate.verifyContains(labelPropertyMap.get("Payments_MBM_status"), paymentLabelMap.get("L_MD_status") , "Status label validation");					
					PrintTemplate.verifyContains(labelPropertyMap.get("Payments_MBM_cancellation"), paymentLabelMap.get("L_MD_cancellation_deadline") , "Cancellation Deadline label validation");					
					PrintTemplate.verifyTrue(labelPropertyMap.get("Payments_MBM_PolicyIndicate"), paymentLabelMap.get("L_MD_cancellation_policy") , "cancellation Indication label validation");														
				}							
			}
			
			if (actPaymentsPage.isMyBasketBillingInfoSectionLoaded()) {
				
				PrintTemplate.verifyTrue(labelPropertyMap.get("Payments_BillingInformation"), paymentCommonLabelMap.get("L_BillingInformation") , "Billing Information label validation");					
				PrintTemplate.verifyTrue(labelPropertyMap.get("Payments_MB_SubTotal"), paymentCommonLabelMap.get("L_mybasketSubTotal_BillingInfo") , "Sub Total label validation");					
				PrintTemplate.verifyTrue(labelPropertyMap.get("Payments_MB_TotalTax"), paymentCommonLabelMap.get("L_mybasketTotalTaxt") , "Total Taxes and Other Charges label validation");					
				PrintTemplate.verifyTrue(labelPropertyMap.get("Payments_MB_Total"), paymentCommonLabelMap.get("L_mybasketTotal") , "Total label validation");					
				PrintTemplate.verifyTrue(labelPropertyMap.get("Payments_MB_AmountBeingProcessed"), paymentCommonLabelMap.get("L_mybasketAmountBeingProcessed") , "Amount being Processed Now label validation");					
				PrintTemplate.verifyTrue(labelPropertyMap.get("Payments_MB_AmountDue"), paymentCommonLabelMap.get("L_mybasketAmountDue") , "Amount Due at Check-in / Utilization label validation");					
			}
			
			//PrintTemplate.markTableEnd();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	
	
	public void getCustomerDetailsValidations(Web_Com_CustomerDetailsPage customerPage){
		
		try {
			HashMap<String, Boolean> customerInputMap = customerPage.getCustomerDetailsInputFields();
			HashMap<String, String> customerLabelMap = customerPage.getCustomerDetailsPageLabels();
			HashMap<String, Boolean> customerMandatoryMap = customerPage.getCustomerDetailsMandatoryFields();
			
			PrintTemplate.verifyTrue(true, customerInputMap.get("IN_customertitle"), "Availability Of Customer title dropdown - Customer Details");
			PrintTemplate.verifyTrue(true, customerInputMap.get("IN_customerFirst"), "Availability Of Customer First Name field - Customer Details");
			PrintTemplate.verifyTrue(true, customerInputMap.get("IN_customer_last"), "Availability Of Customer Last name field - Customer Details");
			PrintTemplate.verifyTrue(true, customerInputMap.get("IN_customer_address"), "Availability Of Customer address field - Customer Details");
			PrintTemplate.verifyTrue(true, customerInputMap.get("IN_customer_city"), "Availability Of Customer city field - Customer Details");
			PrintTemplate.verifyTrue(true, customerInputMap.get("IN_customer_country"), "Availability Of Customer country field - Customer Details");						
			
			ArrayList<Activity_WEB_PaymentDetails> paymentInfoList = searchObject.getPaymentDetailsInfo();
						
			for (Activity_WEB_PaymentDetails paymentDetails : paymentInfoList) {
				
				if (paymentDetails.getCountry().equalsIgnoreCase("USA") || paymentDetails.getCountry().equalsIgnoreCase("Canada") || paymentDetails.getCountry().equalsIgnoreCase("Australia")) {
					PrintTemplate.verifyTrue(true, customerInputMap.get("IN_customer_State"), "Availability Of Customer State field - Customer Details");																								
				}								
			}
			
			PrintTemplate.verifyTrue(true, customerInputMap.get("IN_customer_PostalCode"), "Availability Of Customer PostalCode field - Customer Details");
			PrintTemplate.verifyTrue(true, customerInputMap.get("IN_Phonenumber_Areacode"), "Availability Of Customer PhoneNumber_Areacode field - Customer Details");
			PrintTemplate.verifyTrue(true, customerInputMap.get("IN_Phone_number"), "Availability Of Customer PhoneNumber field - Customer Details");
			PrintTemplate.verifyTrue(true, customerInputMap.get("IN_MobileNumber_Areacode"), "Availability Of Customer MobileNumber_Areacode field - Customer Details");
			PrintTemplate.verifyTrue(true, customerInputMap.get("IN_customer_MobileNumber"), "Availability Of Customer MobileNumber field - Customer Details");
			PrintTemplate.verifyTrue(true, customerInputMap.get("IN_customer_Email"), "Availability Of Customer Email field - Customer Details");
			PrintTemplate.verifyTrue(true, customerInputMap.get("IN_customer_VerifyEmail"), "Availability Of Customer VerifyEmail field - Customer Details");
			PrintTemplate.verifyTrue(true, customerInputMap.get("IN_customer_phone_number"), "Availability Of Customer PhoneNumber radioButton - Customer Details");
			PrintTemplate.verifyTrue(true, customerInputMap.get("IN_customer_mobile_number"), "Availability Of Customer MobileNumber radioButton - Customer Details");												
			PrintTemplate.verifyTrue(true, customerInputMap.get("IN_ProceedToOccupancy"), "Availability Of Proceed To Occupancy button - Customer Details");
			PrintTemplate.verifyTrue(true, customerInputMap.get("IN_Savequotation"), "Availability Of Save quotation button - Customer Details");
			//PrintTemplate.markTableEnd();
										
			//PrintTemplate.setTableHeading("Customer Details Page - Label Text Validations");
			PrintTemplate.verifyTrue(labelPropertyMap.get("Payments_CD_customertitle"), customerLabelMap.get("L_customertitle") , "Customer Title label validation");					
			PrintTemplate.verifyTrue(labelPropertyMap.get("Payments_CD_customerfirstname"), customerLabelMap.get("L_customerfirstname") , "Customer Firstname label validation");					
			PrintTemplate.verifyTrue(labelPropertyMap.get("Payments_CD_customerlastname"), customerLabelMap.get("L_customerlastname") , "Customer Lastname label validation");					
			PrintTemplate.verifyTrue(labelPropertyMap.get("Payments_CD_customeraddress"), customerLabelMap.get("L_customeraddress") , "Customer Address label validation");					
			PrintTemplate.verifyTrue(labelPropertyMap.get("Payments_CD_customercity"), customerLabelMap.get("L_customercity") , "Customer City label validation");					
			PrintTemplate.verifyTrue(labelPropertyMap.get("Payments_CD_customercountry"), customerLabelMap.get("L_customercountry") , "Customer Country label validation");					
			
			for (Activity_WEB_PaymentDetails paymentDetails : paymentInfoList) {
				
				if (paymentDetails.getCountry().equalsIgnoreCase("USA") || paymentDetails.getCountry().equalsIgnoreCase("Canada") || paymentDetails.getCountry().equalsIgnoreCase("Australia")) {
					PrintTemplate.verifyTrue(labelPropertyMap.get("Payments_CD_customerState"), customerLabelMap.get("L_customerState") , "Customer State label validation");					
				}								
			}
						
			PrintTemplate.verifyTrue(labelPropertyMap.get("Payments_CD_customerPostalCode"), customerLabelMap.get("L_customerPostalCode") , "Customer PostalCode label validation");					
			PrintTemplate.verifyTrue(labelPropertyMap.get("Payments_CD_customerPhone_number"), customerLabelMap.get("L_customerPhone_number") , "Customer Phone_number label validation");					
			PrintTemplate.verifyTrue(labelPropertyMap.get("Payments_CD_customerMobileNumber"), customerLabelMap.get("L_customerMobileNumber") , "Customer Mobile Number label validation");					
			PrintTemplate.verifyTrue(labelPropertyMap.get("Payments_CD_customerfEmail"), customerLabelMap.get("L_customerfEmail") , "Customer Email label validation");					
			PrintTemplate.verifyTrue(labelPropertyMap.get("Payments_CD_customerVerifyEmail"), customerLabelMap.get("L_customerVerifyEmail") , "Customer Verify Email label validation");					
			PrintTemplate.verifyContains(labelPropertyMap.get("Payments_CD_SelectOneAsTheECM"), customerLabelMap.get("L_SelectOneAsTheECM") , "Select One As The ECM label validation");					
			//PrintTemplate.markTableEnd();
			
			//PrintTemplate.setTableHeading("Customer Details Page - Mandatory Field Validations");
			PrintTemplate.verifyTrue(true, customerMandatoryMap.get("Man_customertitle"), "Mandatory status of Customer Title");
			PrintTemplate.verifyTrue(true, customerMandatoryMap.get("Man_customerfirstname"), "Mandatory status of Customer FirstName");
			PrintTemplate.verifyTrue(true, customerMandatoryMap.get("Man_customerlastname"), "Mandatory status of Customer LastName");
			PrintTemplate.verifyTrue(true, customerMandatoryMap.get("Man_customeraddress"), "Mandatory status of Customer Address");
			PrintTemplate.verifyTrue(true, customerMandatoryMap.get("Man_customercity"), "Mandatory status of Customer City");
			PrintTemplate.verifyTrue(true, customerMandatoryMap.get("Man_customercountry"), "Mandatory status of Customer Country");
			
			for (Activity_WEB_PaymentDetails paymentDetails : paymentInfoList) {
	
				if (paymentDetails.getCountry().equalsIgnoreCase("USA") || paymentDetails.getCountry().equalsIgnoreCase("Canada") || paymentDetails.getCountry().equalsIgnoreCase("Australia")) {
					PrintTemplate.verifyTrue(true, customerMandatoryMap.get("Man_customerState"), "Mandatory status of Customer State");
					PrintTemplate.verifyTrue(true, customerMandatoryMap.get("Man_customerPostalCode"), "Mandatory status of Customer Postal Code");
				}								
			}
						
			PrintTemplate.verifyTrue(true, customerMandatoryMap.get("Man_customerPhone_numberAreaCode"), "Mandatory status of Customer Phone_number Area Code");
			PrintTemplate.verifyTrue(true, customerMandatoryMap.get("Man_customerPhone_number"), "Mandatory status of Customer Phone_number");
			PrintTemplate.verifyTrue(true, customerMandatoryMap.get("Man_customerfEmail"), "Mandatory status of Customer Email");
			PrintTemplate.verifyTrue(true, customerMandatoryMap.get("Man_customerVerifyEmail"), "Mandatory status of Customer Verify Email");
			//PrintTemplate.markTableEnd();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	
	
	public void getOccupancyDetailsValidations(Web_Com_OccupancyDetailsPage occupancPage){
		
		try {
			HashMap<String, Boolean> occupancyInputMap = occupancPage.getActivity_OccupancyPageInputFields();
			HashMap<String, String> occupancyLabelMap = occupancPage.getActivity_OccupancyPageLabels();
			
			PrintTemplate.verifyTrue(true, occupancyInputMap.get("IN_occupancyActivity_title"), "Availability Of Customer title dropdown - OccupancyDetails");												
			PrintTemplate.verifyTrue(true, occupancyInputMap.get("IN_occupancyActivity_first_name"), "Availability Of Customer first name field - OccupancyDetails");
			PrintTemplate.verifyTrue(true, occupancyInputMap.get("IN_occupancyActivity_last_name"), "Availability Of Customer last name field - OccupancyDetails");
			PrintTemplate.verifyTrue(true, occupancyInputMap.get("IN_PickDetails_Station"), "Availability Of PickUp - Station radiobutton - OccupancyDetails");												
			PrintTemplate.verifyTrue(true, occupancyInputMap.get("IN_PickDetails_Airport"), "Availability Of PickUp - Airport radiobutton - OccupancyDetails");
			PrintTemplate.verifyTrue(true, occupancyInputMap.get("IN_PickDetails_Hotel"), "Availability Of PickUp - Hotel radiobutton - OccupancyDetails");
			PrintTemplate.verifyTrue(true, occupancyInputMap.get("IN_PickDetails_Station"), "Availability Of DropOff - Station radiobutton - OccupancyDetails");												
			PrintTemplate.verifyTrue(true, occupancyInputMap.get("IN_PickDetails_Airport"), "Availability Of DropOff - Airport radiobutton - OccupancyDetails");
			PrintTemplate.verifyTrue(true, occupancyInputMap.get("IN_PickDetails_Hotel"), "Availability Of DropOff - Hotel radiobutton - OccupancyDetails");					
			PrintTemplate.verifyTrue(true, occupancyInputMap.get("IN_OccupancyDetails_PrevStep"), "Availability Of Occupancy Details_PrevStep Button - OccupancyDetails");
			PrintTemplate.verifyTrue(true, occupancyInputMap.get("IN_OccupancyDetails_ProceedToBillingInfo"), "Availability Of Proceed To BillingInfo Button - OccupancyDetails");
			//PrintTemplate.markTableEnd();
			
			//PrintTemplate.setTableHeading("Occupancy Details Page - Label Text Validations");
			PrintTemplate.verifyTrue(labelPropertyMap.get("Payments_OD_occupancyActivity_Title"), occupancyLabelMap.get("L_occupancyActivity_Title") , "Customer Ttitle label validation");					
			PrintTemplate.verifyTrue(labelPropertyMap.get("Payments_OD_occupancyActivity_FName"), occupancyLabelMap.get("L_occupancyActivity_FName") , "Customer FirstName label validation");					
			PrintTemplate.verifyTrue(labelPropertyMap.get("Payments_OD_occupancyActivity_LName"), occupancyLabelMap.get("L_occupancyActivity_LName") , "Customer LastName label validation");					
			PrintTemplate.verifyTrue(labelPropertyMap.get("Payments_OD_occupancyActivity_AdultOne"), occupancyLabelMap.get("L_occupancyActivity_AdultOne") , "Adult 1 label validation");					
			PrintTemplate.verifyTrue(labelPropertyMap.get("Payments_OD_occupancyActivityPickDropDetails"), occupancyLabelMap.get("L_occupancyActivityPickDropDetails") , "PickupDropOff Details label validation");					
			PrintTemplate.verifyTrue(labelPropertyMap.get("Payments_OD_PickDropDetailsEnter"), occupancyLabelMap.get("L_PickDropDetailsEnter") , "PickUpDropOff enter label validation");					
			PrintTemplate.verifyTrue(labelPropertyMap.get("Payments_OD_PickDetails_Heading"), occupancyLabelMap.get("L_PickDetails_Heading") , "Pickup Details label validation");					
			PrintTemplate.verifyTrue(labelPropertyMap.get("Payments_OD_PickDetails_Station"), occupancyLabelMap.get("L_PickDetails_Station") , "Station label validation - Pickup");					
			PrintTemplate.verifyTrue(labelPropertyMap.get("Payments_OD_PickDetails_Airport"), occupancyLabelMap.get("L_PickDetails_Airport") , "Airport label validation - Pickup");					
			PrintTemplate.verifyTrue(labelPropertyMap.get("Payments_OD_PickDetails_Hotel"), occupancyLabelMap.get("L_PickDetails_Hotel") , "Hotel label validation - Pickup");											
			PrintTemplate.verifyTrue(labelPropertyMap.get("Payments_OD_DropDownDetails_Heading"), occupancyLabelMap.get("L_DropDownDetails_Heading") , "Drop Off Details label validation");					
			PrintTemplate.verifyTrue(labelPropertyMap.get("Payments_OD_DropDownDetails_Station"), occupancyLabelMap.get("L_DropDownDetails_Station") , "Station label validation - Drop Off");					
			PrintTemplate.verifyTrue(labelPropertyMap.get("Payments_OD_DropDownDetails_Airport"), occupancyLabelMap.get("L_DropDownDetails_Airpor") , "Airport label validation - Drop Off");					
			PrintTemplate.verifyTrue(labelPropertyMap.get("Payments_OD_DropDownDetails_Hotel"), occupancyLabelMap.get("L_DropDownDetails_Hotel") , "Hotel label validation - Drop Off");					
			//PrintTemplate.markTableEnd();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	
	
	public void getBillingInfoDetailsValidations(Web_Com_BillingInformationPage billingPage){
		
		try {
			HashMap<String, Boolean> billingPageInputMap = billingPage.getBillingInfoInputFields();
			HashMap<String, String> billingPageLabelMap = billingPage.getBillingInfoLabels();
			
			PrintTemplate.verifyTrue(true, billingPageInputMap.get("IN_DiscountCouponNumber"), "Availability Of Discount Coupon field - BillingInfo");
			PrintTemplate.verifyTrue(true, billingPageInputMap.get("IN_DiscountCouponValidate"), "Availability Of Discount validate button - BillingInfo");					
			PrintTemplate.verifyTrue(true, billingPageInputMap.get("IN_CreditCardList"), "Availability Of Credit card dropdown - BillingInfo");
			PrintTemplate.verifyTrue(true, billingPageInputMap.get("IN_credit_card_note"), "Availability Of Credit note - BillingInfo");
			PrintTemplate.verifyTrue(true, billingPageInputMap.get("IN_BillingInfo_PrevStep"), "Availability Of BillingInfo_PrevStep Button - BillingInfo");
			PrintTemplate.verifyTrue(true, billingPageInputMap.get("IN_BillingInfo_ProceedToNotes"), "Availability Of Proceed To Make special requests Button - BillingInfo");
			//PrintTemplate.markTableEnd();
			
			//PrintTemplate.setTableHeading("Billing Informations Page - Label Text Validations");
			PrintTemplate.verifyTrue(labelPropertyMap.get("Payments_BI_EnterDiscountCoupon"), billingPageLabelMap.get("L_EnterDiscountCoupon") , "Enter Discount Coupon label validation");					
			PrintTemplate.verifyTrue(labelPropertyMap.get("Payments_BI_cardTypeLabel"), billingPageLabelMap.get("L_cardTypeLabel") , "Card Type label validation");					
			PrintTemplate.verifyTrue(labelPropertyMap.get("Payments_BI_SubTotal"), billingPageLabelMap.get("L_BI_SubTotal") , "Sub Total label validation");											
			PrintTemplate.verifyTrue(labelPropertyMap.get("Payments_BI_TotalTaxandOthers"), billingPageLabelMap.get("L_BI_TotalTaxandOthers") , "Total Taxes and Fees label validation");					
			PrintTemplate.verifyTrue(labelPropertyMap.get("Payments_BI_Total"), billingPageLabelMap.get("L_BI_Total") , "Total Value label validation");					
			PrintTemplate.verifyTrue(labelPropertyMap.get("Payments_BI_AmountProcessedNow"), billingPageLabelMap.get("L_BI_AmountProcessedNow") , "Amount being Processed Now label validation");					
			PrintTemplate.verifyTrue(labelPropertyMap.get("Payments_BI_AmountDue"), billingPageLabelMap.get("L_BI_AmountDue") , "Amount Due at Check-inUtilization label validation");											
			//PrintTemplate.markTableEnd();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}

	
	public void getNotesPageValidations(Web_Com_NotesPage notesPage){
		
		try {
			HashMap<String, Boolean> notesPageInputMap = notesPage.getActivity_AvailabilityOfInputFields();
			HashMap<String, String> notesPageLabelMap = notesPage.getNotesLabels();
			
			PrintTemplate.verifyTrue(true, notesPageInputMap.get("IN_customerNote"), "Availability Of Customer Notes - Notes");					
			PrintTemplate.verifyTrue(true, notesPageInputMap.get("IN_ActivityBillingInfo_Name_DropDown"), "Availability Of Activity list drop dropdown - Notes");
			PrintTemplate.verifyTrue(true, notesPageInputMap.get("IN_ProgramNotes"), "Availability Of Program Notes - Notes");
			PrintTemplate.verifyTrue(true, notesPageInputMap.get("IN_Notes_PrevStep"), "Availability Of Notes_PrevStep Button - Notes");
			PrintTemplate.verifyTrue(true, notesPageInputMap.get("IN_Notes_ProceedToTerms"), "Availability Of Proceed To Terms and Condition Button - Notes");
			//PrintTemplate.markTableEnd();
			
			//PrintTemplate.setTableHeading("Special Notes Page - Label Text Validations");
			PrintTemplate.verifyTrue(labelPropertyMap.get("Payments_BI_CustomerNoteLabel"), notesPageLabelMap.get("L_CustomerNoteLabel") , "Customer Notes label validation");					
			//PrintTemplate.markTableEnd();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	
	
	public void getTermsAndConditionsValidations(Web_Com_TermsPage termsPage){
		
		try {
			HashMap<String, Boolean> termsPageInputMap = termsPage.getTermsAndConditionInputFields();
			HashMap<String, String> termsPageLabelMap = termsPage.getTermAndConditionPageLabels();
			HashMap<String, Boolean> termsPageMandatoryMap = termsPage.getTCMandatoryInputFields();
			
			PrintTemplate.verifyTrue(true, termsPageInputMap.get("IN_TC_CancelledCheckBox"), "Availability Of Confirmed read cancellation checkbox - Terms&Condition");
			PrintTemplate.verifyTrue(true, termsPageInputMap.get("IN_IConfirmedReadTheTermsAndConditions"), "Availability Of Confirmed Terms&Condition checkbox - Terms&Condition");
			PrintTemplate.verifyTrue(true, termsPageInputMap.get("IN_WouldLikeToReceiv"), "Availability Of Special offer checkbox - Terms&Condition");
			PrintTemplate.verifyTrue(true, termsPageInputMap.get("IN_TC_PrevStep"), "Availability Of Terms&Condition_PrevStep Button - Terms&Condition");
			//PrintTemplate.markTableEnd();
			
			//PrintTemplate.setTableHeading("Terms & Conditions Page - Label Text Validations");
			PrintTemplate.verifyContains(labelPropertyMap.get("Payments_TC_FullyreRundableifCancelled"), termsPageLabelMap.get("L_TC_FullyreRundableifCancelled") , "Fully Rundable label validation");					
			PrintTemplate.verifyTrue(labelPropertyMap.get("Payments_TC_IConfirmReadPolicy"), termsPageLabelMap.get("L_TC_IConfirmReadPolicy") , "I Confirm Read Policy label validation");					
			PrintTemplate.verifyTrue(labelPropertyMap.get("Payments_TC_GeneralTermsAndCondition"), termsPageLabelMap.get("L_TC_GeneralTermsAndCondition") , "General Terms And Condition label validation");											
			PrintTemplate.verifyTrue(labelPropertyMap.get("Payments_TC_ChargeAndCancellationFeesApply"), termsPageLabelMap.get("L_TC_ChargeAndCancellationFeesApply") , "Charge and cancellation fees apply label validation");					
			PrintTemplate.verifyTrue(labelPropertyMap.get("Payments_TC_AvailabilityAtTimeOfMaking"), termsPageLabelMap.get("L_TC_AvailabilityAtTimeOfMaking") , "Availability at time label validation");					
			PrintTemplate.verifyTrue(labelPropertyMap.get("Payments_TC_TheIndividualConditions"), termsPageLabelMap.get("L_TC_TheIndividualConditions") , "The individual conditions label validation");					
			PrintTemplate.verifyContains(labelPropertyMap.get("Payments_TC_OmanairholidaysTermsCondition"), termsPageLabelMap.get("L_TC_OmanairholidaysTermsCondition") , "Terms & Conditions apply label validation");											
			PrintTemplate.verifyTrue(labelPropertyMap.get("Payments_TC_IfYouRequiretoAmendOrCancel"), termsPageLabelMap.get("L_TC_IfYouRequiretoAmendOrCancel") , "Require to amend or cancel your booking label validation");					
			PrintTemplate.verifyContains(labelPropertyMap.get("Payments_TC_portalemail"), termsPageLabelMap.get("L_TC_portalemail") , "Portal Email label validation");					
			PrintTemplate.verifyContains(labelPropertyMap.get("Payments_TC_portalphone"), termsPageLabelMap.get("L_TC_portalphone") , "Portal Phone Value label validation");					
			PrintTemplate.verifyEqualIgnoreCase(labelPropertyMap.get("Payments_TC_IConfirmedReadTheTermsAndConditionsText").replaceAll(" ", ""), termsPageLabelMap.get("L_TC_IConfirmedReadTheTermsAndConditionsText").replaceAll(" ", "") , "Confirmed Read The Terms And Conditions label validation");					
			PrintTemplate.verifyContains(labelPropertyMap.get("Payments_TC_WouldLikeToReceivText"), termsPageLabelMap.get("L_WouldLikeToReceivText") , "would like to receive the special offers label validation");											
			PrintTemplate.verifyTrue(labelPropertyMap.get("Payments_TC_PleaseCheckThatAllYourBooking"), termsPageLabelMap.get("L_PleaseCheckThatAllYourBooking") , "Please check that all your booking details label validation");					
			//PrintTemplate.markTableEnd();
			
			//PrintTemplate.setTableHeading("Terms & Conditions Page - Mandatory Field Validations");
			PrintTemplate.verifyTrue(true, termsPageMandatoryMap.get("Man_TC_CancelledCheckBox"), "Mandatory status of 'I confirm I have read the Cancellation Policy' CheckBox");
			PrintTemplate.verifyTrue(true, termsPageMandatoryMap.get("Man_IConfirmedReadTheTermsAndConditions"), "Mandatory status of 'I confirm I have read the Terms & Conditions of booking ' CheckBox");		
			//PrintTemplate.markTableEnd();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	
	
	public void getConfirmationPageValidations(Web_Act_ConfirmationPage confirmationPage){
		
		try {
			HashMap<String, String> confirmationPageLabelMap = confirmationPage.getConfirmationPageLabels();
			
			PrintTemplate.verifyTrue(labelPropertyMap.get("Confirmation_Confirmationstatus"), confirmationPageLabelMap.get("l_Confirmationstatus") , "Confirmation status label validation");					
			PrintTemplate.verifyTrue(labelPropertyMap.get("Confirmation_BookingReference"), confirmationPageLabelMap.get("l_BookingReference") , "Booking Reference label validation");											
			PrintTemplate.verifyTrue(labelPropertyMap.get("Confirmation_ReservationNo"), confirmationPageLabelMap.get("l_ReservationNo") , "Reservation No label validation");					
			PrintTemplate.verifyTrue(labelPropertyMap.get("Confirmation_PrintRecords"), confirmationPageLabelMap.get("l_PrintRecords") , "Please print this page for your records label validation");					
			PrintTemplate.verifyContains(labelPropertyMap.get("Confirmation_ConfirmationMailSendMail"), confirmationPageLabelMap.get("l_ConfirmationMailSendMail") , "A confirmation email has been sent label validation");					
			PrintTemplate.verifyTrue(labelPropertyMap.get("Confirmation_receiveMailText"), confirmationPageLabelMap.get("l_receiveMailText") , "f you do not receive this email within 24 hours then please contact customer services on label validation");											
			PrintTemplate.verifyContains(labelPropertyMap.get("Confirmation_PortalTel"), confirmationPageLabelMap.get("l_PortalTel") , "Tel label validation");					
			PrintTemplate.verifyContains(labelPropertyMap.get("Confirmation_PortalFax"), confirmationPageLabelMap.get("l_PortalFax") , "Fax label validation");					
			
			PrintTemplate.verifyTrue(labelPropertyMap.get("Confirmation_BookingItineraryInformation"), confirmationPageLabelMap.get("l_BookingItineraryInformation") , "Booking itinerary information label validation");					
			PrintTemplate.verifyTrue(labelPropertyMap.get("Confirmation_BookingStatus"), confirmationPageLabelMap.get("l_BookingStatus") , "Status label validation");											
			PrintTemplate.verifyTrue(labelPropertyMap.get("Confirmation_RateType"), confirmationPageLabelMap.get("l_RateType") , "RateType label validation");					
			PrintTemplate.verifyContains(labelPropertyMap.get("Confirmation_Rate"), confirmationPageLabelMap.get("l_Rate") , "Rate label validation");					
			PrintTemplate.verifyTrue(labelPropertyMap.get("Confirmation_Qty"), confirmationPageLabelMap.get("l_Qty") , "Qty label validation");											
			PrintTemplate.verifyTrue(labelPropertyMap.get("Confirmation_UsableOnTable"), confirmationPageLabelMap.get("l_UsableOnTable") , "UsableOn label validation");					
			PrintTemplate.verifyTrue(labelPropertyMap.get("Confirmation_ActivityRate"), confirmationPageLabelMap.get("l_ActivityRate") , "Activity Rate label validation");					
			PrintTemplate.verifyTrue(labelPropertyMap.get("Confirmation_SubTotal"), confirmationPageLabelMap.get("l_SubTotal") , "Sub Total label validation");					
			PrintTemplate.verifyTrue(labelPropertyMap.get("Confirmation_TaxCharges"), confirmationPageLabelMap.get("l_TaxCharges") , "Tax & Charges label validation");											
			PrintTemplate.verifyTrue(labelPropertyMap.get("Confirmation_Total"), confirmationPageLabelMap.get("l_Total") , "Total label validation");					
			PrintTemplate.verifyTrue(labelPropertyMap.get("Confirmation_D_SubTotal"), confirmationPageLabelMap.get("l_D_SubTotal") , "Detail Sub Total label validation");					
			PrintTemplate.verifyTrue(labelPropertyMap.get("Confirmation_D_TotalTaxes"), confirmationPageLabelMap.get("l_D_TotalTaxes") , "Detail Total Taxes and Other Charges  label validation");					
			PrintTemplate.verifyTrue(labelPropertyMap.get("Confirmation_D_Total"), confirmationPageLabelMap.get("l_D_Total") , "Detail Total label validation");											
			PrintTemplate.verifyTrue(labelPropertyMap.get("Confirmation_D_AmountBeingProcessedNow"), confirmationPageLabelMap.get("l_D_AmountBeingProcessedNow") , "Detail Amount being Processed Now label validation");					
			PrintTemplate.verifyTrue(labelPropertyMap.get("Confirmation_D_AmountDue"), confirmationPageLabelMap.get("l_D_AmountDue") , "Detail Amount Due at Check-in / Utilization label validation");					
			
			PrintTemplate.verifyTrue(labelPropertyMap.get("Confirmation_UserInfomation"), confirmationPageLabelMap.get("l_UserInfomation") , "User infomation label validation");					
			PrintTemplate.verifyTrue(labelPropertyMap.get("Confirmation_FirstName"), confirmationPageLabelMap.get("l_FirstName") , "First Name label validation");											
			PrintTemplate.verifyTrue(labelPropertyMap.get("Confirmation_LastName"), confirmationPageLabelMap.get("l_LastName") , "Last Name label validation");					
			PrintTemplate.verifyTrue(labelPropertyMap.get("Confirmation_Address1"), confirmationPageLabelMap.get("l_Address1") , "Address1 label validation");					
			PrintTemplate.verifyTrue(labelPropertyMap.get("Confirmation_City"), confirmationPageLabelMap.get("l_City") , "City label validation");					
			PrintTemplate.verifyTrue(labelPropertyMap.get("Confirmation_Country"), confirmationPageLabelMap.get("l_Country") , "Country label validation");											
			PrintTemplate.verifyTrue(labelPropertyMap.get("Confirmation_State"), confirmationPageLabelMap.get("l_State") , "State label validation");					
			PrintTemplate.verifyTrue(labelPropertyMap.get("Confirmation_PostalCode"), confirmationPageLabelMap.get("l_PostalCode") , "Postal Code label validation");					
			PrintTemplate.verifyTrue(labelPropertyMap.get("Confirmation_PhoneNumber"), confirmationPageLabelMap.get("l_PhoneNumber") , "Phone Number label validation");					
			PrintTemplate.verifyTrue(labelPropertyMap.get("Confirmation_EmergencyNo"), confirmationPageLabelMap.get("l_EmergencyNo") , "EmergencyNo label validation");											
			PrintTemplate.verifyTrue(labelPropertyMap.get("Confirmation_Email"), confirmationPageLabelMap.get("l_Email") , "Email label validation");					
			PrintTemplate.verifyTrue(labelPropertyMap.get("Confirmation_ActivityOccupancyDetails"), confirmationPageLabelMap.get("l_ActivityOccupancyDetails") , "Activity Occupancy Details label validation");					
			PrintTemplate.verifyTrue(labelPropertyMap.get("Confirmation_Occupancy_Title"), confirmationPageLabelMap.get("l_Occupancy_Title") , "Occupancy_Title label validation");					
			PrintTemplate.verifyTrue(labelPropertyMap.get("Confirmation_Occupancy_FName"), confirmationPageLabelMap.get("l_Occupancy_FName") , "Occupancy_FName label validation");											
			PrintTemplate.verifyTrue(labelPropertyMap.get("Confirmation_Occupancy_LName"), confirmationPageLabelMap.get("l_Occupancy_LName") , "Occupancy_LName label validation");					
			PrintTemplate.verifyTrue(labelPropertyMap.get("Confirmation_Occupancy_Adult1"), confirmationPageLabelMap.get("l_Occupancy_Adult1") , "Occupancy_Adult1 label validation");					
									
			PrintTemplate.verifyTrue(labelPropertyMap.get("Confirmation_BillingInformationCredit"), confirmationPageLabelMap.get("l_BillingInformationCredit") , "Billing Information/Credit card details label validation");					
			PrintTemplate.verifyTrue(labelPropertyMap.get("Confirmation_MerchantTrackID"), confirmationPageLabelMap.get("l_MerchantTrackID") , "Merchant Track ID / Reference Number label validation");											
			PrintTemplate.verifyTrue(labelPropertyMap.get("Confirmation_AuthenticationReference"), confirmationPageLabelMap.get("l_AuthenticationReference") , "Authentication Reference label validation");					
			PrintTemplate.verifyTrue(labelPropertyMap.get("Confirmation_PaymentID"), confirmationPageLabelMap.get("l_PaymentID") , "Payment ID label validation");					
			PrintTemplate.verifyContains(labelPropertyMap.get("Confirmation_Amount"), confirmationPageLabelMap.get("l_Amount") , "Amount label validation");					
			PrintTemplate.verifyTrue(labelPropertyMap.get("Confirmation_AmendmentAndCancellation"), confirmationPageLabelMap.get("l_AmendmentAndCancellation") , "Amendment and cancellation label validation");											
			PrintTemplate.verifyContains(labelPropertyMap.get("Confirmation_TC_FullyreRundableifCancelled"), confirmationPageLabelMap.get("l_TC_FullyreRundableifCancelled") , "Fully refundable if cancelled on or before label validation");					
			PrintTemplate.verifyTrue(labelPropertyMap.get("Confirmation_TC_GeneralTermsAndCondition_xpath"), confirmationPageLabelMap.get("l_TC_GeneralTermsAndCondition") , "General Terms and Conditions label validation");					
			PrintTemplate.verifyTrue(labelPropertyMap.get("Confirmation_TC_ChargeAndCancellationFeesApply"), confirmationPageLabelMap.get("l_TC_ChargeAndCancellationFeesApply") , "Charge and cancellation fees apply and vary depending on, label validation");					
			PrintTemplate.verifyTrue(labelPropertyMap.get("Confirmation_TC_AvailabilityAtTimeOfMaking"), confirmationPageLabelMap.get("l_TC_AvailabilityAtTimeOfMaking") , "Availability at time of making the change or cancellation label validation");											
			PrintTemplate.verifyTrue(labelPropertyMap.get("Confirmation_TC_TheIndividualConditions"), confirmationPageLabelMap.get("l_TC_TheIndividualConditions") , "The individual conditions that will be presented at time of booking label validation");					
			PrintTemplate.verifyContains(labelPropertyMap.get("Confirmation_TC_TermsConditionsAppl"), confirmationPageLabelMap.get("l_TC_TermsConditionsApply") , "Terms & Conditions apply label validation");					
			//PrintTemplate.markTableEnd();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	public void getQuotationConfirmationPageValidations(Web_Act_QuotationConfirmationPage quoteConfirmationPage){
		
		try {
			HashMap<String, String> quoteconPageLabelMap = quoteConfirmationPage.getQuotationConfirmationPageLabels();
			
			PrintTemplate.verifyTrue(labelPropertyMap.get("Quotation_Confirmation_Status"), quoteconPageLabelMap.get("l_QuotationStatus") , "Quotation status label validation");					
			PrintTemplate.verifyTrue(labelPropertyMap.get("Quotation_Confirmation_SavedMessage"), quoteconPageLabelMap.get("l_QuotationSavedMessage") , "Quotation Saved Successfully label validation");					
			PrintTemplate.verifyTrue(labelPropertyMap.get("Confirmation_BookingReference"), quoteconPageLabelMap.get("l_BookingReference") , "Booking Reference label validation");											
			PrintTemplate.verifyTrue(labelPropertyMap.get("Quotation_Confirmation_QuotationNo"), quoteconPageLabelMap.get("l_QuotationNo") , "Reservation No label validation");					
			PrintTemplate.verifyTrue(labelPropertyMap.get("Confirmation_PrintRecords"), quoteconPageLabelMap.get("l_PrintRecords") , "Please print this page for your records label validation");					
			PrintTemplate.verifyContains(labelPropertyMap.get("Confirmation_ConfirmationMailSendMail"), quoteconPageLabelMap.get("l_ConfirmationMailSendMail") , "A confirmation email has been sent label validation");					
			PrintTemplate.verifyTrue(labelPropertyMap.get("Confirmation_receiveMailText"), quoteconPageLabelMap.get("l_receiveMailText") , "f you do not receive this email within 24 hours then please contact customer services on label validation");											
			PrintTemplate.verifyContains(labelPropertyMap.get("Confirmation_PortalTel"), quoteconPageLabelMap.get("l_PortalTel") , "Tel label validation");					
			PrintTemplate.verifyContains(labelPropertyMap.get("Confirmation_PortalFax"), quoteconPageLabelMap.get("l_PortalFax") , "Fax label validation");					
			
			PrintTemplate.verifyTrue(labelPropertyMap.get("Confirmation_BookingItineraryInformation"), quoteconPageLabelMap.get("l_BookingItineraryInformation") , "Booking itinerary information label validation");					
			PrintTemplate.verifyTrue(labelPropertyMap.get("Confirmation_BookingStatus"), quoteconPageLabelMap.get("l_BookingStatus") , "Status label validation");											
			PrintTemplate.verifyTrue(labelPropertyMap.get("Confirmation_RateType"), quoteconPageLabelMap.get("l_RateType") , "RateType label validation");					
			PrintTemplate.verifyContains(labelPropertyMap.get("Confirmation_Rate"), quoteconPageLabelMap.get("l_Rate") , "Rate label validation");					
			PrintTemplate.verifyTrue(labelPropertyMap.get("Confirmation_Qty"), quoteconPageLabelMap.get("l_Qty") , "Qty label validation");											
			PrintTemplate.verifyTrue(labelPropertyMap.get("Confirmation_UsableOnTable"), quoteconPageLabelMap.get("l_UsableOnTable") , "UsableOn label validation");					
			PrintTemplate.verifyTrue(labelPropertyMap.get("Confirmation_ActivityRate"), quoteconPageLabelMap.get("l_ActivityRate") , "Activity Rate label validation");					
			PrintTemplate.verifyTrue(labelPropertyMap.get("Confirmation_SubTotal"), quoteconPageLabelMap.get("l_SubTotal") , "Sub Total label validation");					
			PrintTemplate.verifyTrue(labelPropertyMap.get("Confirmation_TaxCharges"), quoteconPageLabelMap.get("l_TaxCharges") , "Tax & Charges label validation");											
			PrintTemplate.verifyTrue(labelPropertyMap.get("Confirmation_Total"), quoteconPageLabelMap.get("l_Total") , "Total label validation");					
			PrintTemplate.verifyTrue(labelPropertyMap.get("Confirmation_D_SubTotal"), quoteconPageLabelMap.get("l_D_SubTotal") , "Detail Sub Total label validation");					
			PrintTemplate.verifyTrue(labelPropertyMap.get("Confirmation_D_TotalTaxes"), quoteconPageLabelMap.get("l_D_TotalTaxes") , "Detail Total Taxes and Other Charges  label validation");					
			PrintTemplate.verifyTrue(labelPropertyMap.get("Confirmation_D_Total"), quoteconPageLabelMap.get("l_D_Total") , "Detail Total label validation");											
			PrintTemplate.verifyTrue(labelPropertyMap.get("Confirmation_D_AmountBeingProcessedNow"), quoteconPageLabelMap.get("l_D_AmountBeingProcessedNow") , "Detail Amount being Processed Now label validation");					
					
			PrintTemplate.verifyTrue(labelPropertyMap.get("Confirmation_UserInfomation"), quoteconPageLabelMap.get("l_UserInfomation") , "User infomation label validation");					
			PrintTemplate.verifyTrue(labelPropertyMap.get("Confirmation_FirstName"), quoteconPageLabelMap.get("l_FirstName") , "First Name label validation");											
			PrintTemplate.verifyTrue(labelPropertyMap.get("Confirmation_LastName"), quoteconPageLabelMap.get("l_LastName") , "Last Name label validation");					
			PrintTemplate.verifyTrue(labelPropertyMap.get("Confirmation_Address1"), quoteconPageLabelMap.get("l_Address1") , "Address1 label validation");					
			PrintTemplate.verifyTrue(labelPropertyMap.get("Confirmation_City"), quoteconPageLabelMap.get("l_City") , "City label validation");					
			PrintTemplate.verifyTrue(labelPropertyMap.get("Confirmation_Country"), quoteconPageLabelMap.get("l_Country") , "Country label validation");											
			PrintTemplate.verifyTrue(labelPropertyMap.get("Confirmation_State"), quoteconPageLabelMap.get("l_State") , "State label validation");					
			PrintTemplate.verifyTrue(labelPropertyMap.get("Confirmation_PostalCode"), quoteconPageLabelMap.get("l_PostalCode") , "Postal Code label validation");					
			PrintTemplate.verifyTrue(labelPropertyMap.get("Confirmation_PhoneNumber"), quoteconPageLabelMap.get("l_PhoneNumber") , "Phone Number label validation");					
			PrintTemplate.verifyTrue(labelPropertyMap.get("Confirmation_EmergencyNo"), quoteconPageLabelMap.get("l_EmergencyNo") , "EmergencyNo label validation");											
			PrintTemplate.verifyTrue(labelPropertyMap.get("Confirmation_Email"), quoteconPageLabelMap.get("l_Email") , "Email label validation");					
			
			PrintTemplate.verifyTrue(labelPropertyMap.get("Confirmation_AmendmentAndCancellation"), quoteconPageLabelMap.get("l_AmendmentAndCancellation") , "Amendment and cancellation label validation");											
			PrintTemplate.verifyTrue(labelPropertyMap.get("Confirmation_TC_GeneralTermsAndCondition_xpath"), quoteconPageLabelMap.get("l_TC_GeneralTermsAndCondition") , "General Terms and Conditions label validation");					
			PrintTemplate.verifyTrue(labelPropertyMap.get("Confirmation_TC_ChargeAndCancellationFeesApply"), quoteconPageLabelMap.get("l_TC_ChargeAndCancellationFeesApply") , "Charge and cancellation fees apply and vary depending on, label validation");					
			PrintTemplate.verifyTrue(labelPropertyMap.get("Confirmation_TC_AvailabilityAtTimeOfMaking"), quoteconPageLabelMap.get("l_TC_AvailabilityAtTimeOfMaking") , "Availability at time of making the change or cancellation label validation");											
			PrintTemplate.verifyTrue(labelPropertyMap.get("Confirmation_TC_TheIndividualConditions"), quoteconPageLabelMap.get("l_TC_TheIndividualConditions") , "The individual conditions that will be presented at time of booking label validation");					
			PrintTemplate.verifyContains(labelPropertyMap.get("Confirmation_TC_TermsConditionsAppl"), quoteconPageLabelMap.get("l_TC_TermsConditionsApply") , "Terms & Conditions apply label validation");					
			//PrintTemplate.markTableEnd();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	
	}
	
	
	public void getBasicFlowValidations(ActivityDetails activityDetails){
		
		try {
			
			CurrencyConverter currencyConvert = new CurrencyConverter();
			ArrayList<Activity_WEB_ActivityInfo> activityInfoList = searchObject.getInventoryInfo();
			
			for (Activity_WEB_ActivityInfo actInfo : activityInfoList) {
			
				if (activityDetails.isSystemLoaded() == true) {
					
					if (activityDetails.isResultsPageloaded() == true && activityDetails.isActResultsAvailable() == true) {
						
						//PrintTemplate.setTableHeading("Results Page Data/Rates Validations");
						PrintTemplate.verifyEqualIgnoreCase(searchObject.getActivityName(), activityDetails.getResultsPage_ActivityName(), "Results Page - Activity Name");
						PrintTemplate.verifyContains(actInfo.getActivityDescription().replaceAll(" ", "").toLowerCase(), activityDetails.getResultsPage_ActivityDescription().replaceAll(" ", "").toLowerCase(), "Results Page - Activity Description");
						PrintTemplate.verifyTrue(searchObject.getSellingCurrency(), activityDetails.getResultsPage_ActCurrency(), "Results Page - Selling Currency");
						PrintTemplate.verifyTrue(Integer.toString(activityDetails.getActivityRate()), activityDetails.getResultsPage_SubTotal(), "Results Page - Activity Booking value");
						PrintTemplate.verifyTrue(true, activityDetails.isResultsPage_MoreInfoLoaded(), "Results Page - More Info loaded");
						PrintTemplate.verifyTrue(true, activityDetails.isResultsPage_cancellationLoaded(), "Results Page - Cancellation Policy loaded");
						
						if (activityDetails.isAddAndContinue() == true){
							PrintTemplate.verifyTrue(true, activityDetails.isResultsPageMybasketLoaded(), "Results Page - MyBasket loaded");
							if (activityDetails.isResultsPageMybasketLoaded() == true){
								PrintTemplate.verifyTrue(searchObject.getSellingCurrency()+activityDetails.getTotalWToutCCFee(), activityDetails.getResultsPage_cartCurrencyValue(), "Results Page - MyBasket Currency and Total");													
							}
						}
						
						PrintTemplate.verifyIntArrayList(activityDetails.getResultsPage_After_ascendingOrder(), activityDetails.getResultsPage_ascendingOrder(), "Results Page - Prize Ascending order");
						PrintTemplate.verifyIntArrayList(activityDetails.getResultsPage_After_descendingOrder(), activityDetails.getResultsPage_descendingOrder(), "Results Page - Prize Descending order");
						PrintTemplate.verifyTrue(true, activityDetails.isPriceSlider_middle(), "Results Page - Price Slider set to Middle both prices equal");
						PrintTemplate.verifyTrue(true, activityDetails.isPriceSlider_last(), "Results Page - Price Slider set to Last both prices equal");
						
						//PrintTemplate.markTableEnd();
						
						//Payments page
						//PrintTemplate.setTableHeading("Payments Page Data/Rates Validations");
						
						PrintTemplate.verifyTrue(true, activityDetails.isPaymentsPageLoaded(), "Payments Page loaded");
						
						if (activityDetails.isPaymentsPageLoaded() == true) {
							
							PrintTemplate.verifyTrue(true, activityDetails.isResultsPageMybasketLoaded(), "Payments Page - Cart loaded");
							
							if (activityDetails.isPaymentsPageCartLoaded() == true) {							
								PrintTemplate.verifyTrue(searchObject.getSellingCurrency(), activityDetails.getMyBasket_Currency().split(" ")[0], "Payments Page - My Basket Selling Currency");
								PrintTemplate.verifyTrue(Integer.toString(activityDetails.getTotalBookingValue()), activityDetails.getMyBasket_TotalActivityBookingValue(), "Payments Page - My Basket Total Booking value");							
								PrintTemplate.verifyTrue(Integer.toString(activityDetails.getTotalWToutCCFee()), activityDetails.getPaymentsPage_cart_TotalWTOCCfee(), "Payments Page - My Basket Activity Booking Value");
								
								PrintTemplate.verifyTrue(Integer.toString(activityDetails.getSubTotalValue()), activityDetails.getPaymentsPage_cart_SubTotal(), "Payments Page - My Basket_BI Sub Total value");
								PrintTemplate.verifyTrue(Integer.toString(activityDetails.getTax()+activityDetails.getCreditCardFee()), activityDetails.getPaymentsPage_cart_TotalTax(), "Payments Page - My Basket_BI Total Taxes and Other Charges value");
								PrintTemplate.verifyTrue(Integer.toString(activityDetails.getTotalBookingValue()), activityDetails.getPaymentsPage_cart_Total(), "Payments Page - My Basket_BI Total value");
								PrintTemplate.verifyTrue(Integer.toString(activityDetails.getAmountProcessedNow()), activityDetails.getPaymentsPage_cart_AmountNow(), "Payments Page - My Basket_BI Amount being Processed value");
								PrintTemplate.verifyTrue(Integer.toString(activityDetails.getAmountDue()), activityDetails.getPaymentsPage_cart_AmountDue(), "Payments Page - My Basket_BI Amount Due at Check-in value");
														
								PrintTemplate.verifyTrue(true, activityDetails.isPaymentsActivityMoreDetailsInfo(), "Payments Page - Activity More Details Info loaded");
								
								if (activityDetails.isPaymentsActivityMoreDetailsInfo() == true) {
									PrintTemplate.verifyEqualIgnoreCase(searchObject.getActivityName(), activityDetails.getPaymentsPage_AMD_ActiivtyName(), "Payments Page - Activity More Details - Activity Name");
									PrintTemplate.verifyEqualIgnoreCase(actInfo.getActivityType(), activityDetails.getPaymentsPage_AMD_ActivityTypeName(), "Payments Page - Activity More Details - Activity Type Name");
									
									for (int i = 0; i < activityDetails.getResultsPage_Period().size(); i++) {
										
										PrintTemplate.verifyEqualIgnoreCase(searchObject.getActivityDate(), activityDetails.getPaymentsPage_AMD_UsableOnDate(), "Payments Page - Activity More Details - Usable-On Date");
										PrintTemplate.verifyEqualIgnoreCase(activityDetails.getResultsPage_Period().get(i), activityDetails.getPaymentsPage_AMD_Period(), "Payments Page - Activity More Details - Period / Session");
										PrintTemplate.verifyEqualIgnoreCase(activityDetails.getResultsPage_RateType().get(i), activityDetails.getPaymentsPage_AMD_RateType(), "Payments Page - Activity More Details - Rate Type");
										PrintTemplate.verifyEqualIgnoreCase(Integer.toString(activityDetails.getActivityRate()), activityDetails.getPaymentsPage_AMD_DailyRate(), "Payments Page - Activity More Details - Rate");
										PrintTemplate.verifyEqualIgnoreCase(Integer.toString(paxCnt), activityDetails.getPaymentsPage_AMD_QTY(), "Payments Page - Activity More Details - Qty");
										PrintTemplate.verifyEqualIgnoreCase(searchObject.getSellingCurrency()+Integer.toString(activityDetails.getSubTotalValue()), activityDetails.getPaymentsPage_AMD_TotalWithCurrency().replaceAll(" ", ""), "Payments Page - Activity More Details - Currency & Total");
									}
									
									PrintTemplate.verifyTrue(Integer.toString(activityDetails.getSubTotalValue()), activityDetails.getPaymentsPage_AMD_SubTotal(), "Payments Page - Activity More Details - Sub Total");
									PrintTemplate.verifyTrue(Integer.toString(activityDetails.getTax()), activityDetails.getPaymentsPage_AMD_Tax(), "Payments Page - Activity More Details - Tax & Charges");
									PrintTemplate.verifyTrue(Integer.toString(activityDetails.getTotalWToutCCFee()), activityDetails.getPaymentsPage_AMD_Total(), "Payments Page - Activity More Details - Total");
								
									if (actInfo.getInventoryType().equalsIgnoreCase("Allotments")) {
										PrintTemplate.verifyEqualIgnoreCase("Available to book", activityDetails.getPaymentsPage_AMD_Status(), "Payments Page - Activity More Details - Status");									
									}else{
										PrintTemplate.verifyEqualIgnoreCase("On-request", activityDetails.getPaymentsPage_AMD_Status(), "Payments Page - Activity More Details - Status");																		
									}
									
									PrintTemplate.verifyEqualIgnoreCase(activityDetails.getCancellationDeadline(), activityDetails.getPaymentsPage_AMD_CancellationDealine(), "Payments Page - Activity More Details - Cancellation Deadline");									
									
									for (int i = 0; i < activityDetails.getPaymentsPage_AMD_CancelPolicy().size(); i++) {
										PrintTemplate.verifyEqualIgnoreCase(activityDetails.getCancellationPoliciesList().get(i), activityDetails.getPaymentsPage_AMD_CancelPolicy().get(i), "Payments Page - Activity More Details - Cancellation policy:- "+i+"");																		
									}
					
								}
							}
							
							if (searchObject.getQuotationReq().equalsIgnoreCase("No")) {
								
								//OccupancyDetails
								
								ArrayList<Activity_WEB_PaymentDetails> paymentInfoList = searchObject.getPaymentDetailsInfo();
								
								for (Activity_WEB_PaymentDetails paymentDetails : paymentInfoList) {
									
									PrintTemplate.verifyEqualIgnoreCase(paymentDetails.getCustomerName(), activityDetails.getPaymentsPage_OD_FirstName(), "Payments Page - Occupancy Details - Customer First Name");
									PrintTemplate.verifyEqualIgnoreCase(paymentDetails.getCustomerLastName(), activityDetails.getPaymentsPage_OD_LastName(), "Payments Page - Occupancy Details - Customer Last Name");																	
								}
													
								//billing info
								
								int subTotal = Integer.parseInt(currencyConvert.convert(labelProperty.getPortalSpecificData().get("portal.Currency"), Integer.toString(activityDetails.getSubTotalValue()), labelProperty.getPortalSpecificData().get("portal.pgCurrency"), currencyMap, searchObject.getSellingCurrency()));
								int totalTaxWTCC = Integer.parseInt(currencyConvert.convert(labelProperty.getPortalSpecificData().get("portal.Currency"), Integer.toString(activityDetails.getTax()+activityDetails.getCreditCardFee()), labelProperty.getPortalSpecificData().get("portal.pgCurrency"), currencyMap, searchObject.getSellingCurrency()));
								int totalBookingValue = Integer.parseInt(currencyConvert.convert(labelProperty.getPortalSpecificData().get("portal.Currency"), Integer.toString(activityDetails.getTotalBookingValue()), labelProperty.getPortalSpecificData().get("portal.pgCurrency"), currencyMap, searchObject.getSellingCurrency()));
								int amountProceedNow = Integer.parseInt(currencyConvert.convert(labelProperty.getPortalSpecificData().get("portal.Currency"), Integer.toString(activityDetails.getAmountProcessedNow()), labelProperty.getPortalSpecificData().get("portal.pgCurrency"), currencyMap, searchObject.getSellingCurrency()));
								int amountDue = Integer.parseInt(currencyConvert.convert(labelProperty.getPortalSpecificData().get("portal.Currency"), Integer.toString(activityDetails.getAmountDue()), labelProperty.getPortalSpecificData().get("portal.pgCurrency"), currencyMap, searchObject.getSellingCurrency()));
																
								PrintTemplate.verifyContains(labelProperty.getPortalSpecificData().get("portal.pgCurrency"), activityDetails.getPaymentsPage_BI_CardCurrency(), "Payments Page - Billing Info - Payment Gateway Currency");									
								PrintTemplate.verifyTrue(Integer.toString(amountProceedNow), activityDetails.getPaymentsPage_BI_CardTotal(), "Payments Page - Billing Info - Payment Gateway Total Booking Value");							
								PrintTemplate.verifyContains(labelProperty.getPortalSpecificData().get("portal.pgCurrency"), activityDetails.getPaymentsPage_BI_Currency(), "Payments Page - Billing Info - Currency");									
								
								PrintTemplate.verifyTrue(Integer.toString(subTotal), activityDetails.getPaymentsPage_BI_SubTotal(), "Payments Page - Billing Info - Sub Total");
								PrintTemplate.verifyTrue(Integer.toString(totalTaxWTCC), activityDetails.getPaymentsPage_BI_TotalTax(), "Payments Page - Billing Info - Total Taxes and Fees");
								PrintTemplate.verifyTrue(Integer.toString(totalBookingValue), activityDetails.getPaymentsPage_BI_Total(), "Payments Page - Billing Info - Total Value");
								PrintTemplate.verifyTrue(Integer.toString(amountProceedNow), activityDetails.getPaymentsPage_BI_AmountNow(), "Payments Page - Billing Info - Amount being Processed Now");
								PrintTemplate.verifyTrue(Integer.toString(amountDue), activityDetails.getPaymentsPage_BI_AmountDue(), "Payments Page - Billing Info - Amount Due at Check-in / Utilization");
								
								//Terms
								
								PrintTemplate.verifyEqualIgnoreCase(searchObject.getActivityName(), activityDetails.getPaymentsPage_TC_ActName(), "Payments Page - Terms & Condition - Activity Name");
								PrintTemplate.verifyEqualIgnoreCase(activityDetails.getCancellationDeadline(), activityDetails.getPaymentsPage_TC_CancellationDead_1(), "Payments Page - Terms & Condition - Cancellation Deadline 1");									
								
								for (int i = 0; i < activityDetails.getPaymentsPage_TC_CancelPolicy().size(); i++) {
									PrintTemplate.verifyEqualIgnoreCase(activityDetails.getCancellationPoliciesList().get(i), activityDetails.getPaymentsPage_TC_CancelPolicy().get(i), "Payments Page - Terms & Condition - Cancellation policy:- "+i+"");																		
								}
								
								PrintTemplate.verifyEqualIgnoreCase(activityDetails.getCancellationDeadline(), activityDetails.getPaymentsPage_TC_CancellationDead_2(), "Payments Page - Terms & Condition - Cancellation Deadline 2");									
								
								PrintTemplate.verifyEqualIgnoreCase(labelProperty.getPortalSpecificData().get("Portal.Email"), activityDetails.getPaymentsPage_TC_portalName(), "Payments Page - Terms & Condition - Portal Email");									
								PrintTemplate.verifyEqualIgnoreCase(labelProperty.getPortalSpecificData().get("Portal.Tel"), activityDetails.getPaymentsPage_TC_portalPhone(), "Payments Page - Terms & Condition - Portal TP");									
								
							}
					
						}
						
						//PrintTemplate.markTableEnd();
						
						
						if(searchObject.getQuotationReq().equalsIgnoreCase("Yes")){
						
							//Quotation Confirmation page
							
							//PrintTemplate.setTableHeading("Quotation Confirmation Page Data/Rates Validations");
							
							PrintTemplate.verifyTrue(true, activityDetails.isQuotationConfirmationPageLoaded(), "Quotation Confirmation Page loaded");
							
							if (activityDetails.isQuotationConfirmationPageLoaded() == true) {
								
								ArrayList<Activity_WEB_PaymentDetails> paymentInfoList = searchObject.getPaymentDetailsInfo();
								
								for (Activity_WEB_PaymentDetails paymentDetails : paymentInfoList) {
									String fullName = paymentDetails.getCustomerTitle() + paymentDetails.getCustomerName() + paymentDetails.getCustomerLastName();	
									PrintTemplate.verifyEqualIgnoreCase(fullName, activityDetails.getQuote_confirmation_BookingRefference().replaceAll(" ", ""), "Quotation Confirmation Page - Booking Reference");									
									PrintTemplate.verifyEqualIgnoreCase(paymentDetails.getEmail(), activityDetails.getQuote_confirmation_CusMailAddress(), "Quotation Confirmation Page - Customer Email");																
								}
								
								PrintTemplate.verifyContains(labelProperty.getPortalSpecificData().get("Portal.Tel"), activityDetails.getQuote_confirmation_PortalTel(), "Quotation Confirmation Page - Portal Tel");									
								PrintTemplate.verifyContains(labelProperty.getPortalSpecificData().get("Portal.Fax"), activityDetails.getQuote_confirmation_PortalFax(), "Quotation Confirmation Page - Portal Fax");									
								PrintTemplate.verifyContains(labelProperty.getPortalSpecificData().get("Portal.Website"), activityDetails.getQuote_confirmation_PortalEmail(), "Quotation Confirmation Page - Portal Web");									
								
								PrintTemplate.verifyEqualIgnoreCase(searchObject.getActivityName(), activityDetails.getQuote_confirmation_BI_ActivityName(), "Quotation Confirmation Page - Booking itinerary information - Activity Name");
								PrintTemplate.verifyEqualIgnoreCase(searchObject.getDestination().split("\\|")[1], activityDetails.getQuote_confirmation_BI_City(), "Quotation Confirmation Page - Booking itinerary information - City");			
								PrintTemplate.verifyEqualIgnoreCase(actInfo.getActivityType(), activityDetails.getQuote_confirmation_BI_ActType(), "Quotation Confirmation Page - Booking itinerary information - Activity Type");
								PrintTemplate.verifyEqualIgnoreCase("Quote", activityDetails.getQuote_confirmation_BI_BookingStatus(), "Quotation Confirmation Page - Booking itinerary information - Booking Status");
								
								for (int i = 0; i < activityDetails.getResultsPage_Period().size(); i++) {
									
									PrintTemplate.verifyEqualIgnoreCase(activityDetails.getResultsPage_RateType().get(i), activityDetails.getQuote_confirmation_BI_RateType(), "Quotation Confirmation Page - Booking itinerary information - Rate Type");	
									PrintTemplate.verifyEqualIgnoreCase(Integer.toString(activityDetails.getActivityRate()), activityDetails.getQuote_confirmation_BI_DailyRate(), "Quotation Confirmation Page - Booking itinerary information - Rate");
									PrintTemplate.verifyEqualIgnoreCase(Integer.toString(paxCnt), activityDetails.getQuote_confirmation_BI_QTY(), "Quotation Confirmation Page - Booking itinerary information - Qty");
									PrintTemplate.verifyEqualIgnoreCase(searchObject.getActivityDate(), activityDetails.getQuote_confirmation_BI_UsableOnDate(), "Quotation Confirmation Page - Booking itinerary information - Usable-On Date");
								}
								
								PrintTemplate.verifyTrue(searchObject.getSellingCurrency(), activityDetails.getQuote_confirmation_BI_Currency1(), "Quotation Confirmation Page - Booking itinerary information - Selling Currency 1");
								PrintTemplate.verifyTrue(searchObject.getSellingCurrency(), activityDetails.getQuote_confirmation_BI_Currency2(), "Quotation Confirmation Page - Booking itinerary information - Selling Currency 2");
								PrintTemplate.verifyTrue(searchObject.getSellingCurrency(), activityDetails.getQuote_confirmation_BI_Currency3(), "Quotation Confirmation Page - Booking itinerary information - Selling Currency 3");
								
								PrintTemplate.verifyTrue(Integer.toString(activityDetails.getSubTotalValue()), activityDetails.getQuote_confirmation_BI_ActiivtyRate(), "Quotation Confirmation Page - Booking itinerary information - Activity Rate");
														
								PrintTemplate.verifyTrue(Integer.toString(activityDetails.getSubTotalValue()), activityDetails.getQuote_confirmation_BI_SubTotal_1(), "Quotation Confirmation Page - Booking itinerary information - Sub Total");
								PrintTemplate.verifyTrue(Integer.toString(activityDetails.getTax()), activityDetails.getQuote_confirmation_BI_Tax(), "Quotation Confirmation Page - Booking itinerary information - Tax & Charges");
								PrintTemplate.verifyTrue(Integer.toString(activityDetails.getTotalWToutCCFee()), activityDetails.getQuote_confirmation_BI_Total(), "Quotation Confirmation Page - Booking itinerary information - Total");
														
								PrintTemplate.verifyTrue(Integer.toString(activityDetails.getSubTotalValue()), activityDetails.getQuote_confirmation_BI_SubTotal_2(), "Quotation Confirmation Page - Booking itinerary information - Sub Total");
								PrintTemplate.verifyTrue(Integer.toString(activityDetails.getTax()), activityDetails.getQuote_confirmation_BI_TotalTaxWTTax(), "Quotation Confirmation Page - Booking itinerary information - Total Taxes and Other Charges");
								PrintTemplate.verifyTrue(Integer.toString(activityDetails.getTotalWToutCCFee()), activityDetails.getQuote_confirmation_BI_TotalBooking(), "Quotation Confirmation Page - Booking itinerary information - Total Value");
								PrintTemplate.verifyTrue(Integer.toString(activityDetails.getAmountProcessedNow() - activityDetails.getCreditCardFee()), activityDetails.getQuote_confirmation_BI_AmountNow(), "Quotation Confirmation Page - Booking itinerary information - Amount being Processed Now");
								
								for (Activity_WEB_PaymentDetails paymentDetails : paymentInfoList) {
									
									PrintTemplate.verifyEqualIgnoreCase(paymentDetails.getCustomerName(), activityDetails.getQuote_confirmation_FName(), "Quotation Confirmation Page - Customer First Name");																
									PrintTemplate.verifyEqualIgnoreCase(paymentDetails.getCustomerLastName(), activityDetails.getQuote_confirmation_LName(), "Quotation Confirmation Page - Customer Last Name");																
									PrintTemplate.verifyEqualIgnoreCase(paymentDetails.getAddress(), activityDetails.getQuote_confirmation_address(), "Quotation Confirmation Page - Customer Address");																
									PrintTemplate.verifyEqualIgnoreCase(paymentDetails.getCity(), activityDetails.getQuote_confirmation_city(), "Quotation Confirmation Page - Customer City");																
									PrintTemplate.verifyEqualIgnoreCase(paymentDetails.getCountry(), activityDetails.getQuote_confirmation_country(), "Quotation Confirmation Page - Customer Country");																
									
									if (paymentDetails.getCountry().equalsIgnoreCase("USA") || paymentDetails.getCountry().equalsIgnoreCase("Canada") || paymentDetails.getCountry().equalsIgnoreCase("Australia")) {
										PrintTemplate.verifyEqualIgnoreCase(paymentDetails.getState(), activityDetails.getQuote_confirmation_State(), "Quotation Confirmation Page - Customer State");																			
									}								
									PrintTemplate.verifyEqualIgnoreCase(paymentDetails.getPostalCode(), activityDetails.getQuote_confirmation_postalCode(), "Quotation Confirmation Page - Customer Postal Code");																
									PrintTemplate.verifyEqualIgnoreCase(paymentDetails.getTel(), activityDetails.getQuote_confirmation_TP(), "Quotation Confirmation Page - Customer TP");																
									PrintTemplate.verifyEqualIgnoreCase(paymentDetails.getEmail(), activityDetails.getQuote_confirmation_Email(), "Quotation Confirmation Page - Customer Email");																								
								}
					
							}
							
						}else{
							
							//Confirmation page
							//PrintTemplate.setTableHeading("Confirmation Page Data/Rates Validations");
							
							PrintTemplate.verifyTrue(true, activityDetails.isConfirmationPageLoaded(), "Confirmation Page loaded");
							
							if (activityDetails.isConfirmationPageLoaded() == true) {
								
								ArrayList<Activity_WEB_PaymentDetails> paymentInfoList = searchObject.getPaymentDetailsInfo();
								
								for (Activity_WEB_PaymentDetails paymentDetails : paymentInfoList) {
									String fullName = paymentDetails.getCustomerTitle() + paymentDetails.getCustomerName() + paymentDetails.getCustomerLastName();	
									PrintTemplate.verifyEqualIgnoreCase(fullName, activityDetails.getConfirmation_BookingRefference().replaceAll(" ", ""), "Confirmation Page - Booking Reference");									
									PrintTemplate.verifyEqualIgnoreCase(paymentDetails.getEmail(), activityDetails.getConfirmation_CusMailAddress(), "Confirmation Page - Customer Email");																
								}
								
								PrintTemplate.verifyContains(labelProperty.getPortalSpecificData().get("Portal.Tel"), activityDetails.getConfirmation_PortalTel(), "Confirmation Page - Portal Tel");									
								PrintTemplate.verifyContains(labelProperty.getPortalSpecificData().get("Portal.Fax"), activityDetails.getConfirmation_PortalFax(), "Confirmation Page - Portal Fax");									
								PrintTemplate.verifyContains(labelProperty.getPortalSpecificData().get("Portal.Website"), activityDetails.getConfirmation_PortalEmail(), "Confirmation Page - Portal Web");									
								
								PrintTemplate.verifyEqualIgnoreCase(searchObject.getActivityName(), activityDetails.getConfirmation_BI_ActivityName(), "Confirmation Page - Booking itinerary information - Activity Name");
								PrintTemplate.verifyEqualIgnoreCase(searchObject.getDestination().split("\\|")[1], activityDetails.getConfirmation_BI_City(), "Confirmation Page - Booking itinerary information - City");			
								PrintTemplate.verifyEqualIgnoreCase(actInfo.getActivityType(), activityDetails.getConfirmation_BI_ActType(), "Confirmation Page - Booking itinerary information - Activity Type");
								
								if (actInfo.getInventoryType().equalsIgnoreCase("Allotments")) {
									PrintTemplate.verifyEqualIgnoreCase("Confirmed", activityDetails.getConfirmation_BI_BookingStatus(), "Confirmation Page - Booking itinerary information - Booking Status");
								} else {
									PrintTemplate.verifyEqualIgnoreCase("On-request", activityDetails.getConfirmation_BI_BookingStatus(), "Confirmation Page - Booking itinerary information - Booking Status");
								}
								
								for (int i = 0; i < activityDetails.getResultsPage_Period().size(); i++) {
									
									PrintTemplate.verifyEqualIgnoreCase(activityDetails.getResultsPage_RateType().get(i), activityDetails.getConfirmation_BI_RateType(), "Confirmation Page - Booking itinerary information - Rate Type");	
									PrintTemplate.verifyEqualIgnoreCase(Integer.toString(activityDetails.getActivityRate()), activityDetails.getConfirmation_BI_DailyRate(), "Confirmation Page - Booking itinerary information - Rate");
									PrintTemplate.verifyEqualIgnoreCase(Integer.toString(paxCnt), activityDetails.getConfirmation_BI_QTY(), "Confirmation Page - Booking itinerary information - Qty");
									PrintTemplate.verifyEqualIgnoreCase(searchObject.getActivityDate(), activityDetails.getConfirmation_BI_UsableOnDate(), "Confirmation Page - Booking itinerary information - Usable-On Date");
								}
								
								PrintTemplate.verifyTrue(searchObject.getSellingCurrency(), activityDetails.getConfirmation_BI_Currency1(), "Confirmation Page - Booking itinerary information - Selling Currency 1");
								PrintTemplate.verifyTrue(searchObject.getSellingCurrency(), activityDetails.getConfirmation_BI_Currency2(), "Confirmation Page - Booking itinerary information - Selling Currency 2");
								PrintTemplate.verifyTrue(searchObject.getSellingCurrency(), activityDetails.getConfirmation_BI_Currency3(), "Confirmation Page - Booking itinerary information - Selling Currency 3");
								
								PrintTemplate.verifyTrue(Integer.toString(activityDetails.getSubTotalValue()), activityDetails.getConfirmation_BI_ActiivtyRate(), "Confirmation Page - Booking itinerary information - Activity Rate");
														
								PrintTemplate.verifyTrue(Integer.toString(activityDetails.getSubTotalValue()), activityDetails.getConfirmation_BI_SubTotal_1(), "Confirmation Page - Booking itinerary information - Sub Total");
								PrintTemplate.verifyTrue(Integer.toString(activityDetails.getTax()), activityDetails.getConfirmation_BI_Tax(), "Confirmation Page - Booking itinerary information - Tax & Charges");
								PrintTemplate.verifyTrue(Integer.toString(activityDetails.getTotalWToutCCFee()), activityDetails.getConfirmation_BI_Total(), "Confirmation Page - Booking itinerary information - Total");
														
								PrintTemplate.verifyTrue(Integer.toString(activityDetails.getSubTotalValue()), activityDetails.getConfirmation_BI_SubTotal_2(), "Confirmation Page - Booking itinerary information - Sub Total");
								PrintTemplate.verifyTrue(Integer.toString(activityDetails.getTax()+activityDetails.getCreditCardFee()), activityDetails.getConfirmation_BI_TotalTaxWTTax(), "Confirmation Page - Booking itinerary information - Total Taxes and Other Charges");
								PrintTemplate.verifyTrue(Integer.toString(activityDetails.getTotalBookingValue()), activityDetails.getConfirmation_BI_TotalBooking(), "Confirmation Page - Booking itinerary information - Total Value");
								PrintTemplate.verifyTrue(Integer.toString(activityDetails.getAmountProcessedNow()), activityDetails.getConfirmation_BI_AmountNow(), "Confirmation Page - Booking itinerary information - Amount being Processed Now");
								PrintTemplate.verifyTrue(Integer.toString(activityDetails.getAmountDue()), activityDetails.getConfirmation_BI_AmountDue(), "Confirmation Page - Booking itinerary information - Amount Due at Check-in / Utilization");
															
								for (Activity_WEB_PaymentDetails paymentDetails : paymentInfoList) {
									
									PrintTemplate.verifyEqualIgnoreCase(paymentDetails.getCustomerName(), activityDetails.getConfirmation_FName(), "Confirmation Page - Customer First Name");																
									PrintTemplate.verifyEqualIgnoreCase(paymentDetails.getCustomerLastName(), activityDetails.getConfirmation_LName(), "Confirmation Page - Customer Last Name");																
									PrintTemplate.verifyEqualIgnoreCase(paymentDetails.getAddress(), activityDetails.getConfirmation_address(), "Confirmation Page - Customer Address");																
									PrintTemplate.verifyEqualIgnoreCase(paymentDetails.getCity(), activityDetails.getConfirmation_city(), "Confirmation Page - Customer City");																
									PrintTemplate.verifyEqualIgnoreCase(paymentDetails.getCountry(), activityDetails.getConfirmation_country(), "Confirmation Page - Customer Country");																
									
									if (paymentDetails.getCountry().equalsIgnoreCase("USA") || paymentDetails.getCountry().equalsIgnoreCase("Canada") || paymentDetails.getCountry().equalsIgnoreCase("Australia")) {
										PrintTemplate.verifyEqualIgnoreCase(paymentDetails.getState(), activityDetails.getConfirmation_State(), "Confirmation Page - Customer State");																			
									}								
									PrintTemplate.verifyEqualIgnoreCase(paymentDetails.getPostalCode(), activityDetails.getConfirmation_postalCode(), "Confirmation Page - Customer Postal Code");																
									PrintTemplate.verifyEqualIgnoreCase(paymentDetails.getTel(), activityDetails.getConfirmation_TP(), "Confirmation Page - Customer TP");																
									PrintTemplate.verifyEqualIgnoreCase(paymentDetails.getEmail(), activityDetails.getConfirmation_Email(), "Confirmation Page - Customer Email");																								
								}
								
								PrintTemplate.verifyEqualIgnoreCase(activityDetails.getPaymentsPage_OD_FirstName(), activityDetails.getConfirmationPage_cusFName(), "Confirmation Page - Activity Occupancy Details - Customer First");																
								PrintTemplate.verifyEqualIgnoreCase(activityDetails.getPaymentsPage_OD_LastName(), activityDetails.getConfirmationPage_cusLName(), "Confirmation Page - Activity Occupancy Details - Customer Last");																
								
								PrintTemplate.verifyEqualIgnoreCase(activityDetails.getCancellationDeadline(), activityDetails.getConfirmation_Cancel_CancellationDead_1(), "Confirmation Page - Cancellation Deadline 1");									
								PrintTemplate.verifyEqualIgnoreCase(activityDetails.getCancellationDeadline(), activityDetails.getConfirmation_Cancel_CancellationDead_2(), "Confirmation Page - Cancellation Deadline 2");									
								
								for (int i = 0; i < activityDetails.getConfirmation_Cancel_CancelPolicy().size(); i++) {
									PrintTemplate.verifyEqualIgnoreCase(activityDetails.getCancellationPoliciesList().get(i), activityDetails.getConfirmation_Cancel_CancelPolicy().get(i), "Confirmation Page - Cancellation policy:- "+i+"");																		
								}
								
							}
							
						}
						
						//PrintTemplate.markTableEnd();
						
					}				
				}
				
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}

	
	public void getBookingListReportCriteriaLabelAvailability(CC_Com_BookingListReport_criteria bookingListCriteria){
		
		try {
			
			HashMap<String, Boolean> blAvailabiltiy = bookingListCriteria.getLabelAvailability();
			
			PrintTemplate.verifyTrue(true, blAvailabiltiy.get("partner"), 						"Booking list report - Search criteria label availability - Partner");
			PrintTemplate.verifyTrue(true, blAvailabiltiy.get("partnerAll"), 					"Booking list report - Search criteria label availability - Partner - All");
			PrintTemplate.verifyTrue(true, blAvailabiltiy.get("partnerDc"), 					"Booking list report - Search criteria label availability - Partner - DC");
			PrintTemplate.verifyTrue(true, blAvailabiltiy.get("partnerb2b"), 					"Booking list report - Search criteria label availability - Partner - B2B");
			PrintTemplate.verifyTrue(true, blAvailabiltiy.get("partnerAffiliate"), 				"Booking list report - Search criteria label availability - Partner - Affiliate");
			PrintTemplate.verifyTrue(true, blAvailabiltiy.get("posLocation"), 					"Booking list report - Search criteria label availability - Pos location");
			PrintTemplate.verifyTrue(true, blAvailabiltiy.get("partnerBookingNo"), 				"Booking list report - Search criteria label availability - Partner booking number");
			PrintTemplate.verifyTrue(true, blAvailabiltiy.get("consultantName"), 				"Booking list report - Search criteria label availability - Consultant name");
			PrintTemplate.verifyTrue(true, blAvailabiltiy.get("productType"), 					"Booking list report - Search criteria label availability - Product type");
			PrintTemplate.verifyTrue(true, blAvailabiltiy.get("productTypeAll"), 				"Booking list report - Search criteria label availability - Product type - All");
			PrintTemplate.verifyTrue(true, blAvailabiltiy.get("productTypeActivities"), 		"Booking list report - Search criteria label availability - Product type - Activities");
			PrintTemplate.verifyTrue(true, blAvailabiltiy.get("productTypeHotels"), 			"Booking list report - Search criteria label availability - Product type - Hotels");
			PrintTemplate.verifyTrue(true, blAvailabiltiy.get("productTypeAir"), 				"Booking list report - Search criteria label availability - Product type - Air");
			PrintTemplate.verifyTrue(true, blAvailabiltiy.get("productTypeCar"), 				"Booking list report - Search criteria label availability - Product type - Car");
			PrintTemplate.verifyTrue(true, blAvailabiltiy.get("productTypePackaging"), 			"Booking list report - Search criteria label availability - Product type - Packaging");
			PrintTemplate.verifyTrue(true, blAvailabiltiy.get("firstAndLastName"), 				"Booking list report - Search criteria label availability - First and last name");
			PrintTemplate.verifyTrue(true, blAvailabiltiy.get("bookingStatus"), 				"Booking list report - Search criteria label availability - Booking status");
			PrintTemplate.verifyTrue(true, blAvailabiltiy.get("bookingStatusAll"), 				"Booking list report - Search criteria label availability - Booking status - All");
			PrintTemplate.verifyTrue(true, blAvailabiltiy.get("bookingStatusConfirmed"), 		"Booking list report - Search criteria label availability - Booking status - Confirmed");
			PrintTemplate.verifyTrue(true, blAvailabiltiy.get("bookingStatusOnReq"), 			"Booking list report - Search criteria label availability - Booking status - On request");
			PrintTemplate.verifyTrue(true, blAvailabiltiy.get("bookingStatusCancelled"), 		"Booking list report - Search criteria label availability - Booking status - Cancelled");
			PrintTemplate.verifyTrue(true, blAvailabiltiy.get("bookingStatusModified"), 		"Booking list report - Search criteria label availability - Booking status - Modified");
			PrintTemplate.verifyTrue(true, blAvailabiltiy.get("bookingStatusWithFailedSector"), "Booking list report - Search criteria label availability - Booking status - With fail sector");
			PrintTemplate.verifyTrue(true, blAvailabiltiy.get("dateFilter"), 					"Booking list report - Search criteria label availability - Date filter");
			PrintTemplate.verifyTrue(true, blAvailabiltiy.get("reportFrom"), 					"Booking list report - Search criteria label availability - Report from");
			PrintTemplate.verifyTrue(true, blAvailabiltiy.get("reportTo"), 						"Booking list report - Search criteria label availability - Report to");
			PrintTemplate.verifyTrue(true, blAvailabiltiy.get("datesBasedOn"), 					"Booking list report - Search criteria label availability - Dates based on");
			PrintTemplate.verifyTrue(true, blAvailabiltiy.get("datesBasedOnReservation"), 		"Booking list report - Search criteria label availability - Dates based on - Reservation");
			PrintTemplate.verifyTrue(true, blAvailabiltiy.get("datesBasedOnCancellation"), 		"Booking list report - Search criteria label availability - Dates based on - Cancellation");
			PrintTemplate.verifyTrue(true, blAvailabiltiy.get("datesBasedOnArrival"), 			"Booking list report - Search criteria label availability - Dates based on - Arrival");
			PrintTemplate.verifyTrue(true, blAvailabiltiy.get("datesBasedOnDeparture"), 		"Booking list report - Search criteria label availability - Dates based on - Departure");
		
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	
	
	public void getBookingListReportCriteriaLabelVerification(CC_Com_BookingListReport_criteria bookingListCriteria, HashMap<String, String> commonLabels){

		try {
			
			HashMap<String, String> report = bookingListCriteria.getLabels();
			
			PrintTemplate.verifyTrue(commonLabels.get("bookingListCriteriapartner"), 							report.get("partner"), 						"Booking list report - Search criteria label verification - Partner");
			PrintTemplate.verifyTrue(commonLabels.get("bookingListCriteriaPartnerAll"), 						report.get("partnerAll"), 					"Booking list report - Search criteria label verification - Partner - All");
			PrintTemplate.verifyTrue(commonLabels.get("bookingListCriteriaPartnerDC"), 						report.get("partnerDc"), 					"Booking list report - Search criteria label verification - Partner - DC");
			PrintTemplate.verifyTrue(commonLabels.get("bookingListCriteriapartnerB2B"), 						report.get("partnerb2b"), 					"Booking list report - Search criteria label verification - Partner - B2B");
			PrintTemplate.verifyTrue(commonLabels.get("bookingListCriteriapartnerAffiliate"), 					report.get("partnerAffiliate"), 			"Booking list report - Search criteria label verification - Partner - Affiliate");
			PrintTemplate.verifyTrue(commonLabels.get("bookingListCriteriaPosLocationName"), 					report.get("posLocation"), 					"Booking list report - Search criteria label verification - Pos location");
			PrintTemplate.verifyTrue(commonLabels.get("bookingListCriteriaBookingNo"), 						report.get("partnerBookingNo"), 			"Booking list report - Search criteria label verification - Partner booking number");
			PrintTemplate.verifyTrue(commonLabels.get("bookingListCriteriaConsultantName"), 					report.get("consultantName"), 				"Booking list report - Search criteria label verification - Consultant name");
			PrintTemplate.verifyTrue(commonLabels.get("bookingListCriteriaProductType"), 						report.get("productType"), 					"Booking list report - Search criteria label verification - Product type");
			PrintTemplate.verifyTrue(commonLabels.get("bookingListCriteriaProductTypeAll"), 					report.get("productTypeAll"), 				"Booking list report - Search criteria label verification - Product type - All");
			PrintTemplate.verifyTrue(commonLabels.get("bookingListCriteriaProductTypeActivities"), 			report.get("productTypeActivities"), 		"Booking list report - Search criteria label verification - Product type - Activities");
			PrintTemplate.verifyTrue(commonLabels.get("bookingListCriteriaProductTypeHotels"), 				report.get("productTypeHotels"), 			"Booking list report - Search criteria label verification - Product type - Hotels");
			PrintTemplate.verifyTrue(commonLabels.get("bookingListCriteriaProductTypeAir"), 					report.get("productTypeAir"), 				"Booking list report - Search criteria label verification - Product type - Air");
			PrintTemplate.verifyTrue(commonLabels.get("bookingListCriteriaProductTypeCar"), 					report.get("productTypeCar"), 				"Booking list report - Search criteria label verification - Product type - Car");
			PrintTemplate.verifyTrue(commonLabels.get("bookingListCriteriaProductTypePackaging"), 				report.get("productTypePackaging"), 		"Booking list report - Search criteria label verification - Product type - Packaging");
			PrintTemplate.verifyTrue(commonLabels.get("bookingListCriteriaFirstAndLastName"),				 	report.get("firstAndLastName"), 			"Booking list report - Search criteria label verification - First and lact name");
			PrintTemplate.verifyTrue(commonLabels.get("bookingListCriteriaBookingStatus"), 					report.get("bookingStatus"), 				"Booking list report - Search criteria label verification - Booking status");
			PrintTemplate.verifyTrue(commonLabels.get("bookingListCriteriaBookingStatusAll"), 					report.get("bookingStatusAll"), 			"Booking list report - Search criteria label verification - Booking status - All");
			PrintTemplate.verifyTrue(commonLabels.get("bookingListCriteriaBookingStatusConfirmed"), 			report.get("bookingStatusConfirmed"), 		"Booking list report - Search criteria label verification - Booking status - Confirmed");
			PrintTemplate.verifyTrue(commonLabels.get("bookingListCriteriaBookingStatusOnReq"), 				report.get("bookingStatusOnReq"), 			"Booking list report - Search criteria label verification - Booking status - On request");
			PrintTemplate.verifyTrue(commonLabels.get("bookingListCriteriaBookingStatusCancelled"), 			report.get("bookingStatusCancelled"), 		"Booking list report - Search criteria label verification - Booking status - Cancelled");
			PrintTemplate.verifyTrue(commonLabels.get("bookingListCriteriaBookingStatusModified"), 			report.get("bookingStatusModified"), 		"Booking list report - Search criteria label verification - Booking status - Modified");
			PrintTemplate.verifyTrue(commonLabels.get("bookingListCriteriaBookingStatusWithFailedSector"), 	report.get("bookingStatusWithFailedSector"),"Booking list report - Search criteria label verification - Booking status - With failed sector");
			PrintTemplate.verifyTrue(commonLabels.get("bookingListCriteriaDateFilter"), 						report.get("dateFilter"), 					"Booking list report - Search criteria label verification - Date filter");
			PrintTemplate.verifyTrue(commonLabels.get("bookingListCriteriaReportFrom"), 						report.get("reportFrom"), 					"Booking list report - Search criteria label verification - Report from");
			PrintTemplate.verifyTrue(commonLabels.get("bookingListCriteriaReportTo"), 							report.get("reportTo"), 					"Booking list report - Search criteria label verification - Report to");
			PrintTemplate.verifyTrue(commonLabels.get("bookingListCriteriaDatesBasedOn"), 						report.get("datesBasedOn"), 				"Booking list report - Search criteria label verification - Dates based on");
			PrintTemplate.verifyTrue(commonLabels.get("bookingListCriteriaDatesBasedOnReservation"), 			report.get("datesBasedOnReservation"), 		"Booking list report - Search criteria label verification - Dates based on - Reservation");
			PrintTemplate.verifyTrue(commonLabels.get("bookingListCriteriaDatesBasedOnCancellation"), 			report.get("datesBasedOnCancellation"), 	"Booking list report - Search criteria label verification - Dates based on - candellatiom");
			PrintTemplate.verifyTrue(commonLabels.get("bookingListCriteriaDatesBasedOnArrival"), 				report.get("datesBasedOnArrival"), 			"Booking list report - Search criteria label verification - Dates based on - Arrival");
			PrintTemplate.verifyTrue(commonLabels.get("bookingListCriteriaDatesBasedOnDeparture"), 			report.get("datesBasedOnDeparture"), 		"Booking list report - Search criteria label verification - Dates based on - Depature");
		
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	public void getBookingListReportHeadLabelsLoaded(CC_Com_BookingList_Report bookingList){
		
		try {
			
			HashMap<String, Boolean> labels = bookingList.getHotelLabelsAvailability();
			
			PrintTemplate.verifyTrue(true, labels.get("number"), 				"Booking list report - Label availability - Number");
			PrintTemplate.verifyTrue(true, labels.get("reservations"), 			"Booking list report - Label availability - Reservations");
			PrintTemplate.verifyTrue(true, labels.get("customer"), 				"Booking list report - Label availability - Customer");
			PrintTemplate.verifyTrue(true, labels.get("leadGuest"), 			"Booking list report - Label availability - Lead guest");
			PrintTemplate.verifyTrue(true, labels.get("dateOf"), 				"Booking list report - Label availability - Date of");
			PrintTemplate.verifyTrue(true, labels.get("date"), 					"Booking list report - Label availability - Date");
			PrintTemplate.verifyTrue(true, labels.get("time"), 					"Booking list report - Label availability - Time");
			PrintTemplate.verifyTrue(true, labels.get("reservedBy"), 			"Booking list report - Label availability - Reserved by");
			PrintTemplate.verifyTrue(true, labels.get("paymentType"), 			"Booking list report - Label availability - Payment type");
			PrintTemplate.verifyTrue(true, labels.get("profitMarkUpType"), 		"Booking list report - Label availability - Profit mark up type");
			PrintTemplate.verifyTrue(true, labels.get("customerName"), 			"Booking list report - Label availability - Customer name");
			PrintTemplate.verifyTrue(true, labels.get("leadGuestName"), 		"Booking list report - Label availability - Lead guest name");
			PrintTemplate.verifyTrue(true, labels.get("firstElement"), 			"Booking list report - Label availability - First element");
			PrintTemplate.verifyTrue(true, labels.get("cancellationDeadline"), 	"Booking list report - Label availability - Cancellation deadline");
			PrintTemplate.verifyTrue(true, labels.get("cityCountryName"), 		"Booking list report - Label availability - City country name");
			PrintTemplate.verifyTrue(true, labels.get("product"), 				"Booking list report - Label availability - Product");
			PrintTemplate.verifyTrue(true, labels.get("verified"), 				"Booking list report - Label availability - Verified");
			PrintTemplate.verifyTrue(true, labels.get("status"), 				"Booking list report - Label availability - Status");
			PrintTemplate.verifyTrue(true, labels.get("notifications"), 		"Booking list report - Label availability - Notifications");
			PrintTemplate.verifyTrue(true, labels.get("edit"), 					"Booking list report - Label availability - Edit");
			PrintTemplate.verifyTrue(true, labels.get("shoppingCartCode"), 		"Booking list report - Label availability - Shpping cart code");
			PrintTemplate.verifyTrue(true, labels.get("shoppingCart"), 			"Booking list report - Label availability - Shopping cart");
			PrintTemplate.verifyTrue(true, labels.get("dynamicPackageCode"), 	"Booking list report - Label availability - Dynamic package code");
			PrintTemplate.verifyTrue(true, labels.get("dynamicPackage"), 		"Booking list report - Label availability - Dynamic package");
			PrintTemplate.verifyTrue(true, labels.get("fixedPackageCode"), 		"Booking list report - Label availability - Fixed package code");
			PrintTemplate.verifyTrue(true, labels.get("fixedPackages"), 		"Booking list report - Label availability - Fixed package");
			PrintTemplate.verifyTrue(true, labels.get("dmcCode"), 				"Booking list report - Label availability - Dmc code");
			PrintTemplate.verifyTrue(true, labels.get("dmc"), 					"Booking list report - Label availability - DMc");
			PrintTemplate.verifyTrue(true, labels.get("invoice"), 				"Booking list report - Label availability - Invoice");
			PrintTemplate.verifyTrue(true, labels.get("invoiceIssued"), 		"Booking list report - Label availability - Invoice issued");
			PrintTemplate.verifyTrue(true, labels.get("payment"), 				"Booking list report - Label availability - Payment");
			PrintTemplate.verifyTrue(true, labels.get("paymentReceived"), 		"Booking list report - Label availability - Payment received");
			PrintTemplate.verifyTrue(true, labels.get("voucher"), 				"Booking list report - Label availability - Voucher");
			PrintTemplate.verifyTrue(true, labels.get("voucherIssued"), 		"Booking list report - Label availability - Voucher issued");
			PrintTemplate.verifyTrue(true, labels.get("ticket"), 				"Booking list report - Label availability - Ticket");
			PrintTemplate.verifyTrue(true, labels.get("ticketIssued"), 			"Booking list report - Label availability - Ticket issued");
			PrintTemplate.verifyTrue(true, labels.get("emd"), 					"Booking list report - Label availability - EMD");
			PrintTemplate.verifyTrue(true, labels.get("emdIssued"), 			"Booking list report - Label availability - EMD issued");
			PrintTemplate.verifyTrue(true, labels.get("failedSectorImage"), 	"Booking list report - Label availability - Failed sector Image");
			PrintTemplate.verifyTrue(true, labels.get("failedSector"), 			"Booking list report - Label availability - Failed sector");
					
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	
	
	public void getBookingListReportLabelVerifications(CC_Com_BookingList_Report bookingList, HashMap<String, String> commonLabels){
		
		try {
			
			HashMap<String, String> labels = bookingList.getHotelLabels();
			
			PrintTemplate.verifyTrue(commonLabels.get("bookingListReportReservationNumber"), 		labels.get("number"), 					"Booking list report - Label verification - Reservation number");
			PrintTemplate.verifyTrue(commonLabels.get("bookingListReportReservationDate"), 		labels.get("date"), 					"Booking list report - Label verification - Reservation date");
			PrintTemplate.verifyTrue(commonLabels.get("bookingListReportReservationTime"), 		labels.get("time"), 					"Booking list report - Label verification - Reservation time");
			PrintTemplate.verifyTrue(commonLabels.get("bookingListReportReservedBy"), 				labels.get("reservedBy"), 				"Booking list report - Label verification - Reserved by");
			PrintTemplate.verifyTrue(commonLabels.get("bookingListReportPaymentType"), 			labels.get("paymentType"), 				"Booking list report - Label verification - Payment type");
			PrintTemplate.verifyTrue(commonLabels.get("bookingListReportPMType"), 					labels.get("profitMarkUpType"), 		"Booking list report - Label verification - Profit mark up type");
			PrintTemplate.verifyTrue(commonLabels.get("bookingListReportCustomerName"), 			labels.get("customerName"), 			"Booking list report - Label verification - Customer name");
			PrintTemplate.verifyTrue(commonLabels.get("bookingListReportLeadGuestName"), 			labels.get("leadGuestName"), 			"Booking list report - Label verification - Lead guest name");
			PrintTemplate.verifyTrue(commonLabels.get("bookingListReportFirstElement"), 			labels.get("firstElement"), 			"Booking list report - Label verification - First element");
			PrintTemplate.verifyTrue(commonLabels.get("bookingListReportCancellationDeadline"), 	labels.get("cancellationDeadline"), 	"Booking list report - Label verification - Cancellation deadline");
			PrintTemplate.verifyTrue(commonLabels.get("bookingListReportCityCountry"), 			labels.get("cityCountryName"), 			"Booking list report - Label verification - City/Country name");
			PrintTemplate.verifyTrue(commonLabels.get("bookingListReportProduct"), 				labels.get("product"), 					"Booking list report - Label verification - Product");
			PrintTemplate.verifyTrue(commonLabels.get("bookingListReportVerified"), 				labels.get("verified"), 				"Booking list report - Label verification - Verified");
			PrintTemplate.verifyTrue(commonLabels.get("bookingListReportStatus"), 					labels.get("status"), 					"Booking list report - Label verification - Status");
			PrintTemplate.verifyTrue(commonLabels.get("bookingListReportNotifications"), 			labels.get("notifications"), 			"Booking list report - Label verification - Notifications");
			//PrintTemplate.verifyTrue(commonLabels.get("bookingListReportview"), 					labels.get(""), 						"Booking list report - Label verification - ");
			PrintTemplate.verifyTrue(commonLabels.get("bookingListReportEdit"), 					labels.get("edit"), 					"Booking list report - Label verification - Edit");
			PrintTemplate.verifyTrue(commonLabels.get("bookingListReportReservation"), 			labels.get("reservations"), 			"Booking list report - Label verification - Reservations");
			PrintTemplate.verifyTrue(commonLabels.get("bookingListReportCustomer"), 				labels.get("customer"), 				"Booking list report - Label verification - Customer");
			PrintTemplate.verifyTrue(commonLabels.get("bookingListReportLeadGuest"), 				labels.get("leadGuest"), 				"Booking list report - Label verification - Lead guest");
			PrintTemplate.verifyTrue(commonLabels.get("bookingListReportDateOf"), 					labels.get("dateOf"), 					"Booking list report - Label verification - Date of");
			PrintTemplate.verifyTrue(commonLabels.get("bookingListReportSC"), 						labels.get("shoppingCartCode"), 		"Booking list report - Label verification - Shopping cart code");
			PrintTemplate.verifyTrue(commonLabels.get("bookingListReportShoppingCart"), 			labels.get("shoppingCart"), 			"Booking list report - Label verification - Shopping cart");
			PrintTemplate.verifyTrue(commonLabels.get("bookingListReportDP"), 						labels.get("dynamicPackageCode"), 		"Booking list report - Label verification - Dynamic package code");
			PrintTemplate.verifyTrue(commonLabels.get("bookingListReportDynamicPackage"), 			labels.get("dynamicPackage"), 			"Booking list report - Label verification - Dynamic package");
			PrintTemplate.verifyTrue(commonLabels.get("bookingListReportFP"), 						labels.get("fixedPackageCode"), 		"Booking list report - Label verification - Fixed package code");
			PrintTemplate.verifyTrue(commonLabels.get("bookingListReportFixedPackage"), 			labels.get("fixedPackages"), 			"Booking list report - Label verification - Fixed packages");
			PrintTemplate.verifyTrue(commonLabels.get("bookingListReportDMCP"), 					labels.get("dmcCode"), 					"Booking list report - Label verification - DMC code");
			PrintTemplate.verifyTrue(commonLabels.get("bookingListReportDMCPackage"), 				labels.get("dmc"), 						"Booking list report - Label verification - DMC");
			PrintTemplate.verifyTrue(commonLabels.get("bookingListReportInvoiceIssued"), 			labels.get("invoiceIssued"), 			"Booking list report - Label verification - Invoice issued");
			PrintTemplate.verifyTrue(commonLabels.get("bookingListReportPaymentRecieved"), 		labels.get("paymentReceived"), 			"Booking list report - Label verification - Payment received");
			PrintTemplate.verifyTrue(commonLabels.get("bookingListReportVoucherIssued"), 			labels.get("voucherIssued"), 			"Booking list report - Label verification - Voucher issued");
			PrintTemplate.verifyTrue(commonLabels.get("bookingListReportTicketIssued"), 			labels.get("ticketIssued"), 			"Booking list report - Label verification - Ticket issued");
			PrintTemplate.verifyTrue(commonLabels.get("bookingListReportEMDIssued"), 				labels.get("emdIssued"), 				"Booking list report - Label verification - EMD issued");
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	
	
	public void getBookingListReportValidations(CC_Com_BookingList_Report bookingList, ActivityDetails activityDetails){
		
		try {
			
			HashMap<String, String> values = bookingList.getActivityValues();
			
			PrintTemplate.verifyTrue(activityDetails.getReservationDateToday(), values.get("reservationDate"), "Booking List Report - Reservation Date");
			PrintTemplate.verifyTrue(searchObject.getActivityDate(), values.get("firstElement"), "Booking List Report - First Element Date");
			PrintTemplate.verifyTrue(activityDetails.getCancellationDeadline(), values.get("cancellationDeadline"), "Booking List Report - Cancellation Deadline Date");
			PrintTemplate.verifyTrue(searchObject.getDestination().split("\\|")[1] + "/" + searchObject.getDestination().split("\\|")[5], values.get("cityCountry"), "Booking List Report - City/Country");
			
			boolean bookingCon = false;
			System.out.println(values.get("status"));
			if(values.get("status").replaceAll(" ", "").equalsIgnoreCase("CON")){
				bookingCon = true;
			}
		
			PrintTemplate.verifyTrue(true, bookingCon, "Booking List Report - Booking Confirmation");
			
			boolean invoiceIssue = false;
			if(values.get("invoice").equalsIgnoreCase("Invoice issued")){
				invoiceIssue = true;
			}
			boolean paymentIssue = false;
			if(values.get("payment").equalsIgnoreCase("Payment done")){
				paymentIssue = true;
			}
			boolean voucherIssue = false;
			if(values.get("voucher").equalsIgnoreCase("Voucher issued")){
				voucherIssue = true;
			}
			
			PrintTemplate.verifyTrue(true, invoiceIssue, "Booking List Report - Invoice issued");
			PrintTemplate.verifyTrue(true, paymentIssue, "Booking List Report - Payment done");
			PrintTemplate.verifyTrue(true, voucherIssue, "Booking List Report - Voucher issued");
					
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	public void getBookingList_BookingCardLabelAvailability(CC_Com_BookingListReport_BookingCard bookingList_BookingCard){
		
		try {
			
			HashMap<String, Boolean> availability = bookingList_BookingCard.getActivityLabelAvailability();
			
			PrintTemplate.verifyTrue(true, availability.get("reservationSummary"), 					"booking list - Booking card - Label availability - Reservation summary");
			PrintTemplate.verifyTrue(true, availability.get("reservationNumber"), 					"booking list - Booking card - Label availability - Reservation number");
			PrintTemplate.verifyTrue(true, availability.get("originCityCountry"), 					"booking list - Booking card - Label availability - Origin city / country");
			PrintTemplate.verifyTrue(true, availability.get("destinationCityCountry"), 				"booking list - Booking card - Label availability - Destination city / country");
			PrintTemplate.verifyTrue(true, availability.get("productsBooked"), 						"booking list - Booking card - Label availability - Products booked");
			PrintTemplate.verifyTrue(true, availability.get("bookingStatus"), 						"booking list - Booking card - Label availability - Booking status");
			PrintTemplate.verifyTrue(true, availability.get("failedSectorIncluded"), 				"booking list - Booking card - Label availability - Failed sector included");
			PrintTemplate.verifyTrue(true, availability.get("paymentLabel"), 						"booking list - Booking card - Label availability - Payment label");
			PrintTemplate.verifyTrue(true, availability.get("paymentReference"), 					"booking list - Booking card - Label availability - Payment reference");
			PrintTemplate.verifyTrue(true, availability.get("reservationDate"), 					"booking list - Booking card - Label availability - Reservation date");
			PrintTemplate.verifyTrue(true, availability.get("firstElement"), 						"booking list - Booking card - Label availability - First element");
			PrintTemplate.verifyTrue(true, availability.get("cancellationDeadline"), 				"booking list - Booking card - Label availability - Cancellation deadline");
			PrintTemplate.verifyTrue(true, availability.get("amountChergedByPG"), 					"booking list - Booking card - Label availability - Amount charged by payment gateway");
			PrintTemplate.verifyTrue(true, availability.get("subTotal"), 							"booking list - Booking card - Label availability - Sub total");
			PrintTemplate.verifyTrue(true, availability.get("bookingFee"), 							"booking list - Booking card - Label availability - Booking fee");
			PrintTemplate.verifyTrue(true, availability.get("taxAndOtherCharges"), 					"booking list - Booking card - Label availability - Tax and other charges");
			PrintTemplate.verifyTrue(true, availability.get("ccFee"), 								"booking list - Booking card - Label availability - Credit card fee");
			PrintTemplate.verifyTrue(true, availability.get("totalBookingValue"), 					"booking list - Booking card - Label availability - Total booking value");
			PrintTemplate.verifyTrue(true, availability.get("amountPayableUpfront"), 				"booking list - Booking card - Label availability - Amount payable upfront");
			PrintTemplate.verifyTrue(true, availability.get("totalAmountPAyableAmountAtCheckin"), 	"booking list - Booking card - Label availability - Total amount payable at chekin");
			PrintTemplate.verifyTrue(true, availability.get("amountPaid"), 							"booking list - Booking card - Label availability - Amount paid");
			PrintTemplate.verifyTrue(true, availability.get("customerLeadGuestDetails"), 			"booking list - Booking card - Label availability - Customer lead guest details");
			PrintTemplate.verifyTrue(true, availability.get("firstName"), 							"booking list - Booking card - Label availability - First name");
			PrintTemplate.verifyTrue(true, availability.get("address"), 							"booking list - Booking card - Label availability - Address");
			PrintTemplate.verifyTrue(true, availability.get("emailAddress"), 						"booking list - Booking card - Label availability - Email");
			PrintTemplate.verifyTrue(true, availability.get("city"), 								"booking list - Booking card - Label availability - City");
			PrintTemplate.verifyTrue(true, availability.get("countryLabel"), 						"booking list - Booking card - Label availability - Country");
			PrintTemplate.verifyTrue(true, availability.get("emergencyContactNumber"), 				"booking list - Booking card - Label availability - Emergency contact number");
			PrintTemplate.verifyTrue(true, availability.get("phoneNumber"), 						"booking list - Booking card - Label availability - Phone number");
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	
	public void getBookingList_BookingCardLabelVerifications(CC_Com_BookingListReport_BookingCard bookingList_BookingCard, HashMap<String, String> commonLabels){
		
		try {
			
			HashMap<String, String> labels = bookingList_BookingCard.getActivityLabels();
			
			PrintTemplate.verifyTrue(commonLabels.get("reservationSummary"), 			labels.get("reservationSummary"), 					"booking list - Booking card - Label verification - Reservation summary");
			PrintTemplate.verifyTrue(commonLabels.get("resNumber"), 					labels.get("reservationNumber"), 					"booking list - Booking card - Label verification - Reservation number");
			PrintTemplate.verifyTrue(commonLabels.get("originCity"), 					labels.get("originCityCountry"), 					"booking list - Booking card - Label verification - Origin city / country");
			PrintTemplate.verifyTrue(commonLabels.get("destinationCity"), 				labels.get("destinationCityCountry"), 				"booking list - Booking card - Label verification - Destination city / country");
			PrintTemplate.verifyTrue(commonLabels.get("productsBooked"), 				labels.get("productsBooked"), 						"booking list - Booking card - Label verification - Products booked");
			PrintTemplate.verifyTrue(commonLabels.get("bookingStatus"), 				labels.get("bookingStatus"), 						"booking list - Booking card - Label verification - Booking status");
			PrintTemplate.verifyTrue(commonLabels.get("FailedSectorIncluded"), 			labels.get("failedSectorIncluded"), 				"booking list - Booking card - Label verification - Failed sector included");
			PrintTemplate.verifyTrue(commonLabels.get("paymentMethod"), 				labels.get("paymentLabel"), 						"booking list - Booking card - Label verification - Payment label");
			PrintTemplate.verifyTrue(commonLabels.get("paymentReference"), 				labels.get("paymentReference"), 					"booking list - Booking card - Label verification - Payment reference");
			PrintTemplate.verifyTrue(commonLabels.get("reservationDate"), 				labels.get("reservationDate"), 						"booking list - Booking card - Label verification - Reservation date");
			PrintTemplate.verifyTrue(commonLabels.get("firstElement"), 					labels.get("firstElement"), 						"booking list - Booking card - Label verification - First element");
			PrintTemplate.verifyTrue(commonLabels.get("cancellationDeadline"), 			labels.get("cancellationDeadline"), 				"booking list - Booking card - Label verification - Cancellation deadline");
			PrintTemplate.verifyTrue(commonLabels.get("amountChargedByPG"), 			labels.get("amountChergedByPG"), 					"booking list - Booking card - Label verification - Amount charged by payment gateway");
			PrintTemplate.verifyTrue(commonLabels.get("subtotal"), 						labels.get("subTotal"), 							"booking list - Booking card - Label verification - Sub total");
			PrintTemplate.verifyTrue(commonLabels.get("bookingFee"), 					labels.get("bookingFee"), 							"booking list - Booking card - Label verification - Booking fee");
			PrintTemplate.verifyTrue(commonLabels.get("taxAndOtherCharges"), 			labels.get("taxAndOtherCharges"), 					"booking list - Booking card - Label verification - Tax and other charges");
			PrintTemplate.verifyTrue(commonLabels.get("ccFee"), 						labels.get("ccFee"), 								"booking list - Booking card - Label verification - Credit card fee");
			PrintTemplate.verifyTrue(commonLabels.get("totalBookingValue"), 			labels.get("totalBookingValue"), 					"booking list - Booking card - Label verification - Total booking value");
			PrintTemplate.verifyTrue(commonLabels.get("totalAmountPayableUpfront"), 	labels.get("amountPayableUpfront"), 				"booking list - Booking card - Label verification - Amount payable upfront");
			PrintTemplate.verifyTrue(commonLabels.get("totalPayableAtCheckin"), 		labels.get("totalAmountPAyableAmountAtCheckin"), 	"booking list - Booking card - Label verification - Total amount payable at chekin");
			PrintTemplate.verifyTrue(commonLabels.get("amountPaid"), 					labels.get("amountPaid"), 							"booking list - Booking card - Label verification - Amount paid");
			PrintTemplate.verifyTrue(commonLabels.get("customerDetails"), 				labels.get("customerLeadGuestDetails"), 			"booking list - Booking card - Label verification - Customer lead guest details");
			PrintTemplate.verifyTrue(commonLabels.get("firstName"), 					labels.get("firstName"), 							"booking list - Booking card - Label verification - First name");
			PrintTemplate.verifyTrue(commonLabels.get("address"), 						labels.get("address"), 								"booking list - Booking card - Label verification - Address");
			PrintTemplate.verifyTrue(commonLabels.get("email"), 						labels.get("emailAddress"), 						"booking list - Booking card - Label verification - Email");
			PrintTemplate.verifyTrue(commonLabels.get("city"), 							labels.get("city"), 								"booking list - Booking card - Label verification - City");
			PrintTemplate.verifyTrue(commonLabels.get("country"), 						labels.get("countryLabel"), 						"booking list - Booking card - Label verification - Country");
			PrintTemplate.verifyTrue(commonLabels.get("emergencyContact"), 				labels.get("emergencyContactNumber"), 				"booking list - Booking card - Label verification - Emergency contact number");
			PrintTemplate.verifyTrue(commonLabels.get("phoneNumber"), 					labels.get("phoneNumber"), 							"booking list - Booking card - Label verification - Phone number");
		
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
}
