package com.rezrobot.types;

public enum ProfitMarkUpApplicableType {
	
	ALLHOTELS,SELECTEDHOTELS,SELECTEDROOMBED,NONE;
	
	public static ProfitMarkUpApplicableType getChargeType(String ChargeType)
	{
		if(ChargeType.equalsIgnoreCase("ALLHOTELS"))return ALLHOTELS;
		else if(ChargeType.equalsIgnoreCase("SELECTEDHOTELS"))return SELECTEDHOTELS;
		else if(ChargeType.equalsIgnoreCase("SELECTEDROOMBED"))return SELECTEDROOMBED;
		else return NONE;
		
	}

	
}
