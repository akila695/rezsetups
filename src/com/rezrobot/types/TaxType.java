package com.rezrobot.types;

public enum TaxType {
	
 SALESTAX,OCCUPANCYTAX,ENERTGYTAX,MISCTAX,NONE;
 
 public TaxType getChargeType(String ChargeType)
 {
 	if(ChargeType.equalsIgnoreCase("SALESTAX"))return SALESTAX;
 	else if(ChargeType.equalsIgnoreCase("OCCUPANCYTAX"))return OCCUPANCYTAX;
 	else if(ChargeType.equalsIgnoreCase("ENERTGYTAX"))return ENERTGYTAX;
 	else if(ChargeType.equalsIgnoreCase("MISCTAX"))return MISCTAX;
 	else return NONE;
 	
 }

}
