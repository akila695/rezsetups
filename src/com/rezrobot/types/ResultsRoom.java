package com.rezrobot.types;

import java.util.ArrayList;

public class ResultsRoom {

	private String RoomString = "";
	private String RoomType = "";
	private String BedType = "";
	private String RatePlan = "";
	private String RoomRate = "";
	private ArrayList<String> DailyRates = new ArrayList<String>();
	private String RoomCurrency = "";
	private boolean DailyRatePopUpAvailable = false;
	private boolean PromotionApplied;
	private String PromoCode = "";
	private String PromoNote = "N/A";

	public boolean isPromotionApplied() {
		return PromotionApplied;
	}

	public void setPromotionApplied(boolean promotionApplied) {
		PromotionApplied = promotionApplied;
	}

	public String getPromoCode() {
		return PromoCode;
	}

	public void setPromoCode(String promoCode) {
		PromoCode = promoCode;
	}

	public String getPromoNote() {
		return PromoNote;
	}

	public void setPromoNote(String promoNote) {
		PromoNote = promoNote;
	}

	public String getRoomCurrency() {
		return RoomCurrency;
	}

	public void setRoomCurrency(String roomCurrency) {
		RoomCurrency = roomCurrency;
	}

	public boolean isDailyRatePopUpAvailable() {
		return DailyRatePopUpAvailable;
	}

	public void setDailyRatePopUpAvailable(boolean dailyRatePopUpAvailable) {
		DailyRatePopUpAvailable = dailyRatePopUpAvailable;
	}

	public String getRoomString() {
		return RoomString;
	}

	public void setRoomString(String roomString) {
		RoomString = roomString;
	}

	public String getRoomType() {
		return RoomType;
	}

	public void setRoomType(String roomType) {
		RoomType = roomType;
	}

	public String getBedType() {
		return BedType;
	}

	public void setBedType(String bedType) {
		BedType = bedType;
	}

	public String getRatePlan() {
		return RatePlan;
	}

	public void setRatePlan(String ratePlan) {
		RatePlan = ratePlan;
	}

	public String getRoomRate() {
		return RoomRate;
	}

	public void setRoomRate(String roomRate) {
		RoomRate = roomRate;
	}

	public ArrayList<String> getDailyRates() {
		return DailyRates;
	}

	public void setDailyRatsses(ArrayList<String> dailyRates) {
		DailyRates = dailyRates;
	}

}
