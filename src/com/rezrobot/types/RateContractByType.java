package com.rezrobot.types;

public enum RateContractByType {

	NETRATE,COMMISSIONABLE,NONE;
	
	public static RateContractByType getRateContractType(String ChargeType)
	{
		if(ChargeType.equalsIgnoreCase("Net Rate"))return NETRATE;
		else if(ChargeType.equalsIgnoreCase("Commissionable Rate"))return COMMISSIONABLE;
		else return NONE;

		
	}



}
