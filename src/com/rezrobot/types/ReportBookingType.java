package com.rezrobot.types;

public enum ReportBookingType {

	ALL, WITHF, WITHOUTF,FONLY,NONE;

	public static ReportBookingType getTourOperatorType(String reporttype) {

		if (reporttype.trim().equalsIgnoreCase("ALL"))
			return ReportBookingType.ALL;
		else if (reporttype.trim().equalsIgnoreCase("WITHF"))
			return ReportBookingType.WITHF;
		else if (reporttype.trim().equalsIgnoreCase("WITHOUTF"))
			return ReportBookingType.WITHOUTF;
		else if (reporttype.trim().equalsIgnoreCase("FONLY"))
			return ReportBookingType.FONLY;
		else
			return ReportBookingType.NONE;
	}
}
