package com.rezrobot.enumtypes;

public enum WEB_TourOperatorType {

	WEB_DC,WEB_NETCASH, WEB_NETCREDITLPOY, WEB_NETCREDITLPON, WEB_COMCASH, WEB_COMCREDITLPOY, WEB_COMCREDITLPONO, NONE;

	public static WEB_TourOperatorType getTourOperatorType(String touroperator) {

		if (touroperator.trim().equalsIgnoreCase("WEB_NETCASH"))
			return WEB_TourOperatorType.WEB_NETCASH;
		else if (touroperator.trim().equalsIgnoreCase("WEB_NETCREDITLPOY"))
			return WEB_TourOperatorType.WEB_NETCREDITLPOY;
		else if (touroperator.trim().equalsIgnoreCase("WEB_NETCREDITLPON"))
			return WEB_TourOperatorType.WEB_NETCREDITLPON;
		else if (touroperator.trim().equalsIgnoreCase("WEB_COMCASH"))
			return WEB_TourOperatorType.WEB_COMCASH;
		else if (touroperator.trim().equalsIgnoreCase("WEB_COMCREDITLPOY"))
			return WEB_TourOperatorType.WEB_COMCREDITLPOY;
		else if (touroperator.trim().equalsIgnoreCase("WEB_COMCREDITLPONO"))
			return WEB_TourOperatorType.WEB_COMCREDITLPONO;
		else if (touroperator.trim().equalsIgnoreCase("WEB_DC"))
			return WEB_TourOperatorType.WEB_DC;
		else
			return WEB_TourOperatorType.NONE;
	}

}
