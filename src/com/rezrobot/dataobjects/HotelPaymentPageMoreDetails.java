package com.rezrobot.dataobjects;

import java.io.Serializable;
import java.util.ArrayList;

public class HotelPaymentPageMoreDetails implements Serializable{

	String hotelPrice = ""; 
	String subtotal = ""; 
	String taxes = ""; 
	String total = ""; 
	String amountProcessedNow = ""; 
	String status = ""; 
	String cancellationDeadline = ""; 
	ArrayList<String> roomType 	= new ArrayList<String>();
	ArrayList<String> bedType 	= new ArrayList<String>();
	ArrayList<String> ratePlan 	= new ArrayList<String>();
	ArrayList<String> roomRate 	= new ArrayList<String>();
	String moreDetailsSubtotal = "";
	String moreDetailsTax = "";
	String moreDetailsHotelcost = "";
	String currencyCode = "";
	
	
	
	public String getCurrencyCode() {
		return currencyCode;
	}
	public void setCurrencyCode(String currencyCode) {
		this.currencyCode = currencyCode;
	}
	public String getMoreDetailsSubtotal() {
		return moreDetailsSubtotal;
	}
	public void setMoreDetailsSubtotal(String moreDetailsSubtotal) {
		this.moreDetailsSubtotal = moreDetailsSubtotal;
	}
	public String getMoreDetailsTax() {
		return moreDetailsTax;
	}
	public void setMoreDetailsTax(String moreDetailsTax) {
		this.moreDetailsTax = moreDetailsTax;
	}
	public String getMoreDetailsHotelcost() {
		return moreDetailsHotelcost;
	}
	public void setMoreDetailsHotelcost(String moreDetailsHotelcost) {
		this.moreDetailsHotelcost = moreDetailsHotelcost;
	}
	public String getHotelPrice() {
		return hotelPrice;
	}
	public void setHotelPrice(String hotelPrice) {
		this.hotelPrice = hotelPrice;
	}
	public String getSubtotal() {
		return subtotal;
	}
	public void setSubtotal(String subtotal) {
		this.subtotal = subtotal;
	}
	public String getTaxes() {
		return taxes;
	}
	public void setTaxes(String taxes) {
		this.taxes = taxes;
	}
	public String getTotal() {
		return total;
	}
	public void setTotal(String total) {
		this.total = total;
	}
	public String getAmountProcessedNow() {
		return amountProcessedNow;
	}
	public void setAmountProcessedNow(String amountProcessedNow) {
		this.amountProcessedNow = amountProcessedNow;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getCancellationDeadline() {
		return cancellationDeadline;
	}
	public void setCancellationDeadline(String cancellationDeadline) {
		this.cancellationDeadline = cancellationDeadline;
	}
	public ArrayList<String> getRoomType() {
		return roomType;
	}
	public void setRoomType(ArrayList<String> roomType) {
		this.roomType = roomType;
	}
	public ArrayList<String> getBedType() {
		return bedType;
	}
	public void setBedType(ArrayList<String> bedType) {
		this.bedType = bedType;
	}
	public ArrayList<String> getRatePlan() {
		return ratePlan;
	}
	public void setRatePlan(ArrayList<String> ratePlan) {
		this.ratePlan = ratePlan;
	}
	public ArrayList<String> getRoomRate() {
		return roomRate;
	}
	public void setRoomRate(ArrayList<String> roomRate) {
		this.roomRate = roomRate;
	}
	
	
}
