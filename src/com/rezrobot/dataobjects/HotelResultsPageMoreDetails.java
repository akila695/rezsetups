package com.rezrobot.dataobjects;

import java.io.Serializable;
import java.util.ArrayList;

public class HotelResultsPageMoreDetails implements Serializable{

	boolean moreInfoAvailability ;
	String overview = "";
	boolean imageAvailability ;
	boolean mapAvailability ;
	boolean amanityAvailability ;
	ArrayList<String> amenityList = new ArrayList<String>();
	String additionalInfo = "";
	boolean hotelNameAvailability ;
	boolean starratingAvailability ;
	boolean reviewsAndRatingsAvailability ;
	String emailSubject = "";
	String emailBody = "";
	String emailSentStatus = "";
	
	
	public String getEmailSentStatus() {
		return emailSentStatus;
	}
	public void setEmailSentStatus(String emailSentStatus) {
		this.emailSentStatus = emailSentStatus;
	}
	public String getEmailSubject() {
		return emailSubject;
	}
	public void setEmailSubject(String emailSubject) {
		this.emailSubject = emailSubject;
	}
	public String getEmailBody() {
		return emailBody;
	}
	public void setEmailBody(String emailBody) {
		this.emailBody = emailBody;
	}
	public boolean isReviewsAndRatingsAvailability() {
		return reviewsAndRatingsAvailability;
	}
	public void setReviewsAndRatingsAvailability(
			boolean reviewsAndRatingsAvailability) {
		this.reviewsAndRatingsAvailability = reviewsAndRatingsAvailability;
	}
	public boolean isHotelNameAvailability() {
		return hotelNameAvailability;
	}
	public void setHotelNameAvailability(boolean hotelNameAvailability) {
		this.hotelNameAvailability = hotelNameAvailability;
	}
	public boolean isStarratingAvailability() {
		return starratingAvailability;
	}
	public void setStarratingAvailability(boolean starratingAvailability) {
		this.starratingAvailability = starratingAvailability;
	}
	public boolean isMoreInfoAvailability() {
		return moreInfoAvailability;
	}
	public void setMoreInfoAvailability(boolean moreInfoAvailability) {
		this.moreInfoAvailability = moreInfoAvailability;
	}
	public String getOverview() {
		return overview;
	}
	public void setOverview(String overview) {
		this.overview = overview;
	}
	public boolean isImageAvailability() {
		return imageAvailability;
	}
	public void setImageAvailability(boolean imageAvailability) {
		this.imageAvailability = imageAvailability;
	}
	public boolean isMapAvailability() {
		return mapAvailability;
	}
	public void setMapAvailability(boolean mapAvailability) {
		this.mapAvailability = mapAvailability;
	}
	public boolean isAmanityAvailability() {
		return amanityAvailability;
	}
	public void setAmanityAvailability(boolean amanityAvailability) {
		this.amanityAvailability = amanityAvailability;
	}
	public ArrayList<String> getAmenityList() {
		return amenityList;
	}
	public void setAmenityList(ArrayList<String> amenityList) {
		this.amenityList = amenityList;
	}
	public String getAdditionalInfo() {
		return additionalInfo;
	}
	public void setAdditionalInfo(String additionalInfo) {
		this.additionalInfo = additionalInfo;
	}
	
	
}
