package com.rezrobot.dataobjects;

import java.io.Serializable;
import java.util.ArrayList;

public class HotelRoomDetails implements Serializable{


	String room = "";
 	String roomType ="";  
	String ratePlan =""; 
	String rate ="";
	String bedType = "";

	public String getRoomType() {
		return roomType;
	}
	public void setRoomType(String roomType) {
		this.roomType = roomType;
	}
	
	public String getRatePlan() {
		return ratePlan;
	}
	public void setRatePlan(String ratePlan) {
		this.ratePlan = ratePlan;
	}
	public String getRate() {
		return rate;
	}
	public void setRate(String rate) {
		this.rate = rate;
	}
	public String getRoom() {
		return room;
	}
	public void setRoom(String room) {
		this.room = room;
	}
	public String getBedType() {
		return bedType;
	}
	public void setBedType(String bedType) {
		this.bedType = bedType;
	}
	
	
	

	
	
}
