package com.rezrobot.dataobjects;

import java.io.Serializable;
import java.util.ArrayList;



public class HotelDetails implements Serializable{

	String title =""; 
	String address =""; 
	String availability =""; 
	String rate =""; 
	String supplier ="";
	String starRating = "";
	String destination ="";
	String hotelID = "";
	String tracerID = "";
	String currency = "";
	String emailSubject = "";
	String emailBody = "";
	String emailStatus = "";
	boolean rateChangeAvailable = false;
	String rateChangeError = "";
	String rateChangeErrorMsg = "";
	Boolean isRateChangeAvailable = false;
	HotelResultsPageMoreDetails moreInfo = new HotelResultsPageMoreDetails();
	
	ArrayList<HotelRoomDetails> 	roomList		=  	new ArrayList<HotelRoomDetails>();
	
	
	


	public boolean isRateChangeAvailable() {
		return rateChangeAvailable;
	}


	public void setRateChangeAvailable(boolean rateChangeAvailable) {
		this.rateChangeAvailable = rateChangeAvailable;
	}


	public String getRateChangeError() {
		return rateChangeError;
	}


	public void setRateChangeError(String rateChangeError) {
		this.rateChangeError = rateChangeError;
	}


	public String getRateChangeErrorMsg() {
		return rateChangeErrorMsg;
	}


	public void setRateChangeErrorMsg(String rateChangeErrorMsg) {
		this.rateChangeErrorMsg = rateChangeErrorMsg;
	}


	public ArrayList<HotelRoomDetails> getRoomList() {
		return roomList;
	}


	public Boolean getIsRateChangeAvailable() {
		return isRateChangeAvailable;
	}


	public void setIsRateChangeAvailable(Boolean isRateChangeAvailable) {
		this.isRateChangeAvailable = isRateChangeAvailable;
	}


	public HotelResultsPageMoreDetails getMoreInfo() {
		return moreInfo;
	}


	public void setMoreInfo(HotelResultsPageMoreDetails moreInfo) {
		this.moreInfo = moreInfo;
	}


	public String getEmailStatus() {
		return emailStatus;
	}


	public void setEmailStatus(String emailStatus) {
		this.emailStatus = emailStatus;
	}


	public String getEmailSubject() {
		return emailSubject;
	}


	public void setEmailSubject(String emailSubject) {
		this.emailSubject = emailSubject;
	}


	public String getEmailBody() {
		return emailBody;
	}


	public void setEmailBody(String emailBody) {
		this.emailBody = emailBody;
	}


	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public String getDestination() {
		return destination;
	}

	public void setDestination(String destination) {
		this.destination = destination;
	}

	public String getHotelID() {
		return hotelID;
	}

	public void setHotelID(String hotelID) {
		this.hotelID = hotelID;
	}

	public String getTracerID() {
		return tracerID;
	}

	public void setTracerID(String tracerID) {
		this.tracerID = tracerID;
	}

	public void setRoomList(ArrayList<HotelRoomDetails> roomList) {
		this.roomList = roomList;
	}

	/*public ArrayList<hotelSearchObject> getHotelList() {
		return hotelList;
	}

	public void setHotelList(ArrayList<hotelSearchObject> hotelList) {
		this.hotelList = hotelList;
	}*/

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getAvailability() {
		return availability;
	}

	public void setAvailability(String availability) {
		this.availability = availability;
	}


	public String getSupplier() {
		return supplier;
	}

	public void setSupplier(String supplier) {
		this.supplier = supplier;
	}

	

	public String getRate() {
		return rate;
	}

	public void setRate(String rate) {
		this.rate = rate;
	}

	public String getStarRating() {
		return starRating;
	}

	public void setStarRating(String starRating) {
		this.starRating = starRating;
	}
	
	
	
}
